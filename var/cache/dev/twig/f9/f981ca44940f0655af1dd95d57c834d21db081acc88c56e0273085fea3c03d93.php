<?php

/* @FOSUser/Group/new.html.twig */
class __TwigTemplate_c2b6acc8746dde9b5b3242ced9dc30f363b59e1cf3b84eea8d7c8ccd4054d38e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_beedb3f8c8fc771d0e5a0ce58b44138e487d7f26838039ff9ce5cc8fee2a25d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_beedb3f8c8fc771d0e5a0ce58b44138e487d7f26838039ff9ce5cc8fee2a25d1->enter($__internal_beedb3f8c8fc771d0e5a0ce58b44138e487d7f26838039ff9ce5cc8fee2a25d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $__internal_f57d0b12db7b3bc9ccb57024744c9e4f34b9662088c902224e2cb96a0a154d6d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f57d0b12db7b3bc9ccb57024744c9e4f34b9662088c902224e2cb96a0a154d6d->enter($__internal_f57d0b12db7b3bc9ccb57024744c9e4f34b9662088c902224e2cb96a0a154d6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_beedb3f8c8fc771d0e5a0ce58b44138e487d7f26838039ff9ce5cc8fee2a25d1->leave($__internal_beedb3f8c8fc771d0e5a0ce58b44138e487d7f26838039ff9ce5cc8fee2a25d1_prof);

        
        $__internal_f57d0b12db7b3bc9ccb57024744c9e4f34b9662088c902224e2cb96a0a154d6d->leave($__internal_f57d0b12db7b3bc9ccb57024744c9e4f34b9662088c902224e2cb96a0a154d6d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_95241504cd02a3cc0d613be04052224aa6e358d2d8735f4d5b17c5e8d046829b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95241504cd02a3cc0d613be04052224aa6e358d2d8735f4d5b17c5e8d046829b->enter($__internal_95241504cd02a3cc0d613be04052224aa6e358d2d8735f4d5b17c5e8d046829b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_d1a90ed6b4d5673816da2e8c66a7642fefe1c5f7971722250f54574b89d26382 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1a90ed6b4d5673816da2e8c66a7642fefe1c5f7971722250f54574b89d26382->enter($__internal_d1a90ed6b4d5673816da2e8c66a7642fefe1c5f7971722250f54574b89d26382_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "@FOSUser/Group/new.html.twig", 4)->display($context);
        
        $__internal_d1a90ed6b4d5673816da2e8c66a7642fefe1c5f7971722250f54574b89d26382->leave($__internal_d1a90ed6b4d5673816da2e8c66a7642fefe1c5f7971722250f54574b89d26382_prof);

        
        $__internal_95241504cd02a3cc0d613be04052224aa6e358d2d8735f4d5b17c5e8d046829b->leave($__internal_95241504cd02a3cc0d613be04052224aa6e358d2d8735f4d5b17c5e8d046829b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/new_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/new.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\new.html.twig");
    }
}
