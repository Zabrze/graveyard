<?php

/* @FOSUser/Resetting/email.txt.twig */
class __TwigTemplate_b71c46e7c2bfe7d3122145a1a459fc74f6954533b7e1d54f3bb5ee4d1236560d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46facba46f8dcdc6fa2a21c1af9aea2f98c156c0c8162df26c5db9386ef890eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46facba46f8dcdc6fa2a21c1af9aea2f98c156c0c8162df26c5db9386ef890eb->enter($__internal_46facba46f8dcdc6fa2a21c1af9aea2f98c156c0c8162df26c5db9386ef890eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        $__internal_f4b825ce12351634de25009ad2c79e141a0e60ea30f9bdd8997f9a078bf9211f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4b825ce12351634de25009ad2c79e141a0e60ea30f9bdd8997f9a078bf9211f->enter($__internal_f4b825ce12351634de25009ad2c79e141a0e60ea30f9bdd8997f9a078bf9211f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_46facba46f8dcdc6fa2a21c1af9aea2f98c156c0c8162df26c5db9386ef890eb->leave($__internal_46facba46f8dcdc6fa2a21c1af9aea2f98c156c0c8162df26c5db9386ef890eb_prof);

        
        $__internal_f4b825ce12351634de25009ad2c79e141a0e60ea30f9bdd8997f9a078bf9211f->leave($__internal_f4b825ce12351634de25009ad2c79e141a0e60ea30f9bdd8997f9a078bf9211f_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_2823a7122b45edb6f9bd01ba94da1e09e0f25a0dcab18dd4044d50bb00486918 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2823a7122b45edb6f9bd01ba94da1e09e0f25a0dcab18dd4044d50bb00486918->enter($__internal_2823a7122b45edb6f9bd01ba94da1e09e0f25a0dcab18dd4044d50bb00486918_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_93d8d268a6ceebc31e365477464813bbc49b7309ec6d8319733563cfeaeb0a3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93d8d268a6ceebc31e365477464813bbc49b7309ec6d8319733563cfeaeb0a3f->enter($__internal_93d8d268a6ceebc31e365477464813bbc49b7309ec6d8319733563cfeaeb0a3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_93d8d268a6ceebc31e365477464813bbc49b7309ec6d8319733563cfeaeb0a3f->leave($__internal_93d8d268a6ceebc31e365477464813bbc49b7309ec6d8319733563cfeaeb0a3f_prof);

        
        $__internal_2823a7122b45edb6f9bd01ba94da1e09e0f25a0dcab18dd4044d50bb00486918->leave($__internal_2823a7122b45edb6f9bd01ba94da1e09e0f25a0dcab18dd4044d50bb00486918_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_276997dacf62084cda6d574647c7755c4555fac3266213612dd36536c31d3e77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_276997dacf62084cda6d574647c7755c4555fac3266213612dd36536c31d3e77->enter($__internal_276997dacf62084cda6d574647c7755c4555fac3266213612dd36536c31d3e77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_e1590545f9dff67c85e55d336f95fe2522e437b4af1cb1d7c609694d5800db6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1590545f9dff67c85e55d336f95fe2522e437b4af1cb1d7c609694d5800db6b->enter($__internal_e1590545f9dff67c85e55d336f95fe2522e437b4af1cb1d7c609694d5800db6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_e1590545f9dff67c85e55d336f95fe2522e437b4af1cb1d7c609694d5800db6b->leave($__internal_e1590545f9dff67c85e55d336f95fe2522e437b4af1cb1d7c609694d5800db6b_prof);

        
        $__internal_276997dacf62084cda6d574647c7755c4555fac3266213612dd36536c31d3e77->leave($__internal_276997dacf62084cda6d574647c7755c4555fac3266213612dd36536c31d3e77_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_8db907bfd95a387751b2d6a88da2f6564dc55a2654b8b045fc575ae9d14e69cc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8db907bfd95a387751b2d6a88da2f6564dc55a2654b8b045fc575ae9d14e69cc->enter($__internal_8db907bfd95a387751b2d6a88da2f6564dc55a2654b8b045fc575ae9d14e69cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_50e2bd0bb1f6c5efcf3017f97af692257799ae2864154480e620e4919cfe2f7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_50e2bd0bb1f6c5efcf3017f97af692257799ae2864154480e620e4919cfe2f7e->enter($__internal_50e2bd0bb1f6c5efcf3017f97af692257799ae2864154480e620e4919cfe2f7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_50e2bd0bb1f6c5efcf3017f97af692257799ae2864154480e620e4919cfe2f7e->leave($__internal_50e2bd0bb1f6c5efcf3017f97af692257799ae2864154480e620e4919cfe2f7e_prof);

        
        $__internal_8db907bfd95a387751b2d6a88da2f6564dc55a2654b8b045fc575ae9d14e69cc->leave($__internal_8db907bfd95a387751b2d6a88da2f6564dc55a2654b8b045fc575ae9d14e69cc_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "@FOSUser/Resetting/email.txt.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\email.txt.twig");
    }
}
