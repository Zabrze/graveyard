<?php

/* @FOSUser/Resetting/reset.html.twig */
class __TwigTemplate_0830aa1cfcb3bea7c64844792c4dcf556a6ce4f5cab5e15a2239fd94f87c5184 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a7d158718433e3185751e0e7cab8044d4d9651b9d86b560e8238f2265a02a2a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7d158718433e3185751e0e7cab8044d4d9651b9d86b560e8238f2265a02a2a3->enter($__internal_a7d158718433e3185751e0e7cab8044d4d9651b9d86b560e8238f2265a02a2a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $__internal_e8998565135ccb7a3beec8c9d717b4a5d19bd617223ec5d69b772d771ba90224 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8998565135ccb7a3beec8c9d717b4a5d19bd617223ec5d69b772d771ba90224->enter($__internal_e8998565135ccb7a3beec8c9d717b4a5d19bd617223ec5d69b772d771ba90224_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a7d158718433e3185751e0e7cab8044d4d9651b9d86b560e8238f2265a02a2a3->leave($__internal_a7d158718433e3185751e0e7cab8044d4d9651b9d86b560e8238f2265a02a2a3_prof);

        
        $__internal_e8998565135ccb7a3beec8c9d717b4a5d19bd617223ec5d69b772d771ba90224->leave($__internal_e8998565135ccb7a3beec8c9d717b4a5d19bd617223ec5d69b772d771ba90224_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_bc557a3a235b6b85511ad8dc57d6dbb65de08900845640ba028cd9c1b9e9868c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc557a3a235b6b85511ad8dc57d6dbb65de08900845640ba028cd9c1b9e9868c->enter($__internal_bc557a3a235b6b85511ad8dc57d6dbb65de08900845640ba028cd9c1b9e9868c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_52b1dd57a44b8cee0251afac03ca8d223ee2f649aead240301fa1a6e0d670e8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_52b1dd57a44b8cee0251afac03ca8d223ee2f649aead240301fa1a6e0d670e8b->enter($__internal_52b1dd57a44b8cee0251afac03ca8d223ee2f649aead240301fa1a6e0d670e8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "@FOSUser/Resetting/reset.html.twig", 4)->display($context);
        
        $__internal_52b1dd57a44b8cee0251afac03ca8d223ee2f649aead240301fa1a6e0d670e8b->leave($__internal_52b1dd57a44b8cee0251afac03ca8d223ee2f649aead240301fa1a6e0d670e8b_prof);

        
        $__internal_bc557a3a235b6b85511ad8dc57d6dbb65de08900845640ba028cd9c1b9e9868c->leave($__internal_bc557a3a235b6b85511ad8dc57d6dbb65de08900845640ba028cd9c1b9e9868c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/reset_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Resetting/reset.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\reset.html.twig");
    }
}
