<?php

/* @FOSUser/layout.html.twig */
class __TwigTemplate_d93f1f00ca8276a9f906eb87a1aaea3ff3fa4956fa843d1d1f3c3762a5e1a1f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ce32145005bb7856090ece9d1a02db1f1d504f3d91edf8f6912f51f987e6555 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ce32145005bb7856090ece9d1a02db1f1d504f3d91edf8f6912f51f987e6555->enter($__internal_6ce32145005bb7856090ece9d1a02db1f1d504f3d91edf8f6912f51f987e6555_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        $__internal_511462792a97bee2efd7673ed099793817f2832d3399da9909478c34dcb5f6b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_511462792a97bee2efd7673ed099793817f2832d3399da9909478c34dcb5f6b8->enter($__internal_511462792a97bee2efd7673ed099793817f2832d3399da9909478c34dcb5f6b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        ";
        // line 5
        $this->loadTemplate("base.html.twig", "@FOSUser/layout.html.twig", 5)->display($context);
        // line 6
        echo "    </head>
    <body>
        

        <div>
            ";
        // line 11
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 12
            echo "                ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " |
                <a href=\"";
            // line 13
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                    ";
            // line 14
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                </a>
            ";
        } else {
            // line 18
            echo "            ";
        }
        // line 19
        echo "        </div>

        ";
        // line 21
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 22
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 23
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 24
                    echo "                    <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                        ";
                    // line 25
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 28
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "        ";
        }
        // line 30
        echo "
        <div>
            ";
        // line 32
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 34
        echo "        </div>
    </body>
</html>
";
        
        $__internal_6ce32145005bb7856090ece9d1a02db1f1d504f3d91edf8f6912f51f987e6555->leave($__internal_6ce32145005bb7856090ece9d1a02db1f1d504f3d91edf8f6912f51f987e6555_prof);

        
        $__internal_511462792a97bee2efd7673ed099793817f2832d3399da9909478c34dcb5f6b8->leave($__internal_511462792a97bee2efd7673ed099793817f2832d3399da9909478c34dcb5f6b8_prof);

    }

    // line 32
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6321eace9540f689f8436d08e11592a420aa527b64cd1413ece8ca4b2550c535 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6321eace9540f689f8436d08e11592a420aa527b64cd1413ece8ca4b2550c535->enter($__internal_6321eace9540f689f8436d08e11592a420aa527b64cd1413ece8ca4b2550c535_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_e2b1eb90cd41608d4776042b32c0e174b5ff348c6e238da0b04a7fa177f349fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2b1eb90cd41608d4776042b32c0e174b5ff348c6e238da0b04a7fa177f349fd->enter($__internal_e2b1eb90cd41608d4776042b32c0e174b5ff348c6e238da0b04a7fa177f349fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 33
        echo "            ";
        
        $__internal_e2b1eb90cd41608d4776042b32c0e174b5ff348c6e238da0b04a7fa177f349fd->leave($__internal_e2b1eb90cd41608d4776042b32c0e174b5ff348c6e238da0b04a7fa177f349fd_prof);

        
        $__internal_6321eace9540f689f8436d08e11592a420aa527b64cd1413ece8ca4b2550c535->leave($__internal_6321eace9540f689f8436d08e11592a420aa527b64cd1413ece8ca4b2550c535_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 33,  119 => 32,  106 => 34,  104 => 32,  100 => 30,  97 => 29,  91 => 28,  82 => 25,  77 => 24,  72 => 23,  67 => 22,  65 => 21,  61 => 19,  58 => 18,  52 => 14,  48 => 13,  43 => 12,  41 => 11,  34 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        {% include 'base.html.twig' %}
    </head>
    <body>
        

        <div>
            {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |
                <a href=\"{{ path('fos_user_security_logout') }}\">
                    {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                </a>
            {% else %}
{#                <a href=\"{{ path('fos_user_security_login') }}\">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>#}
            {% endif %}
        </div>

        {% if app.request.hasPreviousSession %}
            {% for type, messages in app.session.flashbag.all() %}
                {% for message in messages %}
                    <div class=\"flash-{{ type }}\">
                        {{ message }}
                    </div>
                {% endfor %}
            {% endfor %}
        {% endif %}

        <div>
            {% block fos_user_content %}
            {% endblock fos_user_content %}
        </div>
    </body>
</html>
", "@FOSUser/layout.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\layout.html.twig");
    }
}
