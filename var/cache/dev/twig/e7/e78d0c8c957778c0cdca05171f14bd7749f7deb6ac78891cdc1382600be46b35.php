<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_64ec3235d7327506c7c731123281383cb42a5dbbc7befb315cdfedd61a52079f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e93f8a7f7fa6a0d53284d7098f9b5d5591cd7dc02174ddf8b0e2ce6be2a1b35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e93f8a7f7fa6a0d53284d7098f9b5d5591cd7dc02174ddf8b0e2ce6be2a1b35->enter($__internal_8e93f8a7f7fa6a0d53284d7098f9b5d5591cd7dc02174ddf8b0e2ce6be2a1b35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_4c09beb6ea2ce0e7ef0c788df156bfdcbf7736d8d65033704d753136f02a63e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c09beb6ea2ce0e7ef0c788df156bfdcbf7736d8d65033704d753136f02a63e3->enter($__internal_4c09beb6ea2ce0e7ef0c788df156bfdcbf7736d8d65033704d753136f02a63e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_8e93f8a7f7fa6a0d53284d7098f9b5d5591cd7dc02174ddf8b0e2ce6be2a1b35->leave($__internal_8e93f8a7f7fa6a0d53284d7098f9b5d5591cd7dc02174ddf8b0e2ce6be2a1b35_prof);

        
        $__internal_4c09beb6ea2ce0e7ef0c788df156bfdcbf7736d8d65033704d753136f02a63e3->leave($__internal_4c09beb6ea2ce0e7ef0c788df156bfdcbf7736d8d65033704d753136f02a63e3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\hidden_row.html.php");
    }
}
