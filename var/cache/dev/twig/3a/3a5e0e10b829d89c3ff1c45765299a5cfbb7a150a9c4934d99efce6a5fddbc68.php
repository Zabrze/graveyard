<?php

/* bootstrap_3_horizontal_layout.html.twig */
class __TwigTemplate_d8b2f41ec9e1c811a1050935558c98730acbf56694a3614c38b398db5c7a61c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_3_layout.html.twig", "bootstrap_3_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_3_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_start' => array($this, 'block_form_start'),
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6e9b41f85e49de15863b73159e593aced2f012a03dfc49ae753590d12f0a15ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e9b41f85e49de15863b73159e593aced2f012a03dfc49ae753590d12f0a15ad->enter($__internal_6e9b41f85e49de15863b73159e593aced2f012a03dfc49ae753590d12f0a15ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_horizontal_layout.html.twig"));

        $__internal_bd4919e22dde7bc9011c64dad414a685ba17674d5f678522239b84764689dd5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd4919e22dde7bc9011c64dad414a685ba17674d5f678522239b84764689dd5b->enter($__internal_bd4919e22dde7bc9011c64dad414a685ba17674d5f678522239b84764689dd5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 3
        $this->displayBlock('form_start', $context, $blocks);
        // line 7
        echo "
";
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('form_label', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 22
        echo "
";
        // line 24
        echo "
";
        // line 25
        $this->displayBlock('form_row', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('submit_row', $context, $blocks);
        // line 43
        echo "
";
        // line 44
        $this->displayBlock('reset_row', $context, $blocks);
        // line 52
        echo "
";
        // line 53
        $this->displayBlock('form_group_class', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        $this->displayBlock('checkbox_row', $context, $blocks);
        
        $__internal_6e9b41f85e49de15863b73159e593aced2f012a03dfc49ae753590d12f0a15ad->leave($__internal_6e9b41f85e49de15863b73159e593aced2f012a03dfc49ae753590d12f0a15ad_prof);

        
        $__internal_bd4919e22dde7bc9011c64dad414a685ba17674d5f678522239b84764689dd5b->leave($__internal_bd4919e22dde7bc9011c64dad414a685ba17674d5f678522239b84764689dd5b_prof);

    }

    // line 3
    public function block_form_start($context, array $blocks = array())
    {
        $__internal_fbe76fb46346d32074ae275785f992dec0188f419978a70716def2266507bbfc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fbe76fb46346d32074ae275785f992dec0188f419978a70716def2266507bbfc->enter($__internal_fbe76fb46346d32074ae275785f992dec0188f419978a70716def2266507bbfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        $__internal_55f75fa6c9da366791245f7cc1d8028b309232f318ee696b2974f9073133a321 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55f75fa6c9da366791245f7cc1d8028b309232f318ee696b2974f9073133a321->enter($__internal_55f75fa6c9da366791245f7cc1d8028b309232f318ee696b2974f9073133a321_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        // line 4
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-horizontal"))));
        // line 5
        $this->displayParentBlock("form_start", $context, $blocks);
        
        $__internal_55f75fa6c9da366791245f7cc1d8028b309232f318ee696b2974f9073133a321->leave($__internal_55f75fa6c9da366791245f7cc1d8028b309232f318ee696b2974f9073133a321_prof);

        
        $__internal_fbe76fb46346d32074ae275785f992dec0188f419978a70716def2266507bbfc->leave($__internal_fbe76fb46346d32074ae275785f992dec0188f419978a70716def2266507bbfc_prof);

    }

    // line 10
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_d4c0ab7dc81105d8e4f9b9faa4e51669af9f3ea20f3672665dbf2393ff32f131 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d4c0ab7dc81105d8e4f9b9faa4e51669af9f3ea20f3672665dbf2393ff32f131->enter($__internal_d4c0ab7dc81105d8e4f9b9faa4e51669af9f3ea20f3672665dbf2393ff32f131_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_194e4d4043b82e14d5aed76403e2f24e712db3b427a2b7f5909a4bfb02a697dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_194e4d4043b82e14d5aed76403e2f24e712db3b427a2b7f5909a4bfb02a697dc->enter($__internal_194e4d4043b82e14d5aed76403e2f24e712db3b427a2b7f5909a4bfb02a697dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 11
        if (((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) {
            // line 12
            echo "<div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>";
        } else {
            // line 14
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 15
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        
        $__internal_194e4d4043b82e14d5aed76403e2f24e712db3b427a2b7f5909a4bfb02a697dc->leave($__internal_194e4d4043b82e14d5aed76403e2f24e712db3b427a2b7f5909a4bfb02a697dc_prof);

        
        $__internal_d4c0ab7dc81105d8e4f9b9faa4e51669af9f3ea20f3672665dbf2393ff32f131->leave($__internal_d4c0ab7dc81105d8e4f9b9faa4e51669af9f3ea20f3672665dbf2393ff32f131_prof);

    }

    // line 19
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_40e0b1526a0b9d49c82181896e7c1cbe3a46da372de1ce201570a97e80bb7cf4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40e0b1526a0b9d49c82181896e7c1cbe3a46da372de1ce201570a97e80bb7cf4->enter($__internal_40e0b1526a0b9d49c82181896e7c1cbe3a46da372de1ce201570a97e80bb7cf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_ca420221ee1de7b6e001dc028563003badda1d09a32f6e9bda4fb9fdb2d98c67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca420221ee1de7b6e001dc028563003badda1d09a32f6e9bda4fb9fdb2d98c67->enter($__internal_ca420221ee1de7b6e001dc028563003badda1d09a32f6e9bda4fb9fdb2d98c67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        // line 20
        echo "col-sm-2";
        
        $__internal_ca420221ee1de7b6e001dc028563003badda1d09a32f6e9bda4fb9fdb2d98c67->leave($__internal_ca420221ee1de7b6e001dc028563003badda1d09a32f6e9bda4fb9fdb2d98c67_prof);

        
        $__internal_40e0b1526a0b9d49c82181896e7c1cbe3a46da372de1ce201570a97e80bb7cf4->leave($__internal_40e0b1526a0b9d49c82181896e7c1cbe3a46da372de1ce201570a97e80bb7cf4_prof);

    }

    // line 25
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_6165d44edfc523dba137c942e79e873afb301860c4ab0e1bc1de26c401d35a4f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6165d44edfc523dba137c942e79e873afb301860c4ab0e1bc1de26c401d35a4f->enter($__internal_6165d44edfc523dba137c942e79e873afb301860c4ab0e1bc1de26c401d35a4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_d70169a2606f1adffed82f165038ba4402b4d4f98f634881ae5448eb2f72f856 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d70169a2606f1adffed82f165038ba4402b4d4f98f634881ae5448eb2f72f856->enter($__internal_d70169a2606f1adffed82f165038ba4402b4d4f98f634881ae5448eb2f72f856_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 26
        echo "<div class=\"form-group";
        if ((( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter((isset($context["force_error"]) ? $context["force_error"] : $this->getContext($context, "force_error")), false)) : (false))) &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        // line 28
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 31
        echo "</div>
";
        // line 32
        echo "</div>";
        
        $__internal_d70169a2606f1adffed82f165038ba4402b4d4f98f634881ae5448eb2f72f856->leave($__internal_d70169a2606f1adffed82f165038ba4402b4d4f98f634881ae5448eb2f72f856_prof);

        
        $__internal_6165d44edfc523dba137c942e79e873afb301860c4ab0e1bc1de26c401d35a4f->leave($__internal_6165d44edfc523dba137c942e79e873afb301860c4ab0e1bc1de26c401d35a4f_prof);

    }

    // line 35
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_0fc6f3606fa3b7ac35aff6cdb637cc47cc88cefea214bcb7a06433c100b08174 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0fc6f3606fa3b7ac35aff6cdb637cc47cc88cefea214bcb7a06433c100b08174->enter($__internal_0fc6f3606fa3b7ac35aff6cdb637cc47cc88cefea214bcb7a06433c100b08174_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_d404dfe35592d075ec8af079cdd92dc58ce77b4ec8a64bbadb7dbe7782af2ad1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d404dfe35592d075ec8af079cdd92dc58ce77b4ec8a64bbadb7dbe7782af2ad1->enter($__internal_d404dfe35592d075ec8af079cdd92dc58ce77b4ec8a64bbadb7dbe7782af2ad1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 36
        echo "<div class=\"form-group\">";
        // line 37
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 38
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 40
        echo "</div>";
        // line 41
        echo "</div>";
        
        $__internal_d404dfe35592d075ec8af079cdd92dc58ce77b4ec8a64bbadb7dbe7782af2ad1->leave($__internal_d404dfe35592d075ec8af079cdd92dc58ce77b4ec8a64bbadb7dbe7782af2ad1_prof);

        
        $__internal_0fc6f3606fa3b7ac35aff6cdb637cc47cc88cefea214bcb7a06433c100b08174->leave($__internal_0fc6f3606fa3b7ac35aff6cdb637cc47cc88cefea214bcb7a06433c100b08174_prof);

    }

    // line 44
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_281b108188532fa26effdcf22f18f5a0c2afbf100066320d9fb8696878620475 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_281b108188532fa26effdcf22f18f5a0c2afbf100066320d9fb8696878620475->enter($__internal_281b108188532fa26effdcf22f18f5a0c2afbf100066320d9fb8696878620475_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_9b52453cdf5fa2e9fa8af7fcbb0f5ecdd878b0e1d1d0baa861846f1251244fc4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b52453cdf5fa2e9fa8af7fcbb0f5ecdd878b0e1d1d0baa861846f1251244fc4->enter($__internal_9b52453cdf5fa2e9fa8af7fcbb0f5ecdd878b0e1d1d0baa861846f1251244fc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 45
        echo "<div class=\"form-group\">";
        // line 46
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 47
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 49
        echo "</div>";
        // line 50
        echo "</div>";
        
        $__internal_9b52453cdf5fa2e9fa8af7fcbb0f5ecdd878b0e1d1d0baa861846f1251244fc4->leave($__internal_9b52453cdf5fa2e9fa8af7fcbb0f5ecdd878b0e1d1d0baa861846f1251244fc4_prof);

        
        $__internal_281b108188532fa26effdcf22f18f5a0c2afbf100066320d9fb8696878620475->leave($__internal_281b108188532fa26effdcf22f18f5a0c2afbf100066320d9fb8696878620475_prof);

    }

    // line 53
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_6fd007d87517662ed77126ec4e34eeaceff3229663097c2c4a24ad0efd207115 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fd007d87517662ed77126ec4e34eeaceff3229663097c2c4a24ad0efd207115->enter($__internal_6fd007d87517662ed77126ec4e34eeaceff3229663097c2c4a24ad0efd207115_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_85361df8c39101b9c1195fe43f3ad61ae7ce3e16e0a50e40c7342e4a352dcaf4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85361df8c39101b9c1195fe43f3ad61ae7ce3e16e0a50e40c7342e4a352dcaf4->enter($__internal_85361df8c39101b9c1195fe43f3ad61ae7ce3e16e0a50e40c7342e4a352dcaf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        // line 54
        echo "col-sm-10";
        
        $__internal_85361df8c39101b9c1195fe43f3ad61ae7ce3e16e0a50e40c7342e4a352dcaf4->leave($__internal_85361df8c39101b9c1195fe43f3ad61ae7ce3e16e0a50e40c7342e4a352dcaf4_prof);

        
        $__internal_6fd007d87517662ed77126ec4e34eeaceff3229663097c2c4a24ad0efd207115->leave($__internal_6fd007d87517662ed77126ec4e34eeaceff3229663097c2c4a24ad0efd207115_prof);

    }

    // line 57
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_35e19fbadc057f454fdca3885b1887032b82a107c42f20545dd9600ef4e5266e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35e19fbadc057f454fdca3885b1887032b82a107c42f20545dd9600ef4e5266e->enter($__internal_35e19fbadc057f454fdca3885b1887032b82a107c42f20545dd9600ef4e5266e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_f74547441d758b2f1c9b4034f4a9bc96b7532e150b22d53f314177d06f0df566 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f74547441d758b2f1c9b4034f4a9bc96b7532e150b22d53f314177d06f0df566->enter($__internal_f74547441d758b2f1c9b4034f4a9bc96b7532e150b22d53f314177d06f0df566_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 58
        echo "<div class=\"form-group";
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 59
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 60
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 63
        echo "</div>";
        // line 64
        echo "</div>";
        
        $__internal_f74547441d758b2f1c9b4034f4a9bc96b7532e150b22d53f314177d06f0df566->leave($__internal_f74547441d758b2f1c9b4034f4a9bc96b7532e150b22d53f314177d06f0df566_prof);

        
        $__internal_35e19fbadc057f454fdca3885b1887032b82a107c42f20545dd9600ef4e5266e->leave($__internal_35e19fbadc057f454fdca3885b1887032b82a107c42f20545dd9600ef4e5266e_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  320 => 64,  318 => 63,  316 => 62,  314 => 61,  310 => 60,  306 => 59,  300 => 58,  291 => 57,  281 => 54,  272 => 53,  262 => 50,  260 => 49,  258 => 48,  254 => 47,  250 => 46,  248 => 45,  239 => 44,  229 => 41,  227 => 40,  225 => 39,  221 => 38,  217 => 37,  215 => 36,  206 => 35,  196 => 32,  193 => 31,  191 => 30,  189 => 29,  185 => 28,  183 => 27,  177 => 26,  168 => 25,  158 => 20,  149 => 19,  138 => 15,  136 => 14,  131 => 12,  129 => 11,  120 => 10,  110 => 5,  108 => 4,  99 => 3,  89 => 57,  86 => 56,  84 => 53,  81 => 52,  79 => 44,  76 => 43,  74 => 35,  71 => 34,  69 => 25,  66 => 24,  63 => 22,  61 => 19,  58 => 18,  56 => 10,  53 => 9,  50 => 7,  48 => 3,  45 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_3_layout.html.twig\" %}

{% block form_start -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-horizontal')|trim}) %}
    {{- parent() -}}
{%- endblock form_start %}

{# Labels #}

{% block form_label -%}
    {%- if label is same as(false) -%}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {%- else -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) -%}
        {{- parent() -}}
    {%- endif -%}
{%- endblock form_label %}

{% block form_label_class -%}
col-sm-2
{%- endblock form_label_class %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>
{##}</div>
{%- endblock form_row %}

{% block submit_row -%}
    <div class=\"form-group\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock submit_row %}

{% block reset_row -%}
    <div class=\"form-group\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock reset_row %}

{% block form_group_class -%}
col-sm-10
{%- endblock form_group_class %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>{#--#}
    </div>
{%- endblock checkbox_row %}", "bootstrap_3_horizontal_layout.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_3_horizontal_layout.html.twig");
    }
}
