<?php

/* default/addGrave.html.twig */
class __TwigTemplate_bb79519156609692daa996663b205f0fe2a3a468ebd68c27da78b3c9ad884dc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6483ee629080f0bc00640d6a6ec9e78c0eee9875973f5bdc6ce402a6bfa9e811 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6483ee629080f0bc00640d6a6ec9e78c0eee9875973f5bdc6ce402a6bfa9e811->enter($__internal_6483ee629080f0bc00640d6a6ec9e78c0eee9875973f5bdc6ce402a6bfa9e811_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/addGrave.html.twig"));

        $__internal_63dbfb53a6ef65ce0815d02ff7dd88a1979aaef7acfd0e197c5be27a4b1dcf3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63dbfb53a6ef65ce0815d02ff7dd88a1979aaef7acfd0e197c5be27a4b1dcf3c->enter($__internal_63dbfb53a6ef65ce0815d02ff7dd88a1979aaef7acfd0e197c5be27a4b1dcf3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/addGrave.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "default/addGrave.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_6483ee629080f0bc00640d6a6ec9e78c0eee9875973f5bdc6ce402a6bfa9e811->leave($__internal_6483ee629080f0bc00640d6a6ec9e78c0eee9875973f5bdc6ce402a6bfa9e811_prof);

        
        $__internal_63dbfb53a6ef65ce0815d02ff7dd88a1979aaef7acfd0e197c5be27a4b1dcf3c->leave($__internal_63dbfb53a6ef65ce0815d02ff7dd88a1979aaef7acfd0e197c5be27a4b1dcf3c_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_58adffb8537505a479d60c7565811b0974c0ba37f251b1b484e4fce74bfcc6cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58adffb8537505a479d60c7565811b0974c0ba37f251b1b484e4fce74bfcc6cd->enter($__internal_58adffb8537505a479d60c7565811b0974c0ba37f251b1b484e4fce74bfcc6cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a59dc97097ce39a621188924a28ccb2c4b2455e09afe40e40c158b6904730145 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a59dc97097ce39a621188924a28ccb2c4b2455e09afe40e40c158b6904730145->enter($__internal_a59dc97097ce39a621188924a28ccb2c4b2455e09afe40e40c158b6904730145_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"form-horizontal\" >

        ";
        // line 8
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "flashes", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 9
            echo "
            ";
            // line 10
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "

        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "        ";
        // line 14
        echo "        ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    </div>
";
        
        $__internal_a59dc97097ce39a621188924a28ccb2c4b2455e09afe40e40c158b6904730145->leave($__internal_a59dc97097ce39a621188924a28ccb2c4b2455e09afe40e40c158b6904730145_prof);

        
        $__internal_58adffb8537505a479d60c7565811b0974c0ba37f251b1b484e4fce74bfcc6cd->leave($__internal_58adffb8537505a479d60c7565811b0974c0ba37f251b1b484e4fce74bfcc6cd_prof);

    }

    public function getTemplateName()
    {
        return "default/addGrave.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 16,  78 => 15,  73 => 14,  71 => 13,  62 => 10,  59 => 9,  54 => 8,  49 => 4,  31 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'base.html.twig' %}

{% block body %}

    <div class=\"form-horizontal\" >

        {#{% if app.flashes('notice') %}#}
        {% for message in app.flashes('notice') %}

            {{ message }}

        {% endfor %}
        {#{% endif %}#}
        {{ form_start(form) }}
        {{ form_widget(form) }}
        {{ form_end(form) }}

    </div>
{% endblock %}", "default/addGrave.html.twig", "C:\\projects\\graveyard\\app\\Resources\\views\\default\\addGrave.html.twig");
    }
}
