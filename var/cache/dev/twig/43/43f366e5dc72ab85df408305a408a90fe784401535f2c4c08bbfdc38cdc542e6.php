<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_03f16f7935efc3b1571bcde34bde9ba802edc11a2c7439350b6aca1ef39ef8a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bca499e6aa6aa0ac3acf4c9787a0df3f3262a1cfcf2dc945e0af6914d302baf4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bca499e6aa6aa0ac3acf4c9787a0df3f3262a1cfcf2dc945e0af6914d302baf4->enter($__internal_bca499e6aa6aa0ac3acf4c9787a0df3f3262a1cfcf2dc945e0af6914d302baf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $__internal_c069d01c84e848d26b1821f0a0c49fa92a5fa0e7fadf9198fd9816f9718d28b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c069d01c84e848d26b1821f0a0c49fa92a5fa0e7fadf9198fd9816f9718d28b6->enter($__internal_c069d01c84e848d26b1821f0a0c49fa92a5fa0e7fadf9198fd9816f9718d28b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bca499e6aa6aa0ac3acf4c9787a0df3f3262a1cfcf2dc945e0af6914d302baf4->leave($__internal_bca499e6aa6aa0ac3acf4c9787a0df3f3262a1cfcf2dc945e0af6914d302baf4_prof);

        
        $__internal_c069d01c84e848d26b1821f0a0c49fa92a5fa0e7fadf9198fd9816f9718d28b6->leave($__internal_c069d01c84e848d26b1821f0a0c49fa92a5fa0e7fadf9198fd9816f9718d28b6_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_fc935823c0ada891a933428723e8da46791095be8602af95747be2e5dc18a42f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc935823c0ada891a933428723e8da46791095be8602af95747be2e5dc18a42f->enter($__internal_fc935823c0ada891a933428723e8da46791095be8602af95747be2e5dc18a42f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_841ded2563cd84cb00773571f1958679be4c6e596fceec24657e5dd07c9bdeb2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_841ded2563cd84cb00773571f1958679be4c6e596fceec24657e5dd07c9bdeb2->enter($__internal_841ded2563cd84cb00773571f1958679be4c6e596fceec24657e5dd07c9bdeb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_841ded2563cd84cb00773571f1958679be4c6e596fceec24657e5dd07c9bdeb2->leave($__internal_841ded2563cd84cb00773571f1958679be4c6e596fceec24657e5dd07c9bdeb2_prof);

        
        $__internal_fc935823c0ada891a933428723e8da46791095be8602af95747be2e5dc18a42f->leave($__internal_fc935823c0ada891a933428723e8da46791095be8602af95747be2e5dc18a42f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_4441fcc03441caf87970a3288bb5595c643e39eb3dc9eeafdb96443d050ef30d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4441fcc03441caf87970a3288bb5595c643e39eb3dc9eeafdb96443d050ef30d->enter($__internal_4441fcc03441caf87970a3288bb5595c643e39eb3dc9eeafdb96443d050ef30d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a517c982a79ba864bbc89a6e56986354d953b296a63d627ddad9d2943fe09c0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a517c982a79ba864bbc89a6e56986354d953b296a63d627ddad9d2943fe09c0a->enter($__internal_a517c982a79ba864bbc89a6e56986354d953b296a63d627ddad9d2943fe09c0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_a517c982a79ba864bbc89a6e56986354d953b296a63d627ddad9d2943fe09c0a->leave($__internal_a517c982a79ba864bbc89a6e56986354d953b296a63d627ddad9d2943fe09c0a_prof);

        
        $__internal_4441fcc03441caf87970a3288bb5595c643e39eb3dc9eeafdb96443d050ef30d->leave($__internal_4441fcc03441caf87970a3288bb5595c643e39eb3dc9eeafdb96443d050ef30d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "@WebProfiler/Profiler/toolbar_redirect.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\toolbar_redirect.html.twig");
    }
}
