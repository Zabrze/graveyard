<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_412930169cab5812e4c6f9ba98c814a7b0977534048d3ddfcdd45ed6e523db62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1706bdef099428749cac0113b596cac6ded29f861ca23fa48549f9482718cd6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1706bdef099428749cac0113b596cac6ded29f861ca23fa48549f9482718cd6b->enter($__internal_1706bdef099428749cac0113b596cac6ded29f861ca23fa48549f9482718cd6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_a50a149c78e383b0be45b9e1f7cbe2df8b544399d3a62fb3cb0a9da9b3b8a2cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a50a149c78e383b0be45b9e1f7cbe2df8b544399d3a62fb3cb0a9da9b3b8a2cf->enter($__internal_a50a149c78e383b0be45b9e1f7cbe2df8b544399d3a62fb3cb0a9da9b3b8a2cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_1706bdef099428749cac0113b596cac6ded29f861ca23fa48549f9482718cd6b->leave($__internal_1706bdef099428749cac0113b596cac6ded29f861ca23fa48549f9482718cd6b_prof);

        
        $__internal_a50a149c78e383b0be45b9e1f7cbe2df8b544399d3a62fb3cb0a9da9b3b8a2cf->leave($__internal_a50a149c78e383b0be45b9e1f7cbe2df8b544399d3a62fb3cb0a9da9b3b8a2cf_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_23bb840cdebd681cfe9cb89242028029874e0fb681b35578a0d0ce6a6f49b019 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23bb840cdebd681cfe9cb89242028029874e0fb681b35578a0d0ce6a6f49b019->enter($__internal_23bb840cdebd681cfe9cb89242028029874e0fb681b35578a0d0ce6a6f49b019_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_8b8a2472c94f9725c373fb1fb350693652820af7eb0e7ca648c27e3734624ca8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b8a2472c94f9725c373fb1fb350693652820af7eb0e7ca648c27e3734624ca8->enter($__internal_8b8a2472c94f9725c373fb1fb350693652820af7eb0e7ca648c27e3734624ca8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_8b8a2472c94f9725c373fb1fb350693652820af7eb0e7ca648c27e3734624ca8->leave($__internal_8b8a2472c94f9725c373fb1fb350693652820af7eb0e7ca648c27e3734624ca8_prof);

        
        $__internal_23bb840cdebd681cfe9cb89242028029874e0fb681b35578a0d0ce6a6f49b019->leave($__internal_23bb840cdebd681cfe9cb89242028029874e0fb681b35578a0d0ce6a6f49b019_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_6dbd9281eca7e081ee12790f6ada55e410013a64c3b9bbd7302c357936bdd5ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6dbd9281eca7e081ee12790f6ada55e410013a64c3b9bbd7302c357936bdd5ac->enter($__internal_6dbd9281eca7e081ee12790f6ada55e410013a64c3b9bbd7302c357936bdd5ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_b3d4ce72bb7fb6ab4f51054989c8bc5bf8c65762e8a5afcdfa72e3130f7b55c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3d4ce72bb7fb6ab4f51054989c8bc5bf8c65762e8a5afcdfa72e3130f7b55c6->enter($__internal_b3d4ce72bb7fb6ab4f51054989c8bc5bf8c65762e8a5afcdfa72e3130f7b55c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_b3d4ce72bb7fb6ab4f51054989c8bc5bf8c65762e8a5afcdfa72e3130f7b55c6->leave($__internal_b3d4ce72bb7fb6ab4f51054989c8bc5bf8c65762e8a5afcdfa72e3130f7b55c6_prof);

        
        $__internal_6dbd9281eca7e081ee12790f6ada55e410013a64c3b9bbd7302c357936bdd5ac->leave($__internal_6dbd9281eca7e081ee12790f6ada55e410013a64c3b9bbd7302c357936bdd5ac_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_39a40d98c2ade4de22a1c4ec2ac4f568d39154c00930c20148d1a00eca968b28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39a40d98c2ade4de22a1c4ec2ac4f568d39154c00930c20148d1a00eca968b28->enter($__internal_39a40d98c2ade4de22a1c4ec2ac4f568d39154c00930c20148d1a00eca968b28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e909a199ea7dfedfc824e1deac8aa0faf79f2c47fd5e0274507d7fff16ea2214 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e909a199ea7dfedfc824e1deac8aa0faf79f2c47fd5e0274507d7fff16ea2214->enter($__internal_e909a199ea7dfedfc824e1deac8aa0faf79f2c47fd5e0274507d7fff16ea2214_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_e909a199ea7dfedfc824e1deac8aa0faf79f2c47fd5e0274507d7fff16ea2214->leave($__internal_e909a199ea7dfedfc824e1deac8aa0faf79f2c47fd5e0274507d7fff16ea2214_prof);

        
        $__internal_39a40d98c2ade4de22a1c4ec2ac4f568d39154c00930c20148d1a00eca968b28->leave($__internal_39a40d98c2ade4de22a1c4ec2ac4f568d39154c00930c20148d1a00eca968b28_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
