<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_2ce888abc8756bc87448f14ec3223a891a7272800ed0cdaaf8468d599b636a4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0c04cd6b2681ff31a1a302c01d0d7f8392cb62dc6dfb514f9bcd6d255e8e0f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0c04cd6b2681ff31a1a302c01d0d7f8392cb62dc6dfb514f9bcd6d255e8e0f4->enter($__internal_e0c04cd6b2681ff31a1a302c01d0d7f8392cb62dc6dfb514f9bcd6d255e8e0f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_8e63f753a7626267de9c40eaa1f3ac74c3db76a6877766912e527d160fd01bce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e63f753a7626267de9c40eaa1f3ac74c3db76a6877766912e527d160fd01bce->enter($__internal_8e63f753a7626267de9c40eaa1f3ac74c3db76a6877766912e527d160fd01bce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_e0c04cd6b2681ff31a1a302c01d0d7f8392cb62dc6dfb514f9bcd6d255e8e0f4->leave($__internal_e0c04cd6b2681ff31a1a302c01d0d7f8392cb62dc6dfb514f9bcd6d255e8e0f4_prof);

        
        $__internal_8e63f753a7626267de9c40eaa1f3ac74c3db76a6877766912e527d160fd01bce->leave($__internal_8e63f753a7626267de9c40eaa1f3ac74c3db76a6877766912e527d160fd01bce_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rows.html.php");
    }
}
