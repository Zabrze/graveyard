<?php

/* @WebProfiler/Profiler/info.html.twig */
class __TwigTemplate_6dd5fde8c04f95064acde6414427ce19f673a4f093528a1d70952c8ae57addfe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Profiler/info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6d3b213c9cb8dfccf63f4398f0bcbbba5cbb48af03f84f826771554ff3d2f36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6d3b213c9cb8dfccf63f4398f0bcbbba5cbb48af03f84f826771554ff3d2f36->enter($__internal_e6d3b213c9cb8dfccf63f4398f0bcbbba5cbb48af03f84f826771554ff3d2f36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        $__internal_2a58483400471bc42370ebe333579697b689b718e5de2ddb1b3a223638d6de21 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a58483400471bc42370ebe333579697b689b718e5de2ddb1b3a223638d6de21->enter($__internal_2a58483400471bc42370ebe333579697b689b718e5de2ddb1b3a223638d6de21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e6d3b213c9cb8dfccf63f4398f0bcbbba5cbb48af03f84f826771554ff3d2f36->leave($__internal_e6d3b213c9cb8dfccf63f4398f0bcbbba5cbb48af03f84f826771554ff3d2f36_prof);

        
        $__internal_2a58483400471bc42370ebe333579697b689b718e5de2ddb1b3a223638d6de21->leave($__internal_2a58483400471bc42370ebe333579697b689b718e5de2ddb1b3a223638d6de21_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_a235668dcac17fe012de224adc972eda4486219ea50f92fba4bed2ea388dde1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a235668dcac17fe012de224adc972eda4486219ea50f92fba4bed2ea388dde1d->enter($__internal_a235668dcac17fe012de224adc972eda4486219ea50f92fba4bed2ea388dde1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_8ebb27f235d246c9d131a55a6d8e86ea64c65ee9877865c4a878f292e1fb4339 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ebb27f235d246c9d131a55a6d8e86ea64c65ee9877865c4a878f292e1fb4339->enter($__internal_8ebb27f235d246c9d131a55a6d8e86ea64c65ee9877865c4a878f292e1fb4339_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_8ebb27f235d246c9d131a55a6d8e86ea64c65ee9877865c4a878f292e1fb4339->leave($__internal_8ebb27f235d246c9d131a55a6d8e86ea64c65ee9877865c4a878f292e1fb4339_prof);

        
        $__internal_a235668dcac17fe012de224adc972eda4486219ea50f92fba4bed2ea388dde1d->leave($__internal_a235668dcac17fe012de224adc972eda4486219ea50f92fba4bed2ea388dde1d_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_85d0c722b130683f9ae091190c844e84e128030196a026525102423d17f6b9ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85d0c722b130683f9ae091190c844e84e128030196a026525102423d17f6b9ef->enter($__internal_85d0c722b130683f9ae091190c844e84e128030196a026525102423d17f6b9ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8a023a74cfec3f763ca1fa4ca8ca883f3668bce21b8551f8bcc26296b9e07411 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a023a74cfec3f763ca1fa4ca8ca883f3668bce21b8551f8bcc26296b9e07411->enter($__internal_8a023a74cfec3f763ca1fa4ca8ca883f3668bce21b8551f8bcc26296b9e07411_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_8a023a74cfec3f763ca1fa4ca8ca883f3668bce21b8551f8bcc26296b9e07411->leave($__internal_8a023a74cfec3f763ca1fa4ca8ca883f3668bce21b8551f8bcc26296b9e07411_prof);

        
        $__internal_85d0c722b130683f9ae091190c844e84e128030196a026525102423d17f6b9ef->leave($__internal_85d0c722b130683f9ae091190c844e84e128030196a026525102423d17f6b9ef_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "@WebProfiler/Profiler/info.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\info.html.twig");
    }
}
