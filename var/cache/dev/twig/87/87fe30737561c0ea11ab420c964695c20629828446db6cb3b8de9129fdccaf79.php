<?php

/* bootstrap_3_layout.html.twig */
class __TwigTemplate_6b630d0cc5a1fa07ef2192b4cb0ffb4f437fa58e1edb417d11d22ee443043a7a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_base_layout.html.twig", "bootstrap_3_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_base_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'button_widget' => array($this, 'block_button_widget'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'form_label' => array($this, 'block_form_label'),
                'choice_label' => array($this, 'block_choice_label'),
                'checkbox_label' => array($this, 'block_checkbox_label'),
                'radio_label' => array($this, 'block_radio_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'choice_row' => array($this, 'block_choice_row'),
                'date_row' => array($this, 'block_date_row'),
                'time_row' => array($this, 'block_time_row'),
                'datetime_row' => array($this, 'block_datetime_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'form_errors' => array($this, 'block_form_errors'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1e705f7ae5fe66fb0ac2c368b23cb1a6a4480bca5c87b13a7c09516c55456ef1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e705f7ae5fe66fb0ac2c368b23cb1a6a4480bca5c87b13a7c09516c55456ef1->enter($__internal_1e705f7ae5fe66fb0ac2c368b23cb1a6a4480bca5c87b13a7c09516c55456ef1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        $__internal_ea5d40aa42cbf292d7fa8cd2fc992368554d8ff61a3b8db12273919481fe64c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea5d40aa42cbf292d7fa8cd2fc992368554d8ff61a3b8db12273919481fe64c0->enter($__internal_ea5d40aa42cbf292d7fa8cd2fc992368554d8ff61a3b8db12273919481fe64c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('button_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 27
        echo "
";
        // line 28
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 38
        echo "
";
        // line 40
        echo "
";
        // line 41
        $this->displayBlock('form_label', $context, $blocks);
        // line 45
        echo "
";
        // line 46
        $this->displayBlock('choice_label', $context, $blocks);
        // line 51
        echo "
";
        // line 52
        $this->displayBlock('checkbox_label', $context, $blocks);
        // line 57
        echo "
";
        // line 58
        $this->displayBlock('radio_label', $context, $blocks);
        // line 63
        echo "
";
        // line 64
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 88
        echo "
";
        // line 90
        echo "
";
        // line 91
        $this->displayBlock('form_row', $context, $blocks);
        // line 98
        echo "
";
        // line 99
        $this->displayBlock('button_row', $context, $blocks);
        // line 104
        echo "
";
        // line 105
        $this->displayBlock('choice_row', $context, $blocks);
        // line 109
        echo "
";
        // line 110
        $this->displayBlock('date_row', $context, $blocks);
        // line 114
        echo "
";
        // line 115
        $this->displayBlock('time_row', $context, $blocks);
        // line 119
        echo "
";
        // line 120
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 124
        echo "
";
        // line 125
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 131
        echo "
";
        // line 132
        $this->displayBlock('radio_row', $context, $blocks);
        // line 138
        echo "
";
        // line 140
        echo "
";
        // line 141
        $this->displayBlock('form_errors', $context, $blocks);
        
        $__internal_1e705f7ae5fe66fb0ac2c368b23cb1a6a4480bca5c87b13a7c09516c55456ef1->leave($__internal_1e705f7ae5fe66fb0ac2c368b23cb1a6a4480bca5c87b13a7c09516c55456ef1_prof);

        
        $__internal_ea5d40aa42cbf292d7fa8cd2fc992368554d8ff61a3b8db12273919481fe64c0->leave($__internal_ea5d40aa42cbf292d7fa8cd2fc992368554d8ff61a3b8db12273919481fe64c0_prof);

    }

    // line 5
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_af5438c41e8c02ed7cda07bb07f7a3aaa43519f8af9373589d72e68b85ed2e65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af5438c41e8c02ed7cda07bb07f7a3aaa43519f8af9373589d72e68b85ed2e65->enter($__internal_af5438c41e8c02ed7cda07bb07f7a3aaa43519f8af9373589d72e68b85ed2e65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_ef8860969335fab0b98d6ad250c6002254ca30aa69342b35c9f47243a3ab9878 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef8860969335fab0b98d6ad250c6002254ca30aa69342b35c9f47243a3ab9878->enter($__internal_ef8860969335fab0b98d6ad250c6002254ca30aa69342b35c9f47243a3ab9878_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 6
        if (( !array_key_exists("type", $context) || !twig_in_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), array(0 => "file", 1 => "hidden")))) {
            // line 7
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        }
        // line 9
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_ef8860969335fab0b98d6ad250c6002254ca30aa69342b35c9f47243a3ab9878->leave($__internal_ef8860969335fab0b98d6ad250c6002254ca30aa69342b35c9f47243a3ab9878_prof);

        
        $__internal_af5438c41e8c02ed7cda07bb07f7a3aaa43519f8af9373589d72e68b85ed2e65->leave($__internal_af5438c41e8c02ed7cda07bb07f7a3aaa43519f8af9373589d72e68b85ed2e65_prof);

    }

    // line 12
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_5a99330df01337aa5ea91dca94a5c7a91484335c4cc8cf91fa0e669303107ad9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a99330df01337aa5ea91dca94a5c7a91484335c4cc8cf91fa0e669303107ad9->enter($__internal_5a99330df01337aa5ea91dca94a5c7a91484335c4cc8cf91fa0e669303107ad9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_b86ce2cfe127a9a3d6dae1856a2a869eb58f9a6f7aa57442a7d4acd932dfe6a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b86ce2cfe127a9a3d6dae1856a2a869eb58f9a6f7aa57442a7d4acd932dfe6a6->enter($__internal_b86ce2cfe127a9a3d6dae1856a2a869eb58f9a6f7aa57442a7d4acd932dfe6a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 13
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "btn-default")) : ("btn-default")) . " btn"))));
        // line 14
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_b86ce2cfe127a9a3d6dae1856a2a869eb58f9a6f7aa57442a7d4acd932dfe6a6->leave($__internal_b86ce2cfe127a9a3d6dae1856a2a869eb58f9a6f7aa57442a7d4acd932dfe6a6_prof);

        
        $__internal_5a99330df01337aa5ea91dca94a5c7a91484335c4cc8cf91fa0e669303107ad9->leave($__internal_5a99330df01337aa5ea91dca94a5c7a91484335c4cc8cf91fa0e669303107ad9_prof);

    }

    // line 17
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_68e1086b445af57459b3f0cbb2af598d3af96bc42248d041b3674c4b6e995e16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_68e1086b445af57459b3f0cbb2af598d3af96bc42248d041b3674c4b6e995e16->enter($__internal_68e1086b445af57459b3f0cbb2af598d3af96bc42248d041b3674c4b6e995e16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_02ad195cacd7221c929eeff9db032c32a905a67d1a8ba56064da7f06ebb3529f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02ad195cacd7221c929eeff9db032c32a905a67d1a8ba56064da7f06ebb3529f->enter($__internal_02ad195cacd7221c929eeff9db032c32a905a67d1a8ba56064da7f06ebb3529f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 18
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) : ((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
        // line 19
        if (twig_in_filter("checkbox-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 20
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } else {
            // line 22
            echo "<div class=\"checkbox\">";
            // line 23
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 24
            echo "</div>";
        }
        
        $__internal_02ad195cacd7221c929eeff9db032c32a905a67d1a8ba56064da7f06ebb3529f->leave($__internal_02ad195cacd7221c929eeff9db032c32a905a67d1a8ba56064da7f06ebb3529f_prof);

        
        $__internal_68e1086b445af57459b3f0cbb2af598d3af96bc42248d041b3674c4b6e995e16->leave($__internal_68e1086b445af57459b3f0cbb2af598d3af96bc42248d041b3674c4b6e995e16_prof);

    }

    // line 28
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_d0b18d48322925ea55c044cddb14e7dcfbd5e5786278f16ab3e0c6f5e9e243ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0b18d48322925ea55c044cddb14e7dcfbd5e5786278f16ab3e0c6f5e9e243ad->enter($__internal_d0b18d48322925ea55c044cddb14e7dcfbd5e5786278f16ab3e0c6f5e9e243ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_fe948ade9f1c7932f6fac71991bb26307bfdda5eb2a5d7b5b9073a3c2ca1b44d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe948ade9f1c7932f6fac71991bb26307bfdda5eb2a5d7b5b9073a3c2ca1b44d->enter($__internal_fe948ade9f1c7932f6fac71991bb26307bfdda5eb2a5d7b5b9073a3c2ca1b44d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 29
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) : ((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
        // line 30
        if (twig_in_filter("radio-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 33
            echo "<div class=\"radio\">";
            // line 34
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 35
            echo "</div>";
        }
        
        $__internal_fe948ade9f1c7932f6fac71991bb26307bfdda5eb2a5d7b5b9073a3c2ca1b44d->leave($__internal_fe948ade9f1c7932f6fac71991bb26307bfdda5eb2a5d7b5b9073a3c2ca1b44d_prof);

        
        $__internal_d0b18d48322925ea55c044cddb14e7dcfbd5e5786278f16ab3e0c6f5e9e243ad->leave($__internal_d0b18d48322925ea55c044cddb14e7dcfbd5e5786278f16ab3e0c6f5e9e243ad_prof);

    }

    // line 41
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_775dc531e786cee029fb77b4ea86b6b6b0f9db28fb9f4f4e6e30f402a5e74086 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_775dc531e786cee029fb77b4ea86b6b6b0f9db28fb9f4f4e6e30f402a5e74086->enter($__internal_775dc531e786cee029fb77b4ea86b6b6b0f9db28fb9f4f4e6e30f402a5e74086_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_01ec5e7bc4f87bf5fe661dfdc5aa628a3b9d04babcf94fb6643aaa97f60469fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01ec5e7bc4f87bf5fe661dfdc5aa628a3b9d04babcf94fb6643aaa97f60469fb->enter($__internal_01ec5e7bc4f87bf5fe661dfdc5aa628a3b9d04babcf94fb6643aaa97f60469fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 42
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " control-label"))));
        // line 43
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_01ec5e7bc4f87bf5fe661dfdc5aa628a3b9d04babcf94fb6643aaa97f60469fb->leave($__internal_01ec5e7bc4f87bf5fe661dfdc5aa628a3b9d04babcf94fb6643aaa97f60469fb_prof);

        
        $__internal_775dc531e786cee029fb77b4ea86b6b6b0f9db28fb9f4f4e6e30f402a5e74086->leave($__internal_775dc531e786cee029fb77b4ea86b6b6b0f9db28fb9f4f4e6e30f402a5e74086_prof);

    }

    // line 46
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_af56151a76b10512b8d84002525afc5d277bd7b5fe022e3391a03e6386c44aa1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af56151a76b10512b8d84002525afc5d277bd7b5fe022e3391a03e6386c44aa1->enter($__internal_af56151a76b10512b8d84002525afc5d277bd7b5fe022e3391a03e6386c44aa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_26900b4e67fbaf7526aa904be6417793b95c0cfddd95ea6f958d10f59e990b71 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26900b4e67fbaf7526aa904be6417793b95c0cfddd95ea6f958d10f59e990b71->enter($__internal_26900b4e67fbaf7526aa904be6417793b95c0cfddd95ea6f958d10f59e990b71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 48
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(twig_replace_filter((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 49
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_26900b4e67fbaf7526aa904be6417793b95c0cfddd95ea6f958d10f59e990b71->leave($__internal_26900b4e67fbaf7526aa904be6417793b95c0cfddd95ea6f958d10f59e990b71_prof);

        
        $__internal_af56151a76b10512b8d84002525afc5d277bd7b5fe022e3391a03e6386c44aa1->leave($__internal_af56151a76b10512b8d84002525afc5d277bd7b5fe022e3391a03e6386c44aa1_prof);

    }

    // line 52
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_a474163c98aca11e59daeef8eb713d653f603944b9f51eb684691c131732d0f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a474163c98aca11e59daeef8eb713d653f603944b9f51eb684691c131732d0f0->enter($__internal_a474163c98aca11e59daeef8eb713d653f603944b9f51eb684691c131732d0f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_4f377485c1aacd8cd9a048105fb3a19abd6872504451c8df51f2a7e51ca6c770 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f377485c1aacd8cd9a048105fb3a19abd6872504451c8df51f2a7e51ca6c770->enter($__internal_4f377485c1aacd8cd9a048105fb3a19abd6872504451c8df51f2a7e51ca6c770_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 53
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("for" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
        // line 55
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_4f377485c1aacd8cd9a048105fb3a19abd6872504451c8df51f2a7e51ca6c770->leave($__internal_4f377485c1aacd8cd9a048105fb3a19abd6872504451c8df51f2a7e51ca6c770_prof);

        
        $__internal_a474163c98aca11e59daeef8eb713d653f603944b9f51eb684691c131732d0f0->leave($__internal_a474163c98aca11e59daeef8eb713d653f603944b9f51eb684691c131732d0f0_prof);

    }

    // line 58
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_43b0b67509337f2aef8eb10b2861b3ad5c5745c2ac5ea030e54f4e1831fd2114 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43b0b67509337f2aef8eb10b2861b3ad5c5745c2ac5ea030e54f4e1831fd2114->enter($__internal_43b0b67509337f2aef8eb10b2861b3ad5c5745c2ac5ea030e54f4e1831fd2114_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_88f1a0fe004a7ebcdbc080bc1fced22109c3ccf30a59ca6d0a81e82778dcd492 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88f1a0fe004a7ebcdbc080bc1fced22109c3ccf30a59ca6d0a81e82778dcd492->enter($__internal_88f1a0fe004a7ebcdbc080bc1fced22109c3ccf30a59ca6d0a81e82778dcd492_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 59
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("for" => (isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
        // line 61
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_88f1a0fe004a7ebcdbc080bc1fced22109c3ccf30a59ca6d0a81e82778dcd492->leave($__internal_88f1a0fe004a7ebcdbc080bc1fced22109c3ccf30a59ca6d0a81e82778dcd492_prof);

        
        $__internal_43b0b67509337f2aef8eb10b2861b3ad5c5745c2ac5ea030e54f4e1831fd2114->leave($__internal_43b0b67509337f2aef8eb10b2861b3ad5c5745c2ac5ea030e54f4e1831fd2114_prof);

    }

    // line 64
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_c5317b915beae8cac510cf37e9bd7b829ced83b0defcdd7141f00b8d127b4ef1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5317b915beae8cac510cf37e9bd7b829ced83b0defcdd7141f00b8d127b4ef1->enter($__internal_c5317b915beae8cac510cf37e9bd7b829ced83b0defcdd7141f00b8d127b4ef1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_7508b51226954dc9c28a85842ea9a54378f04eef47c13aa12882628f9d6ffdc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7508b51226954dc9c28a85842ea9a54378f04eef47c13aa12882628f9d6ffdc7->enter($__internal_7508b51226954dc9c28a85842ea9a54378f04eef47c13aa12882628f9d6ffdc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 66
        if (array_key_exists("widget", $context)) {
            // line 67
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                // line 68
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
            }
            // line 70
            if (array_key_exists("parent_label_class", $context)) {
                // line 71
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " ") . (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class"))))));
            }
            // line 73
            if (( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false) && twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))))) {
                // line 74
                if ( !twig_test_empty((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")))) {
                    // line 75
                    $context["label"] = twig_replace_filter((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")), array("%name%" =>                     // line 76
(isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "%id%" =>                     // line 77
(isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                } else {
                    // line 80
                    $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                }
            }
            // line 83
            echo "<label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 84
            echo (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) ? (((((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) ? ((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 85
            echo "</label>";
        }
        
        $__internal_7508b51226954dc9c28a85842ea9a54378f04eef47c13aa12882628f9d6ffdc7->leave($__internal_7508b51226954dc9c28a85842ea9a54378f04eef47c13aa12882628f9d6ffdc7_prof);

        
        $__internal_c5317b915beae8cac510cf37e9bd7b829ced83b0defcdd7141f00b8d127b4ef1->leave($__internal_c5317b915beae8cac510cf37e9bd7b829ced83b0defcdd7141f00b8d127b4ef1_prof);

    }

    // line 91
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_c4896b3a9d5ea94e808ede8441511873541e289d15dca0e991f2ae81bd9ef459 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4896b3a9d5ea94e808ede8441511873541e289d15dca0e991f2ae81bd9ef459->enter($__internal_c4896b3a9d5ea94e808ede8441511873541e289d15dca0e991f2ae81bd9ef459_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_1351f314e1c87d64fc1a45ed58989cefd8f59d748ee4cd86a1f682249592f629 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1351f314e1c87d64fc1a45ed58989cefd8f59d748ee4cd86a1f682249592f629->enter($__internal_1351f314e1c87d64fc1a45ed58989cefd8f59d748ee4cd86a1f682249592f629_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 92
        echo "<div class=\"form-group";
        if ((( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter((isset($context["force_error"]) ? $context["force_error"] : $this->getContext($context, "force_error")), false)) : (false))) &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 93
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        // line 94
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 95
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 96
        echo "</div>";
        
        $__internal_1351f314e1c87d64fc1a45ed58989cefd8f59d748ee4cd86a1f682249592f629->leave($__internal_1351f314e1c87d64fc1a45ed58989cefd8f59d748ee4cd86a1f682249592f629_prof);

        
        $__internal_c4896b3a9d5ea94e808ede8441511873541e289d15dca0e991f2ae81bd9ef459->leave($__internal_c4896b3a9d5ea94e808ede8441511873541e289d15dca0e991f2ae81bd9ef459_prof);

    }

    // line 99
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_2639436baff0ddebbf65b2ac3edca91dfaff5b8607fa3b899bb5527091395eb5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2639436baff0ddebbf65b2ac3edca91dfaff5b8607fa3b899bb5527091395eb5->enter($__internal_2639436baff0ddebbf65b2ac3edca91dfaff5b8607fa3b899bb5527091395eb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_7ab3a07659d8f8c712775e828c8685b8f2ad2c89ab7fc10f45255b0c0c7a5452 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ab3a07659d8f8c712775e828c8685b8f2ad2c89ab7fc10f45255b0c0c7a5452->enter($__internal_7ab3a07659d8f8c712775e828c8685b8f2ad2c89ab7fc10f45255b0c0c7a5452_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 100
        echo "<div class=\"form-group\">";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 102
        echo "</div>";
        
        $__internal_7ab3a07659d8f8c712775e828c8685b8f2ad2c89ab7fc10f45255b0c0c7a5452->leave($__internal_7ab3a07659d8f8c712775e828c8685b8f2ad2c89ab7fc10f45255b0c0c7a5452_prof);

        
        $__internal_2639436baff0ddebbf65b2ac3edca91dfaff5b8607fa3b899bb5527091395eb5->leave($__internal_2639436baff0ddebbf65b2ac3edca91dfaff5b8607fa3b899bb5527091395eb5_prof);

    }

    // line 105
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_96bb8bb76eb07f1b935d44ded76e2acfc1ac9594cbab9ef97a8cf6aef0d87182 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96bb8bb76eb07f1b935d44ded76e2acfc1ac9594cbab9ef97a8cf6aef0d87182->enter($__internal_96bb8bb76eb07f1b935d44ded76e2acfc1ac9594cbab9ef97a8cf6aef0d87182_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_49f7fdde3dbff57c09e187fd0eb4b2c9af7bbd3b82a3bc65b604212d89dff4dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49f7fdde3dbff57c09e187fd0eb4b2c9af7bbd3b82a3bc65b604212d89dff4dc->enter($__internal_49f7fdde3dbff57c09e187fd0eb4b2c9af7bbd3b82a3bc65b604212d89dff4dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 106
        $context["force_error"] = true;
        // line 107
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_49f7fdde3dbff57c09e187fd0eb4b2c9af7bbd3b82a3bc65b604212d89dff4dc->leave($__internal_49f7fdde3dbff57c09e187fd0eb4b2c9af7bbd3b82a3bc65b604212d89dff4dc_prof);

        
        $__internal_96bb8bb76eb07f1b935d44ded76e2acfc1ac9594cbab9ef97a8cf6aef0d87182->leave($__internal_96bb8bb76eb07f1b935d44ded76e2acfc1ac9594cbab9ef97a8cf6aef0d87182_prof);

    }

    // line 110
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_c08f4d884c2ee8602a3dea646d33bf7773f8409aa0abb1da9f7099ba1f2b66a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c08f4d884c2ee8602a3dea646d33bf7773f8409aa0abb1da9f7099ba1f2b66a6->enter($__internal_c08f4d884c2ee8602a3dea646d33bf7773f8409aa0abb1da9f7099ba1f2b66a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_3b1830688542c89af200a5ba05b02ba692841da9e34336aedfd18adeb13024c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b1830688542c89af200a5ba05b02ba692841da9e34336aedfd18adeb13024c3->enter($__internal_3b1830688542c89af200a5ba05b02ba692841da9e34336aedfd18adeb13024c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 111
        $context["force_error"] = true;
        // line 112
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_3b1830688542c89af200a5ba05b02ba692841da9e34336aedfd18adeb13024c3->leave($__internal_3b1830688542c89af200a5ba05b02ba692841da9e34336aedfd18adeb13024c3_prof);

        
        $__internal_c08f4d884c2ee8602a3dea646d33bf7773f8409aa0abb1da9f7099ba1f2b66a6->leave($__internal_c08f4d884c2ee8602a3dea646d33bf7773f8409aa0abb1da9f7099ba1f2b66a6_prof);

    }

    // line 115
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_46931f77c3828f2ff4de0ca55e10b95a2fa4412b8dbe3155939776362e258e21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46931f77c3828f2ff4de0ca55e10b95a2fa4412b8dbe3155939776362e258e21->enter($__internal_46931f77c3828f2ff4de0ca55e10b95a2fa4412b8dbe3155939776362e258e21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_54ca7a4a009cd47c9d776bce1957cd874d4fdc432b7743d84faf6557a7320fed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54ca7a4a009cd47c9d776bce1957cd874d4fdc432b7743d84faf6557a7320fed->enter($__internal_54ca7a4a009cd47c9d776bce1957cd874d4fdc432b7743d84faf6557a7320fed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 116
        $context["force_error"] = true;
        // line 117
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_54ca7a4a009cd47c9d776bce1957cd874d4fdc432b7743d84faf6557a7320fed->leave($__internal_54ca7a4a009cd47c9d776bce1957cd874d4fdc432b7743d84faf6557a7320fed_prof);

        
        $__internal_46931f77c3828f2ff4de0ca55e10b95a2fa4412b8dbe3155939776362e258e21->leave($__internal_46931f77c3828f2ff4de0ca55e10b95a2fa4412b8dbe3155939776362e258e21_prof);

    }

    // line 120
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_04be2e4508de3581bbf4f4a518e91cd72b063e1bb4da39d46b81bf6e148aebf0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04be2e4508de3581bbf4f4a518e91cd72b063e1bb4da39d46b81bf6e148aebf0->enter($__internal_04be2e4508de3581bbf4f4a518e91cd72b063e1bb4da39d46b81bf6e148aebf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_047d7cda343b735f9e2d7fd3fa6b7a53b30a7a32ff9ff82dc049409e50b5f1a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_047d7cda343b735f9e2d7fd3fa6b7a53b30a7a32ff9ff82dc049409e50b5f1a8->enter($__internal_047d7cda343b735f9e2d7fd3fa6b7a53b30a7a32ff9ff82dc049409e50b5f1a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 121
        $context["force_error"] = true;
        // line 122
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_047d7cda343b735f9e2d7fd3fa6b7a53b30a7a32ff9ff82dc049409e50b5f1a8->leave($__internal_047d7cda343b735f9e2d7fd3fa6b7a53b30a7a32ff9ff82dc049409e50b5f1a8_prof);

        
        $__internal_04be2e4508de3581bbf4f4a518e91cd72b063e1bb4da39d46b81bf6e148aebf0->leave($__internal_04be2e4508de3581bbf4f4a518e91cd72b063e1bb4da39d46b81bf6e148aebf0_prof);

    }

    // line 125
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_c5f5fd4bd8c0f528802fd3f08b6f4c3e43c53aa41efbbf9e6b0a06d760fad4a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5f5fd4bd8c0f528802fd3f08b6f4c3e43c53aa41efbbf9e6b0a06d760fad4a2->enter($__internal_c5f5fd4bd8c0f528802fd3f08b6f4c3e43c53aa41efbbf9e6b0a06d760fad4a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_d423daf145b34badc92ff4945ab1735f8d53e60ce1d83851e9d3a3d399db5482 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d423daf145b34badc92ff4945ab1735f8d53e60ce1d83851e9d3a3d399db5482->enter($__internal_d423daf145b34badc92ff4945ab1735f8d53e60ce1d83851e9d3a3d399db5482_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 126
        echo "<div class=\"form-group";
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 127
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 128
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 129
        echo "</div>";
        
        $__internal_d423daf145b34badc92ff4945ab1735f8d53e60ce1d83851e9d3a3d399db5482->leave($__internal_d423daf145b34badc92ff4945ab1735f8d53e60ce1d83851e9d3a3d399db5482_prof);

        
        $__internal_c5f5fd4bd8c0f528802fd3f08b6f4c3e43c53aa41efbbf9e6b0a06d760fad4a2->leave($__internal_c5f5fd4bd8c0f528802fd3f08b6f4c3e43c53aa41efbbf9e6b0a06d760fad4a2_prof);

    }

    // line 132
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_e628e059cd51e20ff77f5df0a2af4d0d63d95c8cc8e5a79d215bfed9bd97bea1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e628e059cd51e20ff77f5df0a2af4d0d63d95c8cc8e5a79d215bfed9bd97bea1->enter($__internal_e628e059cd51e20ff77f5df0a2af4d0d63d95c8cc8e5a79d215bfed9bd97bea1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_56d38fc2fa931e4874f3cb1bcf98936186b62d68b237bd9dba32ac1e6d5264f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56d38fc2fa931e4874f3cb1bcf98936186b62d68b237bd9dba32ac1e6d5264f5->enter($__internal_56d38fc2fa931e4874f3cb1bcf98936186b62d68b237bd9dba32ac1e6d5264f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 133
        echo "<div class=\"form-group";
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 134
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 135
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 136
        echo "</div>";
        
        $__internal_56d38fc2fa931e4874f3cb1bcf98936186b62d68b237bd9dba32ac1e6d5264f5->leave($__internal_56d38fc2fa931e4874f3cb1bcf98936186b62d68b237bd9dba32ac1e6d5264f5_prof);

        
        $__internal_e628e059cd51e20ff77f5df0a2af4d0d63d95c8cc8e5a79d215bfed9bd97bea1->leave($__internal_e628e059cd51e20ff77f5df0a2af4d0d63d95c8cc8e5a79d215bfed9bd97bea1_prof);

    }

    // line 141
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_403c666723db3e3bf730b54feb4690c466967a1f67e32042c1d319f9a4f59858 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_403c666723db3e3bf730b54feb4690c466967a1f67e32042c1d319f9a4f59858->enter($__internal_403c666723db3e3bf730b54feb4690c466967a1f67e32042c1d319f9a4f59858_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_d92f7555547321a96f092b31ad75e3dc1f78c286af3348b8d9c99dba37d11f9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d92f7555547321a96f092b31ad75e3dc1f78c286af3348b8d9c99dba37d11f9b->enter($__internal_d92f7555547321a96f092b31ad75e3dc1f78c286af3348b8d9c99dba37d11f9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 142
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 143
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")))) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 144
            echo "    <ul class=\"list-unstyled\">";
            // line 145
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 146
                echo "<li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 148
            echo "</ul>
    ";
            // line 149
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")))) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_d92f7555547321a96f092b31ad75e3dc1f78c286af3348b8d9c99dba37d11f9b->leave($__internal_d92f7555547321a96f092b31ad75e3dc1f78c286af3348b8d9c99dba37d11f9b_prof);

        
        $__internal_403c666723db3e3bf730b54feb4690c466967a1f67e32042c1d319f9a4f59858->leave($__internal_403c666723db3e3bf730b54feb4690c466967a1f67e32042c1d319f9a4f59858_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  650 => 149,  647 => 148,  639 => 146,  635 => 145,  633 => 144,  627 => 143,  625 => 142,  616 => 141,  606 => 136,  604 => 135,  602 => 134,  596 => 133,  587 => 132,  577 => 129,  575 => 128,  573 => 127,  567 => 126,  558 => 125,  548 => 122,  546 => 121,  537 => 120,  527 => 117,  525 => 116,  516 => 115,  506 => 112,  504 => 111,  495 => 110,  485 => 107,  483 => 106,  474 => 105,  464 => 102,  462 => 101,  460 => 100,  451 => 99,  441 => 96,  439 => 95,  437 => 94,  435 => 93,  429 => 92,  420 => 91,  409 => 85,  405 => 84,  390 => 83,  386 => 80,  383 => 77,  382 => 76,  381 => 75,  379 => 74,  377 => 73,  374 => 71,  372 => 70,  369 => 68,  367 => 67,  365 => 66,  356 => 64,  346 => 61,  344 => 59,  335 => 58,  325 => 55,  323 => 53,  314 => 52,  304 => 49,  302 => 48,  293 => 46,  283 => 43,  281 => 42,  272 => 41,  261 => 35,  259 => 34,  257 => 33,  254 => 31,  252 => 30,  250 => 29,  241 => 28,  230 => 24,  228 => 23,  226 => 22,  223 => 20,  221 => 19,  219 => 18,  210 => 17,  200 => 14,  198 => 13,  189 => 12,  179 => 9,  176 => 7,  174 => 6,  165 => 5,  155 => 141,  152 => 140,  149 => 138,  147 => 132,  144 => 131,  142 => 125,  139 => 124,  137 => 120,  134 => 119,  132 => 115,  129 => 114,  127 => 110,  124 => 109,  122 => 105,  119 => 104,  117 => 99,  114 => 98,  112 => 91,  109 => 90,  106 => 88,  104 => 64,  101 => 63,  99 => 58,  96 => 57,  94 => 52,  91 => 51,  89 => 46,  86 => 45,  84 => 41,  81 => 40,  78 => 38,  76 => 28,  73 => 27,  71 => 17,  68 => 16,  66 => 12,  63 => 11,  61 => 5,  58 => 4,  55 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_base_layout.html.twig\" %}

{# Widgets #}

{% block form_widget_simple -%}
    {% if type is not defined or type not in ['file', 'hidden'] %}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) -%}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block button_widget -%}
    {%- set attr = attr|merge({class: (attr.class|default('btn-default') ~ ' btn')|trim}) -%}
    {{- parent() -}}
{%- endblock button_widget %}

{% block checkbox_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'checkbox-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"checkbox\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif -%}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {%- if 'radio-inline' in parent_label_class -%}
        {{- form_label(form, null, { widget: parent() }) -}}
    {%- else -%}
        <div class=\"radio\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif -%}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' control-label')|trim}) -%}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {%- set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) -%}
    {{- block('form_label') -}}
{% endblock %}

{% block checkbox_label -%}
    {%- set label_attr = label_attr|merge({'for': id}) -%}

    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {%- set label_attr = label_attr|merge({'for': id}) -%}

    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label -%}
    {# Do not display the label if widget is not defined in order to prevent double label rendering #}
    {%- if widget is defined -%}
        {%- if required -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) -%}
        {%- endif -%}
        {%- if parent_label_class is defined -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) -%}
        {%- endif -%}
        {%- if label is not same as(false) and label is empty -%}
            {%- if label_format is not empty -%}
                {%- set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) -%}
            {%- else -%}
                {% set label = name|humanize %}
            {%- endif -%}
        {%- endif -%}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}
        </label>
    {%- endif -%}
{%- endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock form_row %}

{% block button_row -%}
    <div class=\"form-group\">
        {{- form_widget(form) -}}
    </div>
{%- endblock button_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
    {% if form is not rootform %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
    <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            <li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> {{ error.message }}</li>
        {%- endfor -%}
    </ul>
    {% if form is not rootform %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "bootstrap_3_layout.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_3_layout.html.twig");
    }
}
