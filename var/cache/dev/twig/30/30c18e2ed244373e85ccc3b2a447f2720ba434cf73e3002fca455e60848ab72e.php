<?php

/* @FOSUser/Group/show.html.twig */
class __TwigTemplate_f6ea08e3c4f190f065f46b2e65ad516762f7ed413e69a830f9d987140b9c98a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1cf5890dc65e3d40aaa3e6e099bfe42f5ee61fd2c15d524c55e15fcf5c77809 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1cf5890dc65e3d40aaa3e6e099bfe42f5ee61fd2c15d524c55e15fcf5c77809->enter($__internal_c1cf5890dc65e3d40aaa3e6e099bfe42f5ee61fd2c15d524c55e15fcf5c77809_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $__internal_fb8b0546c13231aff51f9614efdddf1afe1a4626b7573a741dffe38fbb0bf61e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb8b0546c13231aff51f9614efdddf1afe1a4626b7573a741dffe38fbb0bf61e->enter($__internal_fb8b0546c13231aff51f9614efdddf1afe1a4626b7573a741dffe38fbb0bf61e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c1cf5890dc65e3d40aaa3e6e099bfe42f5ee61fd2c15d524c55e15fcf5c77809->leave($__internal_c1cf5890dc65e3d40aaa3e6e099bfe42f5ee61fd2c15d524c55e15fcf5c77809_prof);

        
        $__internal_fb8b0546c13231aff51f9614efdddf1afe1a4626b7573a741dffe38fbb0bf61e->leave($__internal_fb8b0546c13231aff51f9614efdddf1afe1a4626b7573a741dffe38fbb0bf61e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a7209f11ee686957492397c4468e99c129bc290e805bbaae96b1890e2bc926f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a7209f11ee686957492397c4468e99c129bc290e805bbaae96b1890e2bc926f1->enter($__internal_a7209f11ee686957492397c4468e99c129bc290e805bbaae96b1890e2bc926f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_63f6b23224317e61ed8b4cc69bfce23b3c8e4a83b6235fed8dae0cc895f816bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63f6b23224317e61ed8b4cc69bfce23b3c8e4a83b6235fed8dae0cc895f816bd->enter($__internal_63f6b23224317e61ed8b4cc69bfce23b3c8e4a83b6235fed8dae0cc895f816bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "@FOSUser/Group/show.html.twig", 4)->display($context);
        
        $__internal_63f6b23224317e61ed8b4cc69bfce23b3c8e4a83b6235fed8dae0cc895f816bd->leave($__internal_63f6b23224317e61ed8b4cc69bfce23b3c8e4a83b6235fed8dae0cc895f816bd_prof);

        
        $__internal_a7209f11ee686957492397c4468e99c129bc290e805bbaae96b1890e2bc926f1->leave($__internal_a7209f11ee686957492397c4468e99c129bc290e805bbaae96b1890e2bc926f1_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/show.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\show.html.twig");
    }
}
