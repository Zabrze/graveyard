<?php

/* @FOSUser/Registration/register_content.html.twig */
class __TwigTemplate_53a9237c5e8a55443d4a55c8433f71d67cc344f1425ac4275fa850c0dbc2023f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9f238930cdaed942a01bf42edaac97959460b49a54ec8bd11964a44fed88a4dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f238930cdaed942a01bf42edaac97959460b49a54ec8bd11964a44fed88a4dd->enter($__internal_9f238930cdaed942a01bf42edaac97959460b49a54ec8bd11964a44fed88a4dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        $__internal_25560f2fdb63edb19f2f0e4b257cff14e438fb7758b5d3681fe75ab42ccf75c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25560f2fdb63edb19f2f0e4b257cff14e438fb7758b5d3681fe75ab42ccf75c3->enter($__internal_25560f2fdb63edb19f2f0e4b257cff14e438fb7758b5d3681fe75ab42ccf75c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register_content.html.twig"));

        // line 2
        echo "<div class=\"form\">
    <div class=\"form-horizontal\">
        <h3>Zarejestruj się:</h3>
        ";
        // line 5
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start', array("method" => "post", "action" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register"), "attr" => array("class" => "fos_user_registration_register")));
        echo "
        ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "

        <div class=\"form-horizontal\">
            <input type=\"submit\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" />
        </div>
    </div>
</div>
";
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_9f238930cdaed942a01bf42edaac97959460b49a54ec8bd11964a44fed88a4dd->leave($__internal_9f238930cdaed942a01bf42edaac97959460b49a54ec8bd11964a44fed88a4dd_prof);

        
        $__internal_25560f2fdb63edb19f2f0e4b257cff14e438fb7758b5d3681fe75ab42ccf75c3->leave($__internal_25560f2fdb63edb19f2f0e4b257cff14e438fb7758b5d3681fe75ab42ccf75c3_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 13,  40 => 9,  34 => 6,  30 => 5,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
<div class=\"form\">
    <div class=\"form-horizontal\">
        <h3>Zarejestruj się:</h3>
        {{ form_start(form, {'method': 'post', 'action': path('fos_user_registration_register'), 'attr': {'class': 'fos_user_registration_register'}}) }}
        {{ form_widget(form) }}

        <div class=\"form-horizontal\">
            <input type=\"submit\" value=\"{{ 'registration.submit'|trans }}\" />
        </div>
    </div>
</div>
{{ form_end(form) }}
", "@FOSUser/Registration/register_content.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\register_content.html.twig");
    }
}
