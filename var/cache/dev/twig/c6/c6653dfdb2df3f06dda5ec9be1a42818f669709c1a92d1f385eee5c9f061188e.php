<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_9c839e6962e475e969c71bbadc33b9411369e900382837047407bdd785cd15cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_393398a2a0228c2e9e3051a067415da28e19f4612c6616fe97d6a0359b04d217 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_393398a2a0228c2e9e3051a067415da28e19f4612c6616fe97d6a0359b04d217->enter($__internal_393398a2a0228c2e9e3051a067415da28e19f4612c6616fe97d6a0359b04d217_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_7d2ad860d08a1f3a1a3bd1cdfcb60835941a44170f96cf018a1e09d83ae0daf2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d2ad860d08a1f3a1a3bd1cdfcb60835941a44170f96cf018a1e09d83ae0daf2->enter($__internal_7d2ad860d08a1f3a1a3bd1cdfcb60835941a44170f96cf018a1e09d83ae0daf2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_393398a2a0228c2e9e3051a067415da28e19f4612c6616fe97d6a0359b04d217->leave($__internal_393398a2a0228c2e9e3051a067415da28e19f4612c6616fe97d6a0359b04d217_prof);

        
        $__internal_7d2ad860d08a1f3a1a3bd1cdfcb60835941a44170f96cf018a1e09d83ae0daf2->leave($__internal_7d2ad860d08a1f3a1a3bd1cdfcb60835941a44170f96cf018a1e09d83ae0daf2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_widget.html.php");
    }
}
