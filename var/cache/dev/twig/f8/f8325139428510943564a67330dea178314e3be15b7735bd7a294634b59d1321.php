<?php

/* @Framework/Form/widget_attributes.html.php */
class __TwigTemplate_9f576521e4b13ae2774aeff939f2420d5283424a40b0bf38a30655851eae58fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3eded1745b9d8bfb68fdbdb8485d5bdced2ad218b765de28283c0a23b157e1c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3eded1745b9d8bfb68fdbdb8485d5bdced2ad218b765de28283c0a23b157e1c0->enter($__internal_3eded1745b9d8bfb68fdbdb8485d5bdced2ad218b765de28283c0a23b157e1c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        $__internal_b3da170fe14d36340afb4ad5659f401028d473a88e7a9717f45f8fa6eef3713f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3da170fe14d36340afb4ad5659f401028d473a88e7a9717f45f8fa6eef3713f->enter($__internal_b3da170fe14d36340afb4ad5659f401028d473a88e7a9717f45f8fa6eef3713f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_3eded1745b9d8bfb68fdbdb8485d5bdced2ad218b765de28283c0a23b157e1c0->leave($__internal_3eded1745b9d8bfb68fdbdb8485d5bdced2ad218b765de28283c0a23b157e1c0_prof);

        
        $__internal_b3da170fe14d36340afb4ad5659f401028d473a88e7a9717f45f8fa6eef3713f->leave($__internal_b3da170fe14d36340afb4ad5659f401028d473a88e7a9717f45f8fa6eef3713f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_attributes.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\widget_attributes.html.php");
    }
}
