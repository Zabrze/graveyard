<?php

/* default/deadCard.html.twig */
class __TwigTemplate_22fb49b02e6a0ffd0ca4e47423becdab88e5f3b14f7f727aca413489ddbdb035 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c94a1ed99795e8fcbd7e77fb7ae4b46b0f7428a876075665585c9dda54c82d5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c94a1ed99795e8fcbd7e77fb7ae4b46b0f7428a876075665585c9dda54c82d5c->enter($__internal_c94a1ed99795e8fcbd7e77fb7ae4b46b0f7428a876075665585c9dda54c82d5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/deadCard.html.twig"));

        $__internal_a06932bb48e272ef703cc3a358a26d22751afa83a936bef0acfbb605d5e1b1e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a06932bb48e272ef703cc3a358a26d22751afa83a936bef0acfbb605d5e1b1e9->enter($__internal_a06932bb48e272ef703cc3a358a26d22751afa83a936bef0acfbb605d5e1b1e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/deadCard.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "default/deadCard.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_c94a1ed99795e8fcbd7e77fb7ae4b46b0f7428a876075665585c9dda54c82d5c->leave($__internal_c94a1ed99795e8fcbd7e77fb7ae4b46b0f7428a876075665585c9dda54c82d5c_prof);

        
        $__internal_a06932bb48e272ef703cc3a358a26d22751afa83a936bef0acfbb605d5e1b1e9->leave($__internal_a06932bb48e272ef703cc3a358a26d22751afa83a936bef0acfbb605d5e1b1e9_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_e9964941fecf8f9c75bd3116e5fc182b51a7d9dcebeec3cc919b309897bda461 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9964941fecf8f9c75bd3116e5fc182b51a7d9dcebeec3cc919b309897bda461->enter($__internal_e9964941fecf8f9c75bd3116e5fc182b51a7d9dcebeec3cc919b309897bda461_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_7a6d08730baea0eb46620763ce02969537854633560b83c1c309beb5d01e7e1e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a6d08730baea0eb46620763ce02969537854633560b83c1c309beb5d01e7e1e->enter($__internal_7a6d08730baea0eb46620763ce02969537854633560b83c1c309beb5d01e7e1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
        

                <div class=\"container\">

                <h2>Zmarły:</h2><br/>

                <p> 

                    Imię: ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "name", array()), "html", null, true);
        echo "<br/>

                    Drugie imię: ";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "secondName", array()), "html", null, true);
        echo "<br/>

                    Nazwisko: ";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "surname", array()), "html", null, true);
        echo "<br/>

                    Nazwisko panieńskie: ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "maidenSurname", array()), "html", null, true);
        echo "

                    

                    <h4>Dane szczegółowe:</h4>

                     <br/> Imię matki: ";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "motherName", array()), "html", null, true);
        echo "

                     <br/> Imię ojca: ";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "fatherName", array()), "html", null, true);
        echo "

                    <br/> Data urodzenia: ";
        // line 29
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "birthDate", array()), "d-m-Y"), "html", null, true);
        echo "

                    <br/> Data zgonu: ";
        // line 31
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "deathDate", array()), "d-m-Y"), "html", null, true);
        echo "

                    <br/> Data pochówku: ";
        // line 33
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "funeralDate", array()), "d-m-Y"), "html", null, true);
        echo "

                    <br/> Typ pochówku: ";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "boxType", array()), "html", null, true);
        echo "

                    <br/> Miejsce grobowe: ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "deadPlace", array()), "html", null, true);
        echo "

                    <br/> Data modyfikacji: ";
        // line 39
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "createDate", array()), "d-m-Y"), "html", null, true);
        echo "

                    

                    

                    <br/> <button type=\"button\"> <a href=\"/display/";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dead"]) ? $context["dead"] : $this->getContext($context, "dead")), "id", array()), "html", null, true);
        echo "\">Wyświetl dane grobu</a></button>

     </div>     

        

";
        
        $__internal_7a6d08730baea0eb46620763ce02969537854633560b83c1c309beb5d01e7e1e->leave($__internal_7a6d08730baea0eb46620763ce02969537854633560b83c1c309beb5d01e7e1e_prof);

        
        $__internal_e9964941fecf8f9c75bd3116e5fc182b51a7d9dcebeec3cc919b309897bda461->leave($__internal_e9964941fecf8f9c75bd3116e5fc182b51a7d9dcebeec3cc919b309897bda461_prof);

    }

    public function getTemplateName()
    {
        return "default/deadCard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 45,  119 => 39,  114 => 37,  109 => 35,  104 => 33,  99 => 31,  94 => 29,  89 => 27,  84 => 25,  75 => 19,  70 => 17,  65 => 15,  60 => 13,  49 => 4,  31 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'base.html.twig' %}

{% block body %}

        

                <div class=\"container\">

                <h2>Zmarły:</h2><br/>

                <p> 

                    Imię: {{ dead.name }}<br/>

                    Drugie imię: {{ dead.secondName }}<br/>

                    Nazwisko: {{ dead.surname }}<br/>

                    Nazwisko panieńskie: {{ dead.maidenSurname }}

                    

                    <h4>Dane szczegółowe:</h4>

                     <br/> Imię matki: {{ dead.motherName}}

                     <br/> Imię ojca: {{ dead.fatherName}}

                    <br/> Data urodzenia: {{ dead.birthDate|date('d-m-Y') }}

                    <br/> Data zgonu: {{ dead.deathDate|date('d-m-Y') }}

                    <br/> Data pochówku: {{ dead.funeralDate|date('d-m-Y')}}

                    <br/> Typ pochówku: {{ dead.boxType}}

                    <br/> Miejsce grobowe: {{ dead.deadPlace}}

                    <br/> Data modyfikacji: {{ dead.createDate|date('d-m-Y')}}

                    

                    

                    <br/> <button type=\"button\"> <a href=\"/display/{{dead.id}}\">Wyświetl dane grobu</a></button>

     </div>     

        

{% endblock %}", "default/deadCard.html.twig", "C:\\projects\\graveyard\\app\\Resources\\views\\default\\deadCard.html.twig");
    }
}
