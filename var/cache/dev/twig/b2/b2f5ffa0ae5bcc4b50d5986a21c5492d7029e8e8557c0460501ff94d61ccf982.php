<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_7c5b9065500a5389b18c52d5579df957534858362bda2fc8dbd1b42e7d402f98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dff1785301abf1fa2313546f7079e91a26fab2a214cca323099ed1e29d7e66ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dff1785301abf1fa2313546f7079e91a26fab2a214cca323099ed1e29d7e66ae->enter($__internal_dff1785301abf1fa2313546f7079e91a26fab2a214cca323099ed1e29d7e66ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $__internal_47536211c358d915b9b9af7a40102181ff7a61e6c196fe1f870791c32f6380cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_47536211c358d915b9b9af7a40102181ff7a61e6c196fe1f870791c32f6380cb->enter($__internal_47536211c358d915b9b9af7a40102181ff7a61e6c196fe1f870791c32f6380cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_dff1785301abf1fa2313546f7079e91a26fab2a214cca323099ed1e29d7e66ae->leave($__internal_dff1785301abf1fa2313546f7079e91a26fab2a214cca323099ed1e29d7e66ae_prof);

        
        $__internal_47536211c358d915b9b9af7a40102181ff7a61e6c196fe1f870791c32f6380cb->leave($__internal_47536211c358d915b9b9af7a40102181ff7a61e6c196fe1f870791c32f6380cb_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_72cddf0ab9f26172d1102f688846e83ec84773e99d3873c14104240b0e4e46cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72cddf0ab9f26172d1102f688846e83ec84773e99d3873c14104240b0e4e46cb->enter($__internal_72cddf0ab9f26172d1102f688846e83ec84773e99d3873c14104240b0e4e46cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_fdd7f89d079a0d61851c120cf1c6f06adbdc455a7fb97945cd75c9129979e780 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fdd7f89d079a0d61851c120cf1c6f06adbdc455a7fb97945cd75c9129979e780->enter($__internal_fdd7f89d079a0d61851c120cf1c6f06adbdc455a7fb97945cd75c9129979e780_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_fdd7f89d079a0d61851c120cf1c6f06adbdc455a7fb97945cd75c9129979e780->leave($__internal_fdd7f89d079a0d61851c120cf1c6f06adbdc455a7fb97945cd75c9129979e780_prof);

        
        $__internal_72cddf0ab9f26172d1102f688846e83ec84773e99d3873c14104240b0e4e46cb->leave($__internal_72cddf0ab9f26172d1102f688846e83ec84773e99d3873c14104240b0e4e46cb_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/register.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\register.html.twig");
    }
}
