<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_30fbfa0eebcbaf63faaaf4393d0733c5e8182cd913dc2b5df4e7ff06d45c3529 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42857485ac465fdcfc943dc1e77219d996129e8180b70f84f3047dad599266bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42857485ac465fdcfc943dc1e77219d996129e8180b70f84f3047dad599266bb->enter($__internal_42857485ac465fdcfc943dc1e77219d996129e8180b70f84f3047dad599266bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_d49349136fa00f75a01d25e8c01427965ca84ff4eff56e286b852a3bcfcbb8ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d49349136fa00f75a01d25e8c01427965ca84ff4eff56e286b852a3bcfcbb8ca->enter($__internal_d49349136fa00f75a01d25e8c01427965ca84ff4eff56e286b852a3bcfcbb8ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_42857485ac465fdcfc943dc1e77219d996129e8180b70f84f3047dad599266bb->leave($__internal_42857485ac465fdcfc943dc1e77219d996129e8180b70f84f3047dad599266bb_prof);

        
        $__internal_d49349136fa00f75a01d25e8c01427965ca84ff4eff56e286b852a3bcfcbb8ca->leave($__internal_d49349136fa00f75a01d25e8c01427965ca84ff4eff56e286b852a3bcfcbb8ca_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form.html.php");
    }
}
