<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_f1436766ee8e4529b0d11de669c5abc28238e99cf16e3a19291d8ccc3863b601 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcb43341260c51ac99ffe94d5e64e4a89d2c87b4a53bb84e797b28deaa7264cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcb43341260c51ac99ffe94d5e64e4a89d2c87b4a53bb84e797b28deaa7264cb->enter($__internal_dcb43341260c51ac99ffe94d5e64e4a89d2c87b4a53bb84e797b28deaa7264cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_d9d7b93971c03cc92fdb1f1f7e579358d541b56a53b209e2f00c42f2b8a9ac41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d9d7b93971c03cc92fdb1f1f7e579358d541b56a53b209e2f00c42f2b8a9ac41->enter($__internal_d9d7b93971c03cc92fdb1f1f7e579358d541b56a53b209e2f00c42f2b8a9ac41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_dcb43341260c51ac99ffe94d5e64e4a89d2c87b4a53bb84e797b28deaa7264cb->leave($__internal_dcb43341260c51ac99ffe94d5e64e4a89d2c87b4a53bb84e797b28deaa7264cb_prof);

        
        $__internal_d9d7b93971c03cc92fdb1f1f7e579358d541b56a53b209e2f00c42f2b8a9ac41->leave($__internal_d9d7b93971c03cc92fdb1f1f7e579358d541b56a53b209e2f00c42f2b8a9ac41_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\email_widget.html.php");
    }
}
