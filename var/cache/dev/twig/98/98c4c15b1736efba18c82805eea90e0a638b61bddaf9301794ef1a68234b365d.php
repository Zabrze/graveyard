<?php

/* @FOSUser/Group/edit.html.twig */
class __TwigTemplate_0e2cf3fe1d5de92ae41a2ed8f3382c4030e31367364785923f02e678b34e83f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f1243ec1c54813e2320a155f501060188a36eb2d611037fbc9a3dddaa3227eea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1243ec1c54813e2320a155f501060188a36eb2d611037fbc9a3dddaa3227eea->enter($__internal_f1243ec1c54813e2320a155f501060188a36eb2d611037fbc9a3dddaa3227eea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $__internal_9c9080dfe260a30328992f1ce6377ae4f35e2427b2b5c83e04f86e431489ca2f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c9080dfe260a30328992f1ce6377ae4f35e2427b2b5c83e04f86e431489ca2f->enter($__internal_9c9080dfe260a30328992f1ce6377ae4f35e2427b2b5c83e04f86e431489ca2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f1243ec1c54813e2320a155f501060188a36eb2d611037fbc9a3dddaa3227eea->leave($__internal_f1243ec1c54813e2320a155f501060188a36eb2d611037fbc9a3dddaa3227eea_prof);

        
        $__internal_9c9080dfe260a30328992f1ce6377ae4f35e2427b2b5c83e04f86e431489ca2f->leave($__internal_9c9080dfe260a30328992f1ce6377ae4f35e2427b2b5c83e04f86e431489ca2f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ba95c44fd2b47aacb49c097a087efb2a84a9e21a257330c2cb22de749deb791e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba95c44fd2b47aacb49c097a087efb2a84a9e21a257330c2cb22de749deb791e->enter($__internal_ba95c44fd2b47aacb49c097a087efb2a84a9e21a257330c2cb22de749deb791e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_45fbba9096a73388a74556323ac1689c91e2526a6c9a50c70773f2cfb4ae2bad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45fbba9096a73388a74556323ac1689c91e2526a6c9a50c70773f2cfb4ae2bad->enter($__internal_45fbba9096a73388a74556323ac1689c91e2526a6c9a50c70773f2cfb4ae2bad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "@FOSUser/Group/edit.html.twig", 4)->display($context);
        
        $__internal_45fbba9096a73388a74556323ac1689c91e2526a6c9a50c70773f2cfb4ae2bad->leave($__internal_45fbba9096a73388a74556323ac1689c91e2526a6c9a50c70773f2cfb4ae2bad_prof);

        
        $__internal_ba95c44fd2b47aacb49c097a087efb2a84a9e21a257330c2cb22de749deb791e->leave($__internal_ba95c44fd2b47aacb49c097a087efb2a84a9e21a257330c2cb22de749deb791e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/edit.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\edit.html.twig");
    }
}
