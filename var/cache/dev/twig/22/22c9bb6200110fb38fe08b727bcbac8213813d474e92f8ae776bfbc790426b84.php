<?php

/* @Twig/Exception/error.rdf.twig */
class __TwigTemplate_07620a6e2f468ccb7d65774c0025d4848029e379e68da8cde83bd90ab25b4fb2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f34851d8f9e33f03eab4f91943f84f35a418a73b5f04cec09b6e5a11f8bafc90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f34851d8f9e33f03eab4f91943f84f35a418a73b5f04cec09b6e5a11f8bafc90->enter($__internal_f34851d8f9e33f03eab4f91943f84f35a418a73b5f04cec09b6e5a11f8bafc90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.rdf.twig"));

        $__internal_1caa749d6cd94ee846e634d2fa72aa42dfc3a2448534f499ec218c3589dcbc28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1caa749d6cd94ee846e634d2fa72aa42dfc3a2448534f499ec218c3589dcbc28->enter($__internal_1caa749d6cd94ee846e634d2fa72aa42dfc3a2448534f499ec218c3589dcbc28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_f34851d8f9e33f03eab4f91943f84f35a418a73b5f04cec09b6e5a11f8bafc90->leave($__internal_f34851d8f9e33f03eab4f91943f84f35a418a73b5f04cec09b6e5a11f8bafc90_prof);

        
        $__internal_1caa749d6cd94ee846e634d2fa72aa42dfc3a2448534f499ec218c3589dcbc28->leave($__internal_1caa749d6cd94ee846e634d2fa72aa42dfc3a2448534f499ec218c3589dcbc28_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "@Twig/Exception/error.rdf.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.rdf.twig");
    }
}
