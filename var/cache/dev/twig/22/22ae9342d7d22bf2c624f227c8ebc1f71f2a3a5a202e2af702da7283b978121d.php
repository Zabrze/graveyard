<?php

/* @FOSUser/Group/list.html.twig */
class __TwigTemplate_0f9eef197d1a39704077227565208a91f878a89d43fa4e46f087b53c1abf0ef8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fef887c822cfa5983246d78095c7f46681a0f8ae89397c0fdce1a4f726fb4e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fef887c822cfa5983246d78095c7f46681a0f8ae89397c0fdce1a4f726fb4e4->enter($__internal_5fef887c822cfa5983246d78095c7f46681a0f8ae89397c0fdce1a4f726fb4e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $__internal_5fab15027e396f29e04792d26473dae7b8b50873de492e382906a5becd17dbb5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5fab15027e396f29e04792d26473dae7b8b50873de492e382906a5becd17dbb5->enter($__internal_5fab15027e396f29e04792d26473dae7b8b50873de492e382906a5becd17dbb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5fef887c822cfa5983246d78095c7f46681a0f8ae89397c0fdce1a4f726fb4e4->leave($__internal_5fef887c822cfa5983246d78095c7f46681a0f8ae89397c0fdce1a4f726fb4e4_prof);

        
        $__internal_5fab15027e396f29e04792d26473dae7b8b50873de492e382906a5becd17dbb5->leave($__internal_5fab15027e396f29e04792d26473dae7b8b50873de492e382906a5becd17dbb5_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fda642e9947f720317633abd148d53f6e18df682793f75aadfd34cc28fb2e41e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fda642e9947f720317633abd148d53f6e18df682793f75aadfd34cc28fb2e41e->enter($__internal_fda642e9947f720317633abd148d53f6e18df682793f75aadfd34cc28fb2e41e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_200d30d92bf26a323996e4bfb2bf44a0f37e307076f0c4fbe75ff04bdb1a7be7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_200d30d92bf26a323996e4bfb2bf44a0f37e307076f0c4fbe75ff04bdb1a7be7->enter($__internal_200d30d92bf26a323996e4bfb2bf44a0f37e307076f0c4fbe75ff04bdb1a7be7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "@FOSUser/Group/list.html.twig", 4)->display($context);
        
        $__internal_200d30d92bf26a323996e4bfb2bf44a0f37e307076f0c4fbe75ff04bdb1a7be7->leave($__internal_200d30d92bf26a323996e4bfb2bf44a0f37e307076f0c4fbe75ff04bdb1a7be7_prof);

        
        $__internal_fda642e9947f720317633abd148d53f6e18df682793f75aadfd34cc28fb2e41e->leave($__internal_fda642e9947f720317633abd148d53f6e18df682793f75aadfd34cc28fb2e41e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Group/list.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\list.html.twig");
    }
}
