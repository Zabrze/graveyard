<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_228e8ac46aaaa0126db04bb93c2a70e5dc7b0abefc42b49737269282dc917e0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d6834d0e00344e6df4c07b52aa44ec91b39554a89e622135af9c3fdaf9d63d5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d6834d0e00344e6df4c07b52aa44ec91b39554a89e622135af9c3fdaf9d63d5d->enter($__internal_d6834d0e00344e6df4c07b52aa44ec91b39554a89e622135af9c3fdaf9d63d5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_bb43c28bdd4612b5580afe2b517ef3c4545af5cd8936de59a1657006d68e9b36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb43c28bdd4612b5580afe2b517ef3c4545af5cd8936de59a1657006d68e9b36->enter($__internal_bb43c28bdd4612b5580afe2b517ef3c4545af5cd8936de59a1657006d68e9b36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_d6834d0e00344e6df4c07b52aa44ec91b39554a89e622135af9c3fdaf9d63d5d->leave($__internal_d6834d0e00344e6df4c07b52aa44ec91b39554a89e622135af9c3fdaf9d63d5d_prof);

        
        $__internal_bb43c28bdd4612b5580afe2b517ef3c4545af5cd8936de59a1657006d68e9b36->leave($__internal_bb43c28bdd4612b5580afe2b517ef3c4545af5cd8936de59a1657006d68e9b36_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget.html.php");
    }
}
