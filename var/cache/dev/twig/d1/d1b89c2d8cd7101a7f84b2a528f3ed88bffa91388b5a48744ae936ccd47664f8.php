<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_0714226eb598172c6df150972fc839567e21a75121d37690b60d95aa14468277 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da73ecf138a6409917034f50df86faa77dae74ec8b1c1fbbfc0f570def22880f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da73ecf138a6409917034f50df86faa77dae74ec8b1c1fbbfc0f570def22880f->enter($__internal_da73ecf138a6409917034f50df86faa77dae74ec8b1c1fbbfc0f570def22880f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_9930c340dcbd7d504972fb0cfcf8d2a3d5fb36b5fd4b1b62dccb10307dc1387c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9930c340dcbd7d504972fb0cfcf8d2a3d5fb36b5fd4b1b62dccb10307dc1387c->enter($__internal_9930c340dcbd7d504972fb0cfcf8d2a3d5fb36b5fd4b1b62dccb10307dc1387c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_da73ecf138a6409917034f50df86faa77dae74ec8b1c1fbbfc0f570def22880f->leave($__internal_da73ecf138a6409917034f50df86faa77dae74ec8b1c1fbbfc0f570def22880f_prof);

        
        $__internal_9930c340dcbd7d504972fb0cfcf8d2a3d5fb36b5fd4b1b62dccb10307dc1387c->leave($__internal_9930c340dcbd7d504972fb0cfcf8d2a3d5fb36b5fd4b1b62dccb10307dc1387c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\search_widget.html.php");
    }
}
