<?php

/* base.html.twig */
class __TwigTemplate_86772047284e53516357911e5f667eafa7a7a01ef5aee93d237fd57d276efa77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4d6c9ab0b24717881f046e52ae2615fc33c340b9672b797a8f38a16d622bb071 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d6c9ab0b24717881f046e52ae2615fc33c340b9672b797a8f38a16d622bb071->enter($__internal_4d6c9ab0b24717881f046e52ae2615fc33c340b9672b797a8f38a16d622bb071_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_66d2c1dbd6bcd441b4b3d461f626f78a5a1a0985237f64b78b926f0fb44574f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66d2c1dbd6bcd441b4b3d461f626f78a5a1a0985237f64b78b926f0fb44574f6->enter($__internal_66d2c1dbd6bcd441b4b3d461f626f78a5a1a0985237f64b78b926f0fb44574f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/image/znicz.jpg"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap-3.3.7-dist/css/bootstrap.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/css/style.css"), "html", null, true);
        echo "\" />
    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/js/jquery-3.3.1.min.js"), "html", null, true);
        echo "\" ></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap-3.3.7-dist/js/bootstrap.js"), "html", null, true);
        echo "\" ></script>

</head>
<body>
    ";
        // line 15
        $this->displayBlock('body', $context, $blocks);
        // line 64
        $this->displayBlock('javascripts', $context, $blocks);
        // line 65
        echo "</body>
</html>
";
        
        $__internal_4d6c9ab0b24717881f046e52ae2615fc33c340b9672b797a8f38a16d622bb071->leave($__internal_4d6c9ab0b24717881f046e52ae2615fc33c340b9672b797a8f38a16d622bb071_prof);

        
        $__internal_66d2c1dbd6bcd441b4b3d461f626f78a5a1a0985237f64b78b926f0fb44574f6->leave($__internal_66d2c1dbd6bcd441b4b3d461f626f78a5a1a0985237f64b78b926f0fb44574f6_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_537575dbc1d684b136484101f5c2c2630d8f247173cfcff0773e7f795dd5bb36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_537575dbc1d684b136484101f5c2c2630d8f247173cfcff0773e7f795dd5bb36->enter($__internal_537575dbc1d684b136484101f5c2c2630d8f247173cfcff0773e7f795dd5bb36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_54f1d18403995026c34017c23568db4db2e1fe138f1aa0b11ceee78e572afb1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54f1d18403995026c34017c23568db4db2e1fe138f1aa0b11ceee78e572afb1c->enter($__internal_54f1d18403995026c34017c23568db4db2e1fe138f1aa0b11ceee78e572afb1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Graveyard";
        
        $__internal_54f1d18403995026c34017c23568db4db2e1fe138f1aa0b11ceee78e572afb1c->leave($__internal_54f1d18403995026c34017c23568db4db2e1fe138f1aa0b11ceee78e572afb1c_prof);

        
        $__internal_537575dbc1d684b136484101f5c2c2630d8f247173cfcff0773e7f795dd5bb36->leave($__internal_537575dbc1d684b136484101f5c2c2630d8f247173cfcff0773e7f795dd5bb36_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_fc6771d19ed9148753c23adda8c8b0d94b9f530ff87645c5f5e252471289bcdd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc6771d19ed9148753c23adda8c8b0d94b9f530ff87645c5f5e252471289bcdd->enter($__internal_fc6771d19ed9148753c23adda8c8b0d94b9f530ff87645c5f5e252471289bcdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_dc1c7a9b9b0f012f7a26f7f6fc68d853a5d9b8a43b6f5469b488c46074f93066 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc1c7a9b9b0f012f7a26f7f6fc68d853a5d9b8a43b6f5469b488c46074f93066->enter($__internal_dc1c7a9b9b0f012f7a26f7f6fc68d853a5d9b8a43b6f5469b488c46074f93066_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_dc1c7a9b9b0f012f7a26f7f6fc68d853a5d9b8a43b6f5469b488c46074f93066->leave($__internal_dc1c7a9b9b0f012f7a26f7f6fc68d853a5d9b8a43b6f5469b488c46074f93066_prof);

        
        $__internal_fc6771d19ed9148753c23adda8c8b0d94b9f530ff87645c5f5e252471289bcdd->leave($__internal_fc6771d19ed9148753c23adda8c8b0d94b9f530ff87645c5f5e252471289bcdd_prof);

    }

    // line 15
    public function block_body($context, array $blocks = array())
    {
        $__internal_ba06a56f5c3fa2750f877f0033214c9097db21fef083e06f862ae5567d545131 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba06a56f5c3fa2750f877f0033214c9097db21fef083e06f862ae5567d545131->enter($__internal_ba06a56f5c3fa2750f877f0033214c9097db21fef083e06f862ae5567d545131_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b978aa041fc74df3038189b770f5eeeebe91c9812636456b7f966850e46ae95e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b978aa041fc74df3038189b770f5eeeebe91c9812636456b7f966850e46ae95e->enter($__internal_b978aa041fc74df3038189b770f5eeeebe91c9812636456b7f966850e46ae95e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 16
        echo "

        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">

                <div class=\"navbar-header\">
                    <a class=\"navbar-brand\" href=\"/\">Strona główna</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                    <ul class=\"nav navbar-nav navbar-left\" >

                        <li><a href=\"/szukaj\">Wyszukaj zmarłego</a></li>
                    </ul>   
                    <ul class=\"nav navbar-nav navbar-right\" >
                        ";
        // line 32
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 33
            echo "                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Wybierz: <span class=\"caret\"></span></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/addGrave\">Dodaj grób</a></li>
                                    <li role=\"separator\" class=\"divider\"></li>
                                    <li><a href=\"/addDead\">Dodaj zmarłego</a></li>
                                    <li role=\"separator\" class=\"divider\"></li>
                                    <li><a href=\"/addCurator\">Dodaj opiekuna</a></li>
                                </ul>
                            </li>
                            <li><a href=\"/profile\">Mój profil</a></li>

                            <li id=\"ifLogged\">";
            // line 45
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo " |</li>

                            <li><a href=\"";
            // line 47
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                                    ";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                </a></li>
                            ";
        } else {
            // line 51
            echo "                            <li><a href=\"/login\">Zaloguj</a></li>
                            <li><a href=\"/register\">Zarejestruj</a></li>
                            ";
        }
        // line 54
        echo "

                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


    ";
        
        $__internal_b978aa041fc74df3038189b770f5eeeebe91c9812636456b7f966850e46ae95e->leave($__internal_b978aa041fc74df3038189b770f5eeeebe91c9812636456b7f966850e46ae95e_prof);

        
        $__internal_ba06a56f5c3fa2750f877f0033214c9097db21fef083e06f862ae5567d545131->leave($__internal_ba06a56f5c3fa2750f877f0033214c9097db21fef083e06f862ae5567d545131_prof);

    }

    // line 64
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_5b97808e6eee45d09d7433c550eccb6a9f5e5c009de3d0e4a020b71bc3500b8b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b97808e6eee45d09d7433c550eccb6a9f5e5c009de3d0e4a020b71bc3500b8b->enter($__internal_5b97808e6eee45d09d7433c550eccb6a9f5e5c009de3d0e4a020b71bc3500b8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_5b2818b3265ae972a67ca184d3c57fc9a40002df4fa410bc807953b5e0f84719 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b2818b3265ae972a67ca184d3c57fc9a40002df4fa410bc807953b5e0f84719->enter($__internal_5b2818b3265ae972a67ca184d3c57fc9a40002df4fa410bc807953b5e0f84719_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_5b2818b3265ae972a67ca184d3c57fc9a40002df4fa410bc807953b5e0f84719->leave($__internal_5b2818b3265ae972a67ca184d3c57fc9a40002df4fa410bc807953b5e0f84719_prof);

        
        $__internal_5b97808e6eee45d09d7433c550eccb6a9f5e5c009de3d0e4a020b71bc3500b8b->leave($__internal_5b97808e6eee45d09d7433c550eccb6a9f5e5c009de3d0e4a020b71bc3500b8b_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 64,  179 => 54,  174 => 51,  168 => 48,  164 => 47,  159 => 45,  145 => 33,  143 => 32,  125 => 16,  116 => 15,  99 => 6,  81 => 5,  69 => 65,  67 => 64,  65 => 15,  58 => 11,  54 => 10,  50 => 9,  46 => 8,  41 => 7,  39 => 6,  35 => 5,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Graveyard{% endblock %}</title>
    {% block stylesheets %}{% endblock %}
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('/image/znicz.jpg') }}\" />
    <link rel=\"stylesheet\" href=\"{{ asset('bootstrap-3.3.7-dist/css/bootstrap.css') }}\" />
    <link rel=\"stylesheet\" href=\"{{ asset('/css/style.css') }}\" />
    <script src=\"{{ asset('/js/jquery-3.3.1.min.js') }}\" ></script>
    <script src=\"{{ asset('bootstrap-3.3.7-dist/js/bootstrap.js') }}\" ></script>

</head>
<body>
    {% block body %}


        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">

                <div class=\"navbar-header\">
                    <a class=\"navbar-brand\" href=\"/\">Strona główna</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                    <ul class=\"nav navbar-nav navbar-left\" >

                        <li><a href=\"/szukaj\">Wyszukaj zmarłego</a></li>
                    </ul>   
                    <ul class=\"nav navbar-nav navbar-right\" >
                        {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">Wybierz: <span class=\"caret\"></span></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/addGrave\">Dodaj grób</a></li>
                                    <li role=\"separator\" class=\"divider\"></li>
                                    <li><a href=\"/addDead\">Dodaj zmarłego</a></li>
                                    <li role=\"separator\" class=\"divider\"></li>
                                    <li><a href=\"/addCurator\">Dodaj opiekuna</a></li>
                                </ul>
                            </li>
                            <li><a href=\"/profile\">Mój profil</a></li>

                            <li id=\"ifLogged\">{{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }} |</li>

                            <li><a href=\"{{ path('fos_user_security_logout') }}\">
                                    {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                                </a></li>
                            {% else %}
                            <li><a href=\"/login\">Zaloguj</a></li>
                            <li><a href=\"/register\">Zarejestruj</a></li>
                            {% endif %}


                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


    {% endblock %}
{% block javascripts %}{% endblock %}
</body>
</html>
", "base.html.twig", "C:\\projects\\graveyard\\app\\Resources\\views\\base.html.twig");
    }
}
