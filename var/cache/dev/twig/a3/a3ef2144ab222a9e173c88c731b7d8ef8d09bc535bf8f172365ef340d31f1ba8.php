<?php

/* @FOSUser/Registration/email.txt.twig */
class __TwigTemplate_c3d852ff6032b433de15d0cf0b96428d6943b1f806a59e31955754a2953ec5cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_affee3a95aba4de8cf1ea818db21b6dfdd8dc71ceaefd83697d5812356b221f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_affee3a95aba4de8cf1ea818db21b6dfdd8dc71ceaefd83697d5812356b221f1->enter($__internal_affee3a95aba4de8cf1ea818db21b6dfdd8dc71ceaefd83697d5812356b221f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        $__internal_6de4a2637d93683eb2924ee57d13013e672bc536e9e99b80f57446b74b53fcb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6de4a2637d93683eb2924ee57d13013e672bc536e9e99b80f57446b74b53fcb6->enter($__internal_6de4a2637d93683eb2924ee57d13013e672bc536e9e99b80f57446b74b53fcb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_affee3a95aba4de8cf1ea818db21b6dfdd8dc71ceaefd83697d5812356b221f1->leave($__internal_affee3a95aba4de8cf1ea818db21b6dfdd8dc71ceaefd83697d5812356b221f1_prof);

        
        $__internal_6de4a2637d93683eb2924ee57d13013e672bc536e9e99b80f57446b74b53fcb6->leave($__internal_6de4a2637d93683eb2924ee57d13013e672bc536e9e99b80f57446b74b53fcb6_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_2569d69f06b77ebf879f61e5abdc666bb178fe80d71b9d043594c4b9bc96b49b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2569d69f06b77ebf879f61e5abdc666bb178fe80d71b9d043594c4b9bc96b49b->enter($__internal_2569d69f06b77ebf879f61e5abdc666bb178fe80d71b9d043594c4b9bc96b49b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_65fb13b1d2ffd141a8ba9ca47d9660439d5c4a662335afa1df66a4b689dd1764 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65fb13b1d2ffd141a8ba9ca47d9660439d5c4a662335afa1df66a4b689dd1764->enter($__internal_65fb13b1d2ffd141a8ba9ca47d9660439d5c4a662335afa1df66a4b689dd1764_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_65fb13b1d2ffd141a8ba9ca47d9660439d5c4a662335afa1df66a4b689dd1764->leave($__internal_65fb13b1d2ffd141a8ba9ca47d9660439d5c4a662335afa1df66a4b689dd1764_prof);

        
        $__internal_2569d69f06b77ebf879f61e5abdc666bb178fe80d71b9d043594c4b9bc96b49b->leave($__internal_2569d69f06b77ebf879f61e5abdc666bb178fe80d71b9d043594c4b9bc96b49b_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_5f3a4a01d28eae7dfd6144b8dec9868c5ff483902ed6f5d83645ee6f8eb1a919 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f3a4a01d28eae7dfd6144b8dec9868c5ff483902ed6f5d83645ee6f8eb1a919->enter($__internal_5f3a4a01d28eae7dfd6144b8dec9868c5ff483902ed6f5d83645ee6f8eb1a919_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_156d880cdfa7024c524525bb17c16e49fa10c25e4e64378e8d75013803ab41c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_156d880cdfa7024c524525bb17c16e49fa10c25e4e64378e8d75013803ab41c7->enter($__internal_156d880cdfa7024c524525bb17c16e49fa10c25e4e64378e8d75013803ab41c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_156d880cdfa7024c524525bb17c16e49fa10c25e4e64378e8d75013803ab41c7->leave($__internal_156d880cdfa7024c524525bb17c16e49fa10c25e4e64378e8d75013803ab41c7_prof);

        
        $__internal_5f3a4a01d28eae7dfd6144b8dec9868c5ff483902ed6f5d83645ee6f8eb1a919->leave($__internal_5f3a4a01d28eae7dfd6144b8dec9868c5ff483902ed6f5d83645ee6f8eb1a919_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_7a57759acea89ae931bdc5d723af5ac34e148b24b5da5fa9b263dfe2c6745f94 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a57759acea89ae931bdc5d723af5ac34e148b24b5da5fa9b263dfe2c6745f94->enter($__internal_7a57759acea89ae931bdc5d723af5ac34e148b24b5da5fa9b263dfe2c6745f94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_6529ab902194431bab24125a8448bb2e1273df69d32eab8ab50be03d0008ded8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6529ab902194431bab24125a8448bb2e1273df69d32eab8ab50be03d0008ded8->enter($__internal_6529ab902194431bab24125a8448bb2e1273df69d32eab8ab50be03d0008ded8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_6529ab902194431bab24125a8448bb2e1273df69d32eab8ab50be03d0008ded8->leave($__internal_6529ab902194431bab24125a8448bb2e1273df69d32eab8ab50be03d0008ded8_prof);

        
        $__internal_7a57759acea89ae931bdc5d723af5ac34e148b24b5da5fa9b263dfe2c6745f94->leave($__internal_7a57759acea89ae931bdc5d723af5ac34e148b24b5da5fa9b263dfe2c6745f94_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "@FOSUser/Registration/email.txt.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\email.txt.twig");
    }
}
