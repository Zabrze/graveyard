<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_d547fad0deeda39c521fdfaa7901bbb200b18e123bf57b0818ce44cec12bb378 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac5da82709a3c6047c0c08e6c9d462917bd77e02bf7261533946cfcb907c12ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac5da82709a3c6047c0c08e6c9d462917bd77e02bf7261533946cfcb907c12ae->enter($__internal_ac5da82709a3c6047c0c08e6c9d462917bd77e02bf7261533946cfcb907c12ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_8370ce13957ea394b186f8e4abe7082e1d4a56a7274dc1cbe5b0fd98e6e64091 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8370ce13957ea394b186f8e4abe7082e1d4a56a7274dc1cbe5b0fd98e6e64091->enter($__internal_8370ce13957ea394b186f8e4abe7082e1d4a56a7274dc1cbe5b0fd98e6e64091_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_ac5da82709a3c6047c0c08e6c9d462917bd77e02bf7261533946cfcb907c12ae->leave($__internal_ac5da82709a3c6047c0c08e6c9d462917bd77e02bf7261533946cfcb907c12ae_prof);

        
        $__internal_8370ce13957ea394b186f8e4abe7082e1d4a56a7274dc1cbe5b0fd98e6e64091->leave($__internal_8370ce13957ea394b186f8e4abe7082e1d4a56a7274dc1cbe5b0fd98e6e64091_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_attributes.html.php");
    }
}
