<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_3892d89617b06f9b0dbf43ae52f066ff9d0dcd458dc93d966d62c3793086e0b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21e52e22ade03c1e82ebb410c3216e8dc8db74f06286a0da607fd739172e6045 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21e52e22ade03c1e82ebb410c3216e8dc8db74f06286a0da607fd739172e6045->enter($__internal_21e52e22ade03c1e82ebb410c3216e8dc8db74f06286a0da607fd739172e6045_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_f0f240d7b4799cb2ee72003df722c625dbbb62831794146ff8db271b1ea982ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0f240d7b4799cb2ee72003df722c625dbbb62831794146ff8db271b1ea982ae->enter($__internal_f0f240d7b4799cb2ee72003df722c625dbbb62831794146ff8db271b1ea982ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_21e52e22ade03c1e82ebb410c3216e8dc8db74f06286a0da607fd739172e6045->leave($__internal_21e52e22ade03c1e82ebb410c3216e8dc8db74f06286a0da607fd739172e6045_prof);

        
        $__internal_f0f240d7b4799cb2ee72003df722c625dbbb62831794146ff8db271b1ea982ae->leave($__internal_f0f240d7b4799cb2ee72003df722c625dbbb62831794146ff8db271b1ea982ae_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\collection_widget.html.php");
    }
}
