<?php

/* @WebProfiler/Icon/forward.svg */
class __TwigTemplate_2ea5de337293a077619b1c68bf40300cc9a222bb95eb8147468cf71a89c1c314 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d21801a15de96fa343893708a4e2af8c819f8b26d9fb9f8dd6294765cfd4df42 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d21801a15de96fa343893708a4e2af8c819f8b26d9fb9f8dd6294765cfd4df42->enter($__internal_d21801a15de96fa343893708a4e2af8c819f8b26d9fb9f8dd6294765cfd4df42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        $__internal_6e318bbfe5beb9d7b4d74d5189dca1e76280cbc384ecc77f169c184ef6db4d59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e318bbfe5beb9d7b4d74d5189dca1e76280cbc384ecc77f169c184ef6db4d59->enter($__internal_6e318bbfe5beb9d7b4d74d5189dca1e76280cbc384ecc77f169c184ef6db4d59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
";
        
        $__internal_d21801a15de96fa343893708a4e2af8c819f8b26d9fb9f8dd6294765cfd4df42->leave($__internal_d21801a15de96fa343893708a4e2af8c819f8b26d9fb9f8dd6294765cfd4df42_prof);

        
        $__internal_6e318bbfe5beb9d7b4d74d5189dca1e76280cbc384ecc77f169c184ef6db4d59->leave($__internal_6e318bbfe5beb9d7b4d74d5189dca1e76280cbc384ecc77f169c184ef6db4d59_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/forward.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
", "@WebProfiler/Icon/forward.svg", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Icon\\forward.svg");
    }
}
