<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_be2c0dc4792edc946e0dd84057ee419940788424bab4fa0b16bf573d4834e00e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_334a318cb8cf88bfc669c344e3356ae174c285c287ba5417a83d88e9a21c14dd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_334a318cb8cf88bfc669c344e3356ae174c285c287ba5417a83d88e9a21c14dd->enter($__internal_334a318cb8cf88bfc669c344e3356ae174c285c287ba5417a83d88e9a21c14dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $__internal_ea74af56eca39f15dfe3bd20e62b5bed0399654e16ba0efe4a5eb4c45e49532f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea74af56eca39f15dfe3bd20e62b5bed0399654e16ba0efe4a5eb4c45e49532f->enter($__internal_ea74af56eca39f15dfe3bd20e62b5bed0399654e16ba0efe4a5eb4c45e49532f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_334a318cb8cf88bfc669c344e3356ae174c285c287ba5417a83d88e9a21c14dd->leave($__internal_334a318cb8cf88bfc669c344e3356ae174c285c287ba5417a83d88e9a21c14dd_prof);

        
        $__internal_ea74af56eca39f15dfe3bd20e62b5bed0399654e16ba0efe4a5eb4c45e49532f->leave($__internal_ea74af56eca39f15dfe3bd20e62b5bed0399654e16ba0efe4a5eb4c45e49532f_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_eb40da03bb29631bd4a360c32576f596e38536532ac6f7c9a59132468e0ec57d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb40da03bb29631bd4a360c32576f596e38536532ac6f7c9a59132468e0ec57d->enter($__internal_eb40da03bb29631bd4a360c32576f596e38536532ac6f7c9a59132468e0ec57d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_90bfe966566640f5884785ba3c0e147ada5902e28cf5022a8c097afb799cb587 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_90bfe966566640f5884785ba3c0e147ada5902e28cf5022a8c097afb799cb587->enter($__internal_90bfe966566640f5884785ba3c0e147ada5902e28cf5022a8c097afb799cb587_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_90bfe966566640f5884785ba3c0e147ada5902e28cf5022a8c097afb799cb587->leave($__internal_90bfe966566640f5884785ba3c0e147ada5902e28cf5022a8c097afb799cb587_prof);

        
        $__internal_eb40da03bb29631bd4a360c32576f596e38536532ac6f7c9a59132468e0ec57d->leave($__internal_eb40da03bb29631bd4a360c32576f596e38536532ac6f7c9a59132468e0ec57d_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "@FOSUser/Registration/confirmed.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\confirmed.html.twig");
    }
}
