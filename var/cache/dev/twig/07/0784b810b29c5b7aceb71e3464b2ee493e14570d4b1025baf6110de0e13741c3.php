<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_b92b510ad15e43217efec13c3d5adc9a9cb2aaddff263dde4ff37c7cf648f6aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6385024554edf54adfbc7282c0a7f9beefbee26395d57dd84cc0a2f03b391b71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6385024554edf54adfbc7282c0a7f9beefbee26395d57dd84cc0a2f03b391b71->enter($__internal_6385024554edf54adfbc7282c0a7f9beefbee26395d57dd84cc0a2f03b391b71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_6340778a1ae8ba80b40d259f2ca7c77eb14ca1a90a410789c725690d1d83a5f5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6340778a1ae8ba80b40d259f2ca7c77eb14ca1a90a410789c725690d1d83a5f5->enter($__internal_6340778a1ae8ba80b40d259f2ca7c77eb14ca1a90a410789c725690d1d83a5f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_6385024554edf54adfbc7282c0a7f9beefbee26395d57dd84cc0a2f03b391b71->leave($__internal_6385024554edf54adfbc7282c0a7f9beefbee26395d57dd84cc0a2f03b391b71_prof);

        
        $__internal_6340778a1ae8ba80b40d259f2ca7c77eb14ca1a90a410789c725690d1d83a5f5->leave($__internal_6340778a1ae8ba80b40d259f2ca7c77eb14ca1a90a410789c725690d1d83a5f5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\number_widget.html.php");
    }
}
