<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_a46878681bbf088ea526b8ae59cab7c8dcd48daad4236878bdc82d6d9f1dbd8c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_859c2390465286677e4310e4e294ba6c6e446a3d24766145810b8d3fee27389d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_859c2390465286677e4310e4e294ba6c6e446a3d24766145810b8d3fee27389d->enter($__internal_859c2390465286677e4310e4e294ba6c6e446a3d24766145810b8d3fee27389d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_a9299a92b1803b20947e422ae666959317b5cb93b0a88ba892c4843efe2e6606 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9299a92b1803b20947e422ae666959317b5cb93b0a88ba892c4843efe2e6606->enter($__internal_a9299a92b1803b20947e422ae666959317b5cb93b0a88ba892c4843efe2e6606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_859c2390465286677e4310e4e294ba6c6e446a3d24766145810b8d3fee27389d->leave($__internal_859c2390465286677e4310e4e294ba6c6e446a3d24766145810b8d3fee27389d_prof);

        
        $__internal_a9299a92b1803b20947e422ae666959317b5cb93b0a88ba892c4843efe2e6606->leave($__internal_a9299a92b1803b20947e422ae666959317b5cb93b0a88ba892c4843efe2e6606_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\percent_widget.html.php");
    }
}
