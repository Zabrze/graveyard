<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_06b2ce1cda4cf4349cfb1ac13e07352eb356da3144616c431c40abcfc9160326 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2133b53234b0f2cf2c03389a3b75a75d3e48f81a9550bf0f868c92678f11feb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2133b53234b0f2cf2c03389a3b75a75d3e48f81a9550bf0f868c92678f11feb->enter($__internal_a2133b53234b0f2cf2c03389a3b75a75d3e48f81a9550bf0f868c92678f11feb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        $__internal_6e7e67698f9bcdd53338438dbf6ffa101fdb0a541ddd13b1a881c5a32504976b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e7e67698f9bcdd53338438dbf6ffa101fdb0a541ddd13b1a881c5a32504976b->enter($__internal_6e7e67698f9bcdd53338438dbf6ffa101fdb0a541ddd13b1a881c5a32504976b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_a2133b53234b0f2cf2c03389a3b75a75d3e48f81a9550bf0f868c92678f11feb->leave($__internal_a2133b53234b0f2cf2c03389a3b75a75d3e48f81a9550bf0f868c92678f11feb_prof);

        
        $__internal_6e7e67698f9bcdd53338438dbf6ffa101fdb0a541ddd13b1a881c5a32504976b->leave($__internal_6e7e67698f9bcdd53338438dbf6ffa101fdb0a541ddd13b1a881c5a32504976b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\repeated_row.html.php");
    }
}
