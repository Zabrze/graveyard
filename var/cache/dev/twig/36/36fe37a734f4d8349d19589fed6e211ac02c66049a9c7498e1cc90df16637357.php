<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_5f766dc1ba623aa00771436ea30ac7826055479f6629570edd362d5ce775847d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_886bb0ca32f62ca5e1f715ccb2f8a340adbdb236a9a4daef75a24ffde6cd69c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_886bb0ca32f62ca5e1f715ccb2f8a340adbdb236a9a4daef75a24ffde6cd69c9->enter($__internal_886bb0ca32f62ca5e1f715ccb2f8a340adbdb236a9a4daef75a24ffde6cd69c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_8081d8cdbdb46433cac23783e2e717de2464ab3bade9bd5b2acd5cb325af6335 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8081d8cdbdb46433cac23783e2e717de2464ab3bade9bd5b2acd5cb325af6335->enter($__internal_8081d8cdbdb46433cac23783e2e717de2464ab3bade9bd5b2acd5cb325af6335_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_886bb0ca32f62ca5e1f715ccb2f8a340adbdb236a9a4daef75a24ffde6cd69c9->leave($__internal_886bb0ca32f62ca5e1f715ccb2f8a340adbdb236a9a4daef75a24ffde6cd69c9_prof);

        
        $__internal_8081d8cdbdb46433cac23783e2e717de2464ab3bade9bd5b2acd5cb325af6335->leave($__internal_8081d8cdbdb46433cac23783e2e717de2464ab3bade9bd5b2acd5cb325af6335_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\integer_widget.html.php");
    }
}
