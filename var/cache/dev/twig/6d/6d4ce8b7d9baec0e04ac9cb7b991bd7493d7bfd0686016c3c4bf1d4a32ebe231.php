<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_a3befd5d08ceb339cb7d06b906f646730360c7ea0efc9092049009f640a48f14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0e790c2afe32f84b4a7c28178c904d50dba1cedda1a3b8b9887c263775afe396 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e790c2afe32f84b4a7c28178c904d50dba1cedda1a3b8b9887c263775afe396->enter($__internal_0e790c2afe32f84b4a7c28178c904d50dba1cedda1a3b8b9887c263775afe396_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $__internal_6ef83714dd49f84ac405386d5c4eb1580f67fb8327410b28c6646e869ebeafb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ef83714dd49f84ac405386d5c4eb1580f67fb8327410b28c6646e869ebeafb9->enter($__internal_6ef83714dd49f84ac405386d5c4eb1580f67fb8327410b28c6646e869ebeafb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0e790c2afe32f84b4a7c28178c904d50dba1cedda1a3b8b9887c263775afe396->leave($__internal_0e790c2afe32f84b4a7c28178c904d50dba1cedda1a3b8b9887c263775afe396_prof);

        
        $__internal_6ef83714dd49f84ac405386d5c4eb1580f67fb8327410b28c6646e869ebeafb9->leave($__internal_6ef83714dd49f84ac405386d5c4eb1580f67fb8327410b28c6646e869ebeafb9_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_411d615bdec1fc56e83dc7ca2c5e584318db034f0f41298d914ca81753b129ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_411d615bdec1fc56e83dc7ca2c5e584318db034f0f41298d914ca81753b129ad->enter($__internal_411d615bdec1fc56e83dc7ca2c5e584318db034f0f41298d914ca81753b129ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_10b07e5c3a82af4fc647aae8e4fc82d297f7879669ddaadbc3d67995c53ab40b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10b07e5c3a82af4fc647aae8e4fc82d297f7879669ddaadbc3d67995c53ab40b->enter($__internal_10b07e5c3a82af4fc647aae8e4fc82d297f7879669ddaadbc3d67995c53ab40b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "@FOSUser/Profile/show.html.twig", 4)->display($context);
        
        $__internal_10b07e5c3a82af4fc647aae8e4fc82d297f7879669ddaadbc3d67995c53ab40b->leave($__internal_10b07e5c3a82af4fc647aae8e4fc82d297f7879669ddaadbc3d67995c53ab40b_prof);

        
        $__internal_411d615bdec1fc56e83dc7ca2c5e584318db034f0f41298d914ca81753b129ad->leave($__internal_411d615bdec1fc56e83dc7ca2c5e584318db034f0f41298d914ca81753b129ad_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/show.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\show.html.twig");
    }
}
