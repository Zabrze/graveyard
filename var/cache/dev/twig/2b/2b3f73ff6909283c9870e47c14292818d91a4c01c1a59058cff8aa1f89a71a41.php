<?php

/* default/searchForm.html.twig */
class __TwigTemplate_a9e5022af51d5d8571cd7ba59e6154e5b4469a849b6fe7f365e81084f6eab251 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18b0886ff2349be6b97de2f326f8a707740d30f82ef659a00972400146531650 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18b0886ff2349be6b97de2f326f8a707740d30f82ef659a00972400146531650->enter($__internal_18b0886ff2349be6b97de2f326f8a707740d30f82ef659a00972400146531650_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/searchForm.html.twig"));

        $__internal_1741ba77037401cd6ef39f1e3eb4433242e573428234ec6943566bad4a72774c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1741ba77037401cd6ef39f1e3eb4433242e573428234ec6943566bad4a72774c->enter($__internal_1741ba77037401cd6ef39f1e3eb4433242e573428234ec6943566bad4a72774c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/searchForm.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "default/searchForm.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_18b0886ff2349be6b97de2f326f8a707740d30f82ef659a00972400146531650->leave($__internal_18b0886ff2349be6b97de2f326f8a707740d30f82ef659a00972400146531650_prof);

        
        $__internal_1741ba77037401cd6ef39f1e3eb4433242e573428234ec6943566bad4a72774c->leave($__internal_1741ba77037401cd6ef39f1e3eb4433242e573428234ec6943566bad4a72774c_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_7e4cbaa0eaa7f461f0aa17f7e52b1651be4d59986fa42b9c6b6bd6a6ed1d4b31 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e4cbaa0eaa7f461f0aa17f7e52b1651be4d59986fa42b9c6b6bd6a6ed1d4b31->enter($__internal_7e4cbaa0eaa7f461f0aa17f7e52b1651be4d59986fa42b9c6b6bd6a6ed1d4b31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8fe9e842f79d762b132f6ec0c0f0cc4c5def66caf328e6b5b711f165416e6091 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8fe9e842f79d762b132f6ec0c0f0cc4c5def66caf328e6b5b711f165416e6091->enter($__internal_8fe9e842f79d762b132f6ec0c0f0cc4c5def66caf328e6b5b711f165416e6091_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <form method=\"POST\">

        <div class=\"form-horizontal\" >
            <label for=\"name\">Imię:</label>
            <input type=\"text\" id=\"name\" name=\"name\" value=\"\">
        </div>
        <div class=\"form-horizontal\">
            <label for=\"surname\">Nazwisko:</label>
            <input type=\"text\" id=\"surname\" name=\"surname\" value=\"\">
        </div>
        <div class=\"form-horizontal\">
            <input type=\"submit\" class=\"btn\" value=\"Szukaj\" />
        </div>
    </form>
";
        
        $__internal_8fe9e842f79d762b132f6ec0c0f0cc4c5def66caf328e6b5b711f165416e6091->leave($__internal_8fe9e842f79d762b132f6ec0c0f0cc4c5def66caf328e6b5b711f165416e6091_prof);

        
        $__internal_7e4cbaa0eaa7f461f0aa17f7e52b1651be4d59986fa42b9c6b6bd6a6ed1d4b31->leave($__internal_7e4cbaa0eaa7f461f0aa17f7e52b1651be4d59986fa42b9c6b6bd6a6ed1d4b31_prof);

    }

    public function getTemplateName()
    {
        return "default/searchForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  31 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'base.html.twig' %}

{% block body %}
    <form method=\"POST\">

        <div class=\"form-horizontal\" >
            <label for=\"name\">Imię:</label>
            <input type=\"text\" id=\"name\" name=\"name\" value=\"\">
        </div>
        <div class=\"form-horizontal\">
            <label for=\"surname\">Nazwisko:</label>
            <input type=\"text\" id=\"surname\" name=\"surname\" value=\"\">
        </div>
        <div class=\"form-horizontal\">
            <input type=\"submit\" class=\"btn\" value=\"Szukaj\" />
        </div>
    </form>
{% endblock %}
", "default/searchForm.html.twig", "C:\\projects\\graveyard\\app\\Resources\\views\\default\\searchForm.html.twig");
    }
}
