<?php

/* @FOSUser/Profile/edit.html.twig */
class __TwigTemplate_d30c65a1bc4b21f10af81526c191d08b54981eddd8d6dd888feb19566e927cf9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e2696a0b7410e17bba717ec4cf5f79c04399fe50b98770a2390691e6f14c78ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2696a0b7410e17bba717ec4cf5f79c04399fe50b98770a2390691e6f14c78ca->enter($__internal_e2696a0b7410e17bba717ec4cf5f79c04399fe50b98770a2390691e6f14c78ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $__internal_60d2001dcac15d88c8f2d2d459aaec63fa0ffbfb52f12aba44b700d713b7f518 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60d2001dcac15d88c8f2d2d459aaec63fa0ffbfb52f12aba44b700d713b7f518->enter($__internal_60d2001dcac15d88c8f2d2d459aaec63fa0ffbfb52f12aba44b700d713b7f518_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e2696a0b7410e17bba717ec4cf5f79c04399fe50b98770a2390691e6f14c78ca->leave($__internal_e2696a0b7410e17bba717ec4cf5f79c04399fe50b98770a2390691e6f14c78ca_prof);

        
        $__internal_60d2001dcac15d88c8f2d2d459aaec63fa0ffbfb52f12aba44b700d713b7f518->leave($__internal_60d2001dcac15d88c8f2d2d459aaec63fa0ffbfb52f12aba44b700d713b7f518_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ec9b8ca29a09cfcc86f4d71fc95c984381ca975a610bdc9d27f205638bdaf0b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec9b8ca29a09cfcc86f4d71fc95c984381ca975a610bdc9d27f205638bdaf0b1->enter($__internal_ec9b8ca29a09cfcc86f4d71fc95c984381ca975a610bdc9d27f205638bdaf0b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_a6303d8b51fa0cd01581954fa11eceb18c82068428124e1c86bd23787c9e5688 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6303d8b51fa0cd01581954fa11eceb18c82068428124e1c86bd23787c9e5688->enter($__internal_a6303d8b51fa0cd01581954fa11eceb18c82068428124e1c86bd23787c9e5688_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "@FOSUser/Profile/edit.html.twig", 4)->display($context);
        
        $__internal_a6303d8b51fa0cd01581954fa11eceb18c82068428124e1c86bd23787c9e5688->leave($__internal_a6303d8b51fa0cd01581954fa11eceb18c82068428124e1c86bd23787c9e5688_prof);

        
        $__internal_ec9b8ca29a09cfcc86f4d71fc95c984381ca975a610bdc9d27f205638bdaf0b1->leave($__internal_ec9b8ca29a09cfcc86f4d71fc95c984381ca975a610bdc9d27f205638bdaf0b1_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/edit_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/edit.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\edit.html.twig");
    }
}
