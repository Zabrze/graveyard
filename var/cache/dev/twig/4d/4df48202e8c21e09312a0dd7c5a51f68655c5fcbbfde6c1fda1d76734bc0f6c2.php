<?php

/* @WebProfiler/Collector/exception.css.twig */
class __TwigTemplate_235507b6ccab229fd2ac989d3475873a20d6ccfdfdc33d4d6d71e33769cc901b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b811bdb13c4b41ec2d2fb77960b7f8b493ff4e917d14befa42eb098e34b4405 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b811bdb13c4b41ec2d2fb77960b7f8b493ff4e917d14befa42eb098e34b4405->enter($__internal_6b811bdb13c4b41ec2d2fb77960b7f8b493ff4e917d14befa42eb098e34b4405_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.css.twig"));

        $__internal_4a1075d1af1de4563939314a7b6e7bd2b4628e990beb290ba32de8f8b56409c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a1075d1af1de4563939314a7b6e7bd2b4628e990beb290ba32de8f8b56409c0->enter($__internal_4a1075d1af1de4563939314a7b6e7bd2b4628e990beb290ba32de8f8b56409c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.css.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
";
        
        $__internal_6b811bdb13c4b41ec2d2fb77960b7f8b493ff4e917d14befa42eb098e34b4405->leave($__internal_6b811bdb13c4b41ec2d2fb77960b7f8b493ff4e917d14befa42eb098e34b4405_prof);

        
        $__internal_4a1075d1af1de4563939314a7b6e7bd2b4628e990beb290ba32de8f8b56409c0->leave($__internal_4a1075d1af1de4563939314a7b6e7bd2b4628e990beb290ba32de8f8b56409c0_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/exception.css.twig') }}

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
", "@WebProfiler/Collector/exception.css.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.css.twig");
    }
}
