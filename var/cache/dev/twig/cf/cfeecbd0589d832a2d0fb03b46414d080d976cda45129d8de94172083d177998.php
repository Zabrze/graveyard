<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_20529d079b6463059d8454991ce531d22911166bf8332281ab27aa7c8a702fd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46ac8f80358727a3e5a109744fa419c19f101264a14c776658f464d46f38200f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46ac8f80358727a3e5a109744fa419c19f101264a14c776658f464d46f38200f->enter($__internal_46ac8f80358727a3e5a109744fa419c19f101264a14c776658f464d46f38200f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_26c52293138f9cd7c8ed51bca98f9879796a782e09229d823b4c0ef8f0a86472 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_26c52293138f9cd7c8ed51bca98f9879796a782e09229d823b4c0ef8f0a86472->enter($__internal_26c52293138f9cd7c8ed51bca98f9879796a782e09229d823b4c0ef8f0a86472_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_46ac8f80358727a3e5a109744fa419c19f101264a14c776658f464d46f38200f->leave($__internal_46ac8f80358727a3e5a109744fa419c19f101264a14c776658f464d46f38200f_prof);

        
        $__internal_26c52293138f9cd7c8ed51bca98f9879796a782e09229d823b4c0ef8f0a86472->leave($__internal_26c52293138f9cd7c8ed51bca98f9879796a782e09229d823b4c0ef8f0a86472_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6dec3d667b6f378f918c472d66f44ae1c0a584524e6e77979640a7b400f25c15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6dec3d667b6f378f918c472d66f44ae1c0a584524e6e77979640a7b400f25c15->enter($__internal_6dec3d667b6f378f918c472d66f44ae1c0a584524e6e77979640a7b400f25c15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_d02f2d6b3dde9551c767fa29fd8dc29c213452707d5376be3c4a601d2fa43ea4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d02f2d6b3dde9551c767fa29fd8dc29c213452707d5376be3c4a601d2fa43ea4->enter($__internal_d02f2d6b3dde9551c767fa29fd8dc29c213452707d5376be3c4a601d2fa43ea4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_d02f2d6b3dde9551c767fa29fd8dc29c213452707d5376be3c4a601d2fa43ea4->leave($__internal_d02f2d6b3dde9551c767fa29fd8dc29c213452707d5376be3c4a601d2fa43ea4_prof);

        
        $__internal_6dec3d667b6f378f918c472d66f44ae1c0a584524e6e77979640a7b400f25c15->leave($__internal_6dec3d667b6f378f918c472d66f44ae1c0a584524e6e77979640a7b400f25c15_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock fos_user_content %}
", "@FOSUser/Security/login.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Security\\login.html.twig");
    }
}
