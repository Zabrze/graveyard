<?php

/* @FOSUser/Group/show_content.html.twig */
class __TwigTemplate_8cb067fab8952f50f4bf5d82c9eec2096954155e4f4228629234efcd9de2cbb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e54844c9f2fa8e571b20710fa8c9a9c7a50b0833ed0c0245dd86526f60bd853 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e54844c9f2fa8e571b20710fa8c9a9c7a50b0833ed0c0245dd86526f60bd853->enter($__internal_8e54844c9f2fa8e571b20710fa8c9a9c7a50b0833ed0c0245dd86526f60bd853_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show_content.html.twig"));

        $__internal_53bd202c35041a45ade7cca29469b0d15d29ff0ee8d934e7b9c3b7bc143f8a07 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53bd202c35041a45ade7cca29469b0d15d29ff0ee8d934e7b9c3b7bc143f8a07->enter($__internal_53bd202c35041a45ade7cca29469b0d15d29ff0ee8d934e7b9c3b7bc143f8a07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_8e54844c9f2fa8e571b20710fa8c9a9c7a50b0833ed0c0245dd86526f60bd853->leave($__internal_8e54844c9f2fa8e571b20710fa8c9a9c7a50b0833ed0c0245dd86526f60bd853_prof);

        
        $__internal_53bd202c35041a45ade7cca29469b0d15d29ff0ee8d934e7b9c3b7bc143f8a07->leave($__internal_53bd202c35041a45ade7cca29469b0d15d29ff0ee8d934e7b9c3b7bc143f8a07_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "@FOSUser/Group/show_content.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\show_content.html.twig");
    }
}
