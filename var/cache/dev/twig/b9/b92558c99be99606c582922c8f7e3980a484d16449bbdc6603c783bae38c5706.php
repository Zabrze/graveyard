<?php

/* @FOSUser/Group/list_content.html.twig */
class __TwigTemplate_b87815137a73819241fda44743f9b5976cf90cbb0b5e7cfd73452e230b25c454 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2a55cec232723c8f6dbe5239c8e4c155914764ef1610c49e3c01b09f55e4b3f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a55cec232723c8f6dbe5239c8e4c155914764ef1610c49e3c01b09f55e4b3f7->enter($__internal_2a55cec232723c8f6dbe5239c8e4c155914764ef1610c49e3c01b09f55e4b3f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list_content.html.twig"));

        $__internal_76dd9d63440ab2b8fb38eefa9b1e74d373d83cfd1dfe2632657c11b33a114a16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76dd9d63440ab2b8fb38eefa9b1e74d373d83cfd1dfe2632657c11b33a114a16->enter($__internal_76dd9d63440ab2b8fb38eefa9b1e74d373d83cfd1dfe2632657c11b33a114a16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list_content.html.twig"));

        // line 1
        echo "<div class=\"fos_user_group_list\">
    <ul>
    ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")));
        foreach ($context['_seq'] as $context["_key"] => $context["group"]) {
            // line 4
            echo "        <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_group_show", array("groupName" => $this->getAttribute($context["group"], "getName", array(), "method"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["group"], "getName", array(), "method"), "html", null, true);
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 6
        echo "    </ul>
</div>
";
        
        $__internal_2a55cec232723c8f6dbe5239c8e4c155914764ef1610c49e3c01b09f55e4b3f7->leave($__internal_2a55cec232723c8f6dbe5239c8e4c155914764ef1610c49e3c01b09f55e4b3f7_prof);

        
        $__internal_76dd9d63440ab2b8fb38eefa9b1e74d373d83cfd1dfe2632657c11b33a114a16->leave($__internal_76dd9d63440ab2b8fb38eefa9b1e74d373d83cfd1dfe2632657c11b33a114a16_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/list_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 6,  33 => 4,  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"fos_user_group_list\">
    <ul>
    {% for group in groups %}
        <li><a href=\"{{ path('fos_user_group_show', {'groupName': group.getName()} ) }}\">{{ group.getName() }}</a></li>
    {% endfor %}
    </ul>
</div>
", "@FOSUser/Group/list_content.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\list_content.html.twig");
    }
}
