<?php

/* @Framework/Form/color_widget.html.php */
class __TwigTemplate_fc0f651c65593b08079f30317d382ad7503a94106f7e15bc094be7a204317d80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_329fdf6a86871b0975eea162f85f99f2dd2ba621370d6d2ceb8a4c1a092d2861 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_329fdf6a86871b0975eea162f85f99f2dd2ba621370d6d2ceb8a4c1a092d2861->enter($__internal_329fdf6a86871b0975eea162f85f99f2dd2ba621370d6d2ceb8a4c1a092d2861_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/color_widget.html.php"));

        $__internal_ff36d8dabceb96dc1e7a245a6487bd0fe01a9f6bc804176194794297d42ca223 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff36d8dabceb96dc1e7a245a6487bd0fe01a9f6bc804176194794297d42ca223->enter($__internal_ff36d8dabceb96dc1e7a245a6487bd0fe01a9f6bc804176194794297d42ca223_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/color_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'color'));
";
        
        $__internal_329fdf6a86871b0975eea162f85f99f2dd2ba621370d6d2ceb8a4c1a092d2861->leave($__internal_329fdf6a86871b0975eea162f85f99f2dd2ba621370d6d2ceb8a4c1a092d2861_prof);

        
        $__internal_ff36d8dabceb96dc1e7a245a6487bd0fe01a9f6bc804176194794297d42ca223->leave($__internal_ff36d8dabceb96dc1e7a245a6487bd0fe01a9f6bc804176194794297d42ca223_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/color_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'color'));
", "@Framework/Form/color_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\color_widget.html.php");
    }
}
