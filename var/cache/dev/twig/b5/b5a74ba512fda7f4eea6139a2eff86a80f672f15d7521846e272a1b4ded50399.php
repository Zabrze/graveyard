<?php

/* @FOSUser/Profile/show_content.html.twig */
class __TwigTemplate_89f53452f19f6a831b152667d059d1cc54ccb7df36813ace8314bed60881f54c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe28d1a64c801b95934f36cc51361133fda34ec5f27dfa367d2354b113aae7f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe28d1a64c801b95934f36cc51361133fda34ec5f27dfa367d2354b113aae7f4->enter($__internal_fe28d1a64c801b95934f36cc51361133fda34ec5f27dfa367d2354b113aae7f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show_content.html.twig"));

        $__internal_040557a7ec7908632acd26b079cfa815019f1ef03756292f41be4cc4bee5687a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_040557a7ec7908632acd26b079cfa815019f1ef03756292f41be4cc4bee5687a->enter($__internal_040557a7ec7908632acd26b079cfa815019f1ef03756292f41be4cc4bee5687a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_user_show\">
    <div class=\"form-horizontal\">
        <p>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</p>
    </div>
    <div class=\"form-horizontal\">
        <p>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array()), "html", null, true);
        echo "</p>
    </div>
    <div class=\"form-horizontal\">
        <br/> <input type=\"submit\" class=\"btn\" value=\"wyświetl pełny profil\" />
    </div>
</div>
";
        
        $__internal_fe28d1a64c801b95934f36cc51361133fda34ec5f27dfa367d2354b113aae7f4->leave($__internal_fe28d1a64c801b95934f36cc51361133fda34ec5f27dfa367d2354b113aae7f4_prof);

        
        $__internal_040557a7ec7908632acd26b079cfa815019f1ef03756292f41be4cc4bee5687a->leave($__internal_040557a7ec7908632acd26b079cfa815019f1ef03756292f41be4cc4bee5687a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  30 => 5,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_user_show\">
    <div class=\"form-horizontal\">
        <p>{{ 'profile.show.username'|trans }}: {{ user.username }}</p>
    </div>
    <div class=\"form-horizontal\">
        <p>{{ 'profile.show.email'|trans }}: {{ user.email }}</p>
    </div>
    <div class=\"form-horizontal\">
        <br/> <input type=\"submit\" class=\"btn\" value=\"wyświetl pełny profil\" />
    </div>
</div>
", "@FOSUser/Profile/show_content.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\show_content.html.twig");
    }
}
