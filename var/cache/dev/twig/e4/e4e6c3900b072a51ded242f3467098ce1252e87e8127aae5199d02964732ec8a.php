<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_a32a2300229e65097bb9c052c2988d133c1507631ac9bb906f431e2ec968fcfa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de908c41b0fc1a804435f8ee09e0f99d9c6f42822789e3946a174318532553e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de908c41b0fc1a804435f8ee09e0f99d9c6f42822789e3946a174318532553e4->enter($__internal_de908c41b0fc1a804435f8ee09e0f99d9c6f42822789e3946a174318532553e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_b900e4377fd80e9f713bef624f3394533acbe19415f23e071a875672872edb8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b900e4377fd80e9f713bef624f3394533acbe19415f23e071a875672872edb8e->enter($__internal_b900e4377fd80e9f713bef624f3394533acbe19415f23e071a875672872edb8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_de908c41b0fc1a804435f8ee09e0f99d9c6f42822789e3946a174318532553e4->leave($__internal_de908c41b0fc1a804435f8ee09e0f99d9c6f42822789e3946a174318532553e4_prof);

        
        $__internal_b900e4377fd80e9f713bef624f3394533acbe19415f23e071a875672872edb8e->leave($__internal_b900e4377fd80e9f713bef624f3394533acbe19415f23e071a875672872edb8e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\reset_widget.html.php");
    }
}
