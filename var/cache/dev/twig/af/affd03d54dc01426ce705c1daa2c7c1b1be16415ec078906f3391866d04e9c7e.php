<?php

/* bootstrap_4_horizontal_layout.html.twig */
class __TwigTemplate_7ec69c8e0d874336fed1ee7456a2fe4f8f4d14317f6124e26f3668629305050f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_4_layout.html.twig", "bootstrap_4_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_4_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'fieldset_form_row' => array($this, 'block_fieldset_form_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5068c33345d06a6b564913abd28843f09d1d8ff20b77b43cee1bec86d76a6e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5068c33345d06a6b564913abd28843f09d1d8ff20b77b43cee1bec86d76a6e0->enter($__internal_e5068c33345d06a6b564913abd28843f09d1d8ff20b77b43cee1bec86d76a6e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_horizontal_layout.html.twig"));

        $__internal_9f8023b6139ba28e0b5f9da741f01eccb6589984ee19b6d0f78e8df54b743289 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f8023b6139ba28e0b5f9da741f01eccb6589984ee19b6d0f78e8df54b743289->enter($__internal_9f8023b6139ba28e0b5f9da741f01eccb6589984ee19b6d0f78e8df54b743289_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_label', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 20
        echo "
";
        // line 22
        echo "
";
        // line 23
        $this->displayBlock('form_row', $context, $blocks);
        // line 36
        echo "
";
        // line 37
        $this->displayBlock('fieldset_form_row', $context, $blocks);
        // line 48
        echo "
";
        // line 49
        $this->displayBlock('submit_row', $context, $blocks);
        // line 57
        echo "
";
        // line 58
        $this->displayBlock('reset_row', $context, $blocks);
        // line 66
        echo "
";
        // line 67
        $this->displayBlock('form_group_class', $context, $blocks);
        // line 70
        echo "
";
        // line 71
        $this->displayBlock('checkbox_row', $context, $blocks);
        
        $__internal_e5068c33345d06a6b564913abd28843f09d1d8ff20b77b43cee1bec86d76a6e0->leave($__internal_e5068c33345d06a6b564913abd28843f09d1d8ff20b77b43cee1bec86d76a6e0_prof);

        
        $__internal_9f8023b6139ba28e0b5f9da741f01eccb6589984ee19b6d0f78e8df54b743289->leave($__internal_9f8023b6139ba28e0b5f9da741f01eccb6589984ee19b6d0f78e8df54b743289_prof);

    }

    // line 5
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_6f1115c4c7ecb6370a585fd77c06cea90e0abe94b4b6311af9beb926367ac2ea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f1115c4c7ecb6370a585fd77c06cea90e0abe94b4b6311af9beb926367ac2ea->enter($__internal_6f1115c4c7ecb6370a585fd77c06cea90e0abe94b4b6311af9beb926367ac2ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_4fbcb65f03d954f4f95936ad378aa601e57ec1f8d24551acd92d4771964a9b6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fbcb65f03d954f4f95936ad378aa601e57ec1f8d24551acd92d4771964a9b6b->enter($__internal_4fbcb65f03d954f4f95936ad378aa601e57ec1f8d24551acd92d4771964a9b6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 6
        if (((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) {
            // line 7
            echo "<div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>";
        } else {
            // line 9
            if (( !array_key_exists("expanded", $context) ||  !(isset($context["expanded"]) ? $context["expanded"] : $this->getContext($context, "expanded")))) {
                // line 10
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " col-form-label"))));
            }
            // line 12
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 13
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        
        $__internal_4fbcb65f03d954f4f95936ad378aa601e57ec1f8d24551acd92d4771964a9b6b->leave($__internal_4fbcb65f03d954f4f95936ad378aa601e57ec1f8d24551acd92d4771964a9b6b_prof);

        
        $__internal_6f1115c4c7ecb6370a585fd77c06cea90e0abe94b4b6311af9beb926367ac2ea->leave($__internal_6f1115c4c7ecb6370a585fd77c06cea90e0abe94b4b6311af9beb926367ac2ea_prof);

    }

    // line 17
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_5f1a440480f6529777696470be649ba74e1375b84d2bf028042f2908e895fabe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f1a440480f6529777696470be649ba74e1375b84d2bf028042f2908e895fabe->enter($__internal_5f1a440480f6529777696470be649ba74e1375b84d2bf028042f2908e895fabe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_bcf100e5955c93db86f7bc280a8e5aff8ec90921d4ff033eb51f7a537c07a32d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcf100e5955c93db86f7bc280a8e5aff8ec90921d4ff033eb51f7a537c07a32d->enter($__internal_bcf100e5955c93db86f7bc280a8e5aff8ec90921d4ff033eb51f7a537c07a32d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        // line 18
        echo "col-sm-2";
        
        $__internal_bcf100e5955c93db86f7bc280a8e5aff8ec90921d4ff033eb51f7a537c07a32d->leave($__internal_bcf100e5955c93db86f7bc280a8e5aff8ec90921d4ff033eb51f7a537c07a32d_prof);

        
        $__internal_5f1a440480f6529777696470be649ba74e1375b84d2bf028042f2908e895fabe->leave($__internal_5f1a440480f6529777696470be649ba74e1375b84d2bf028042f2908e895fabe_prof);

    }

    // line 23
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_d5613d4a5c4e9a768dcc994233969f38255bacfea326feead62f40eb9d15c148 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5613d4a5c4e9a768dcc994233969f38255bacfea326feead62f40eb9d15c148->enter($__internal_d5613d4a5c4e9a768dcc994233969f38255bacfea326feead62f40eb9d15c148_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_974030cfc1789e79367160c48c41cfaafb19ae994957e2af9304c2e5aaae7d9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_974030cfc1789e79367160c48c41cfaafb19ae994957e2af9304c2e5aaae7d9a->enter($__internal_974030cfc1789e79367160c48c41cfaafb19ae994957e2af9304c2e5aaae7d9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 24
        if ((array_key_exists("expanded", $context) && (isset($context["expanded"]) ? $context["expanded"] : $this->getContext($context, "expanded")))) {
            // line 25
            $this->displayBlock("fieldset_form_row", $context, $blocks);
        } else {
            // line 27
            echo "<div class=\"form-group row";
            if ((( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter((isset($context["force_error"]) ? $context["force_error"] : $this->getContext($context, "force_error")), false)) : (false))) &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
                echo " is-invalid";
            }
            echo "\">";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
            // line 29
            echo "<div class=\"";
            $this->displayBlock("form_group_class", $context, $blocks);
            echo "\">";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            // line 32
            echo "</div>
    ";
            // line 33
            echo "</div>";
        }
        
        $__internal_974030cfc1789e79367160c48c41cfaafb19ae994957e2af9304c2e5aaae7d9a->leave($__internal_974030cfc1789e79367160c48c41cfaafb19ae994957e2af9304c2e5aaae7d9a_prof);

        
        $__internal_d5613d4a5c4e9a768dcc994233969f38255bacfea326feead62f40eb9d15c148->leave($__internal_d5613d4a5c4e9a768dcc994233969f38255bacfea326feead62f40eb9d15c148_prof);

    }

    // line 37
    public function block_fieldset_form_row($context, array $blocks = array())
    {
        $__internal_d5dd31407e3e8e82ba77102d11207eb4d12d6f81da8a1f82425effbe0801f55c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5dd31407e3e8e82ba77102d11207eb4d12d6f81da8a1f82425effbe0801f55c->enter($__internal_d5dd31407e3e8e82ba77102d11207eb4d12d6f81da8a1f82425effbe0801f55c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fieldset_form_row"));

        $__internal_27719c699c0b00f925262c17970e203e6d996a0beff6c4a837f04298b62bd6dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_27719c699c0b00f925262c17970e203e6d996a0beff6c4a837f04298b62bd6dc->enter($__internal_27719c699c0b00f925262c17970e203e6d996a0beff6c4a837f04298b62bd6dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fieldset_form_row"));

        // line 38
        echo "<fieldset class=\"form-group\">
        <div class=\"row";
        // line 39
        if ((( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter((isset($context["force_error"]) ? $context["force_error"] : $this->getContext($context, "force_error")), false)) : (false))) &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            echo " is-invalid";
        }
        echo "\">";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        // line 41
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 44
        echo "</div>
        </div>
";
        // line 46
        echo "</fieldset>";
        
        $__internal_27719c699c0b00f925262c17970e203e6d996a0beff6c4a837f04298b62bd6dc->leave($__internal_27719c699c0b00f925262c17970e203e6d996a0beff6c4a837f04298b62bd6dc_prof);

        
        $__internal_d5dd31407e3e8e82ba77102d11207eb4d12d6f81da8a1f82425effbe0801f55c->leave($__internal_d5dd31407e3e8e82ba77102d11207eb4d12d6f81da8a1f82425effbe0801f55c_prof);

    }

    // line 49
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_2fb085c2c9935f0c1462a534fe5e5a0d91eef511d0fb5de0dad0b9a08acb5dcf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2fb085c2c9935f0c1462a534fe5e5a0d91eef511d0fb5de0dad0b9a08acb5dcf->enter($__internal_2fb085c2c9935f0c1462a534fe5e5a0d91eef511d0fb5de0dad0b9a08acb5dcf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_5bd7965cfba774837464f3bc673d594122f703289c990351e0305030ef0120ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5bd7965cfba774837464f3bc673d594122f703289c990351e0305030ef0120ec->enter($__internal_5bd7965cfba774837464f3bc673d594122f703289c990351e0305030ef0120ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 50
        echo "<div class=\"form-group row\">";
        // line 51
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 52
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 54
        echo "</div>";
        // line 55
        echo "</div>";
        
        $__internal_5bd7965cfba774837464f3bc673d594122f703289c990351e0305030ef0120ec->leave($__internal_5bd7965cfba774837464f3bc673d594122f703289c990351e0305030ef0120ec_prof);

        
        $__internal_2fb085c2c9935f0c1462a534fe5e5a0d91eef511d0fb5de0dad0b9a08acb5dcf->leave($__internal_2fb085c2c9935f0c1462a534fe5e5a0d91eef511d0fb5de0dad0b9a08acb5dcf_prof);

    }

    // line 58
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_173cd674e8f765919ddc643feb101e5e08b380d94a1e19730fc8ff5adf6856e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_173cd674e8f765919ddc643feb101e5e08b380d94a1e19730fc8ff5adf6856e3->enter($__internal_173cd674e8f765919ddc643feb101e5e08b380d94a1e19730fc8ff5adf6856e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_c43c29f4afde7f59b2195ff62866970c58cef480213db88510d795ccaac1bd7a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c43c29f4afde7f59b2195ff62866970c58cef480213db88510d795ccaac1bd7a->enter($__internal_c43c29f4afde7f59b2195ff62866970c58cef480213db88510d795ccaac1bd7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 59
        echo "<div class=\"form-group row\">";
        // line 60
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 61
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 63
        echo "</div>";
        // line 64
        echo "</div>";
        
        $__internal_c43c29f4afde7f59b2195ff62866970c58cef480213db88510d795ccaac1bd7a->leave($__internal_c43c29f4afde7f59b2195ff62866970c58cef480213db88510d795ccaac1bd7a_prof);

        
        $__internal_173cd674e8f765919ddc643feb101e5e08b380d94a1e19730fc8ff5adf6856e3->leave($__internal_173cd674e8f765919ddc643feb101e5e08b380d94a1e19730fc8ff5adf6856e3_prof);

    }

    // line 67
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_ae2fb8a78b821c96b6610713ccb041833ce823862e5e6ec004d18bfc62b934fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae2fb8a78b821c96b6610713ccb041833ce823862e5e6ec004d18bfc62b934fd->enter($__internal_ae2fb8a78b821c96b6610713ccb041833ce823862e5e6ec004d18bfc62b934fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_c4454d78d87b0b1e4f59914f1634830331fec63c027f60ded8002de3e888bd41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4454d78d87b0b1e4f59914f1634830331fec63c027f60ded8002de3e888bd41->enter($__internal_c4454d78d87b0b1e4f59914f1634830331fec63c027f60ded8002de3e888bd41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        // line 68
        echo "col-sm-10";
        
        $__internal_c4454d78d87b0b1e4f59914f1634830331fec63c027f60ded8002de3e888bd41->leave($__internal_c4454d78d87b0b1e4f59914f1634830331fec63c027f60ded8002de3e888bd41_prof);

        
        $__internal_ae2fb8a78b821c96b6610713ccb041833ce823862e5e6ec004d18bfc62b934fd->leave($__internal_ae2fb8a78b821c96b6610713ccb041833ce823862e5e6ec004d18bfc62b934fd_prof);

    }

    // line 71
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_1bb0469fd34d8b7dc5c04536999e98f212edb29347bb03fdb11d84a117a89b62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bb0469fd34d8b7dc5c04536999e98f212edb29347bb03fdb11d84a117a89b62->enter($__internal_1bb0469fd34d8b7dc5c04536999e98f212edb29347bb03fdb11d84a117a89b62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_6a2903381d4c9014680985cb4f66850eeb270c30a9dcfbab0bb85e21be68ca64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a2903381d4c9014680985cb4f66850eeb270c30a9dcfbab0bb85e21be68ca64->enter($__internal_6a2903381d4c9014680985cb4f66850eeb270c30a9dcfbab0bb85e21be68ca64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 72
        echo "<div class=\"form-group row\">";
        // line 73
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 74
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 77
        echo "</div>";
        // line 78
        echo "</div>";
        
        $__internal_6a2903381d4c9014680985cb4f66850eeb270c30a9dcfbab0bb85e21be68ca64->leave($__internal_6a2903381d4c9014680985cb4f66850eeb270c30a9dcfbab0bb85e21be68ca64_prof);

        
        $__internal_1bb0469fd34d8b7dc5c04536999e98f212edb29347bb03fdb11d84a117a89b62->leave($__internal_1bb0469fd34d8b7dc5c04536999e98f212edb29347bb03fdb11d84a117a89b62_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_4_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  347 => 78,  345 => 77,  343 => 76,  341 => 75,  337 => 74,  333 => 73,  331 => 72,  322 => 71,  312 => 68,  303 => 67,  293 => 64,  291 => 63,  289 => 62,  285 => 61,  281 => 60,  279 => 59,  270 => 58,  260 => 55,  258 => 54,  256 => 53,  252 => 52,  248 => 51,  246 => 50,  237 => 49,  227 => 46,  223 => 44,  221 => 43,  219 => 42,  215 => 41,  213 => 40,  208 => 39,  205 => 38,  196 => 37,  185 => 33,  182 => 32,  180 => 31,  178 => 30,  174 => 29,  172 => 28,  166 => 27,  163 => 25,  161 => 24,  152 => 23,  142 => 18,  133 => 17,  122 => 13,  120 => 12,  117 => 10,  115 => 9,  110 => 7,  108 => 6,  99 => 5,  89 => 71,  86 => 70,  84 => 67,  81 => 66,  79 => 58,  76 => 57,  74 => 49,  71 => 48,  69 => 37,  66 => 36,  64 => 23,  61 => 22,  58 => 20,  56 => 17,  53 => 16,  51 => 5,  48 => 4,  45 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_4_layout.html.twig\" %}

{# Labels #}

{% block form_label -%}
    {%- if label is same as(false) -%}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {%- else -%}
        {%- if expanded is not defined or not expanded -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' col-form-label')|trim}) -%}
        {%- endif -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) -%}
        {{- parent() -}}
    {%- endif -%}
{%- endblock form_label %}

{% block form_label_class -%}
col-sm-2
{%- endblock form_label_class %}

{# Rows #}

{% block form_row -%}
    {%- if expanded is defined and expanded -%}
        {{ block('fieldset_form_row') }}
    {%- else -%}
        <div class=\"form-group row{% if (not compound or force_error|default(false)) and not valid %} is-invalid{% endif %}\">
            {{- form_label(form) -}}
            <div class=\"{{ block('form_group_class') }}\">
                {{- form_widget(form) -}}
                {{- form_errors(form) -}}
            </div>
    {##}</div>
    {%- endif -%}
{%- endblock form_row %}

{% block fieldset_form_row -%}
    <fieldset class=\"form-group\">
        <div class=\"row{% if (not compound or force_error|default(false)) and not valid %} is-invalid{% endif %}\">
            {{- form_label(form) -}}
            <div class=\"{{ block('form_group_class') }}\">
                {{- form_widget(form) -}}
                {{- form_errors(form) -}}
            </div>
        </div>
{##}</fieldset>
{%- endblock fieldset_form_row %}

{% block submit_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock submit_row %}

{% block reset_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock reset_row %}

{% block form_group_class -%}
col-sm-10
{%- endblock form_group_class %}

{% block checkbox_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>{#--#}
    </div>
{%- endblock checkbox_row %}
", "bootstrap_4_horizontal_layout.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_4_horizontal_layout.html.twig");
    }
}
