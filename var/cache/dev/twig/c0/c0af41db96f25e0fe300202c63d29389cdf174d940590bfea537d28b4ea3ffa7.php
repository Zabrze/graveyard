<?php

/* default/deadView.html.twig */
class __TwigTemplate_f73a5ea23b26392a35f586de5ed915ebfe4ba5e83f91961af7167c0ea77c94a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8915bcc7c1fa0311e603210b8503313b7d35915890f216bf3edb7f4828077225 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8915bcc7c1fa0311e603210b8503313b7d35915890f216bf3edb7f4828077225->enter($__internal_8915bcc7c1fa0311e603210b8503313b7d35915890f216bf3edb7f4828077225_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/deadView.html.twig"));

        $__internal_bb13b34fad4a905c978b91d7b058c2cd7f746792ed2401a25130d5dfe0ecc230 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb13b34fad4a905c978b91d7b058c2cd7f746792ed2401a25130d5dfe0ecc230->enter($__internal_bb13b34fad4a905c978b91d7b058c2cd7f746792ed2401a25130d5dfe0ecc230_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/deadView.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "default/deadView.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_8915bcc7c1fa0311e603210b8503313b7d35915890f216bf3edb7f4828077225->leave($__internal_8915bcc7c1fa0311e603210b8503313b7d35915890f216bf3edb7f4828077225_prof);

        
        $__internal_bb13b34fad4a905c978b91d7b058c2cd7f746792ed2401a25130d5dfe0ecc230->leave($__internal_bb13b34fad4a905c978b91d7b058c2cd7f746792ed2401a25130d5dfe0ecc230_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_7f5804f15b49c7e4578c216e80c9cb90cfd30bb8899dfd20ff4b31385ad88583 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f5804f15b49c7e4578c216e80c9cb90cfd30bb8899dfd20ff4b31385ad88583->enter($__internal_7f5804f15b49c7e4578c216e80c9cb90cfd30bb8899dfd20ff4b31385ad88583_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a6b8ccba715159c9f2ad95f9d1789c445dc77e176762f8dfa302719a4a3fa4c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6b8ccba715159c9f2ad95f9d1789c445dc77e176762f8dfa302719a4a3fa4c2->enter($__internal_a6b8ccba715159c9f2ad95f9d1789c445dc77e176762f8dfa302719a4a3fa4c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    ";
        // line 5
        if ((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users"))) {
            echo " 
        ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
            foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                // line 7
                echo "            <div class=\"container\">
                <h3>Zmarły:</h3><br/>
                <p> 
                    Imię: ";
                // line 10
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "name", array()), "html", null, true);
                echo "<br/> Nazwisko: ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "surname", array()), "html", null, true);
                echo "
                    <br/> Data urodzenia: ";
                // line 11
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "birthDate", array()), "d-m-Y"), "html", null, true);
                echo "
                    <br/> Data zgonu: ";
                // line 12
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "deathDate", array()), "d-m-Y"), "html", null, true);
                echo "
                    <br/> Data pochówku: ";
                // line 13
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["user"], "funeralDate", array()), "d-m-Y"), "html", null, true);
                echo "
                    <br/> <button type=\"button\"> <a href=\"/display/";
                // line 14
                echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "id", array()), "html", null, true);
                echo "\">Wyświetl pełny profil</a></button>
                </p>
            </div>

    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "
";
        } else {
            // line 21
            echo "    <div class=\"form-horizontal\" <span id=\"err\">Brak osoby w bazie!</span></div>
";
        }
        // line 22
        echo "  
";
        
        $__internal_a6b8ccba715159c9f2ad95f9d1789c445dc77e176762f8dfa302719a4a3fa4c2->leave($__internal_a6b8ccba715159c9f2ad95f9d1789c445dc77e176762f8dfa302719a4a3fa4c2_prof);

        
        $__internal_7f5804f15b49c7e4578c216e80c9cb90cfd30bb8899dfd20ff4b31385ad88583->leave($__internal_7f5804f15b49c7e4578c216e80c9cb90cfd30bb8899dfd20ff4b31385ad88583_prof);

    }

    public function getTemplateName()
    {
        return "default/deadView.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 22,  98 => 21,  94 => 19,  83 => 14,  79 => 13,  75 => 12,  71 => 11,  65 => 10,  60 => 7,  56 => 6,  52 => 5,  49 => 4,  31 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'base.html.twig' %}

{% block body %}

    {% if users %} 
        {% for user in users %}
            <div class=\"container\">
                <h3>Zmarły:</h3><br/>
                <p> 
                    Imię: {{ user.name }}<br/> Nazwisko: {{ user.surname }}
                    <br/> Data urodzenia: {{ user.birthDate|date('d-m-Y') }}
                    <br/> Data zgonu: {{ user.deathDate|date('d-m-Y') }}
                    <br/> Data pochówku: {{ user.funeralDate|date('d-m-Y')}}
                    <br/> <button type=\"button\"> <a href=\"/display/{{user.id}}\">Wyświetl pełny profil</a></button>
                </p>
            </div>

    {% endfor %}

{% else %}
    <div class=\"form-horizontal\" <span id=\"err\">Brak osoby w bazie!</span></div>
{% endif %}  
{% endblock %}", "default/deadView.html.twig", "C:\\projects\\graveyard\\app\\Resources\\views\\default\\deadView.html.twig");
    }
}
