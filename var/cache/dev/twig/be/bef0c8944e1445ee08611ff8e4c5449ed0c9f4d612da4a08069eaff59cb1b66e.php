<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_ce92e9ff632d23a8124d6f3c9913552e588320e10026530b34f962ac1d8df186 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9447adc9be885f966018e2de48aa1ef451fcf81865b11ddac84a4e843d97e44f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9447adc9be885f966018e2de48aa1ef451fcf81865b11ddac84a4e843d97e44f->enter($__internal_9447adc9be885f966018e2de48aa1ef451fcf81865b11ddac84a4e843d97e44f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_debb7f1d2c620c51699146c34a80d4aeefb6d3639f58f762c7998d94ae8b6904 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_debb7f1d2c620c51699146c34a80d4aeefb6d3639f58f762c7998d94ae8b6904->enter($__internal_debb7f1d2c620c51699146c34a80d4aeefb6d3639f58f762c7998d94ae8b6904_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_9447adc9be885f966018e2de48aa1ef451fcf81865b11ddac84a4e843d97e44f->leave($__internal_9447adc9be885f966018e2de48aa1ef451fcf81865b11ddac84a4e843d97e44f_prof);

        
        $__internal_debb7f1d2c620c51699146c34a80d4aeefb6d3639f58f762c7998d94ae8b6904->leave($__internal_debb7f1d2c620c51699146c34a80d4aeefb6d3639f58f762c7998d94ae8b6904_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form); ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form); ?>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\form_row.html.php");
    }
}
