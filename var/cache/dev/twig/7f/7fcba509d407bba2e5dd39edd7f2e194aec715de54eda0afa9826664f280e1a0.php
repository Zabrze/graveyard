<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_ec3ac6b739e785c8aef676a8943024fd86c75767a041f0f1b05816b8d359aa2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_603879f5d00d93e8d347ba682b81db4ebefe1b1cdb2fd34d5847c32d780a7b16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_603879f5d00d93e8d347ba682b81db4ebefe1b1cdb2fd34d5847c32d780a7b16->enter($__internal_603879f5d00d93e8d347ba682b81db4ebefe1b1cdb2fd34d5847c32d780a7b16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_9a1b7f2d1994f0edea482f0abe1ba423ddf73b767b935f4e2e605c303dab8c59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a1b7f2d1994f0edea482f0abe1ba423ddf73b767b935f4e2e605c303dab8c59->enter($__internal_9a1b7f2d1994f0edea482f0abe1ba423ddf73b767b935f4e2e605c303dab8c59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_603879f5d00d93e8d347ba682b81db4ebefe1b1cdb2fd34d5847c32d780a7b16->leave($__internal_603879f5d00d93e8d347ba682b81db4ebefe1b1cdb2fd34d5847c32d780a7b16_prof);

        
        $__internal_9a1b7f2d1994f0edea482f0abe1ba423ddf73b767b935f4e2e605c303dab8c59->leave($__internal_9a1b7f2d1994f0edea482f0abe1ba423ddf73b767b935f4e2e605c303dab8c59_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\submit_widget.html.php");
    }
}
