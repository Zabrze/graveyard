<?php

/* bootstrap_4_layout.html.twig */
class __TwigTemplate_78fa470282a7f3f83ee1be91da1d9b0400bcf831b4d7868a55b947e495271455 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_base_layout.html.twig", "bootstrap_4_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_base_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'money_widget' => array($this, 'block_money_widget'),
                'datetime_widget' => array($this, 'block_datetime_widget'),
                'date_widget' => array($this, 'block_date_widget'),
                'time_widget' => array($this, 'block_time_widget'),
                'dateinterval_widget' => array($this, 'block_dateinterval_widget'),
                'percent_widget' => array($this, 'block_percent_widget'),
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'widget_attributes' => array($this, 'block_widget_attributes'),
                'button_widget' => array($this, 'block_button_widget'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
                'form_label' => array($this, 'block_form_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'form_errors' => array($this, 'block_form_errors'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_774de65705bd8980687b4a8a69e4c44f90ab93c021963541e3ca68e6f0705a6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_774de65705bd8980687b4a8a69e4c44f90ab93c021963541e3ca68e6f0705a6f->enter($__internal_774de65705bd8980687b4a8a69e4c44f90ab93c021963541e3ca68e6f0705a6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_layout.html.twig"));

        $__internal_7c4f924a848b52a7a2636dece9aba0e5e73781ec7bdc62c3e100dda86dee952f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c4f924a848b52a7a2636dece9aba0e5e73781ec7bdc62c3e100dda86dee952f->enter($__internal_7c4f924a848b52a7a2636dece9aba0e5e73781ec7bdc62c3e100dda86dee952f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('money_widget', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('date_widget', $context, $blocks);
        // line 28
        echo "
";
        // line 29
        $this->displayBlock('time_widget', $context, $blocks);
        // line 36
        echo "
";
        // line 37
        $this->displayBlock('dateinterval_widget', $context, $blocks);
        // line 44
        echo "
";
        // line 45
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 52
        echo "
";
        // line 53
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 60
        $this->displayBlock('widget_attributes', $context, $blocks);
        // line 67
        $this->displayBlock('button_widget', $context, $blocks);
        // line 71
        echo "
";
        // line 72
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 87
        echo "
";
        // line 88
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 99
        echo "
";
        // line 100
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 124
        echo "
";
        // line 126
        echo "
";
        // line 127
        $this->displayBlock('form_label', $context, $blocks);
        // line 136
        echo "
";
        // line 137
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 162
        echo "
";
        // line 164
        echo "
";
        // line 165
        $this->displayBlock('form_row', $context, $blocks);
        // line 175
        echo "
";
        // line 177
        echo "
";
        // line 178
        $this->displayBlock('form_errors', $context, $blocks);
        
        $__internal_774de65705bd8980687b4a8a69e4c44f90ab93c021963541e3ca68e6f0705a6f->leave($__internal_774de65705bd8980687b4a8a69e4c44f90ab93c021963541e3ca68e6f0705a6f_prof);

        
        $__internal_7c4f924a848b52a7a2636dece9aba0e5e73781ec7bdc62c3e100dda86dee952f->leave($__internal_7c4f924a848b52a7a2636dece9aba0e5e73781ec7bdc62c3e100dda86dee952f_prof);

    }

    // line 5
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_518da2556983053da07c79811c08c45b4ba3759eeed107b95849dbd54284e6d5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_518da2556983053da07c79811c08c45b4ba3759eeed107b95849dbd54284e6d5->enter($__internal_518da2556983053da07c79811c08c45b4ba3759eeed107b95849dbd54284e6d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_86eedb89a34daea46c8dc299ad668ed4f69c84846c8a8150e78faf4730e6cbe1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86eedb89a34daea46c8dc299ad668ed4f69c84846c8a8150e78faf4730e6cbe1->enter($__internal_86eedb89a34daea46c8dc299ad668ed4f69c84846c8a8150e78faf4730e6cbe1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 6
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            // line 7
            echo "        ";
            $context["group_class"] = " form-control is-invalid";
            // line 8
            echo "        ";
            $context["valid"] = true;
            // line 9
            echo "    ";
        }
        // line 10
        $this->displayParentBlock("money_widget", $context, $blocks);
        
        $__internal_86eedb89a34daea46c8dc299ad668ed4f69c84846c8a8150e78faf4730e6cbe1->leave($__internal_86eedb89a34daea46c8dc299ad668ed4f69c84846c8a8150e78faf4730e6cbe1_prof);

        
        $__internal_518da2556983053da07c79811c08c45b4ba3759eeed107b95849dbd54284e6d5->leave($__internal_518da2556983053da07c79811c08c45b4ba3759eeed107b95849dbd54284e6d5_prof);

    }

    // line 13
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_1801464221c9f9d48f2802e7920234818367916984245a9c1fd9cc35992e40ef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1801464221c9f9d48f2802e7920234818367916984245a9c1fd9cc35992e40ef->enter($__internal_1801464221c9f9d48f2802e7920234818367916984245a9c1fd9cc35992e40ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_32009791a32ed426ca78dc0daf67fe7e520f26b7237c8aa2fb7e86cc1cd65d4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32009791a32ed426ca78dc0daf67fe7e520f26b7237c8aa2fb7e86cc1cd65d4a->enter($__internal_32009791a32ed426ca78dc0daf67fe7e520f26b7237c8aa2fb7e86cc1cd65d4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 14
        if ((((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) != "single_text") &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            // line 15
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 16
            $context["valid"] = true;
        }
        // line 18
        $this->displayParentBlock("datetime_widget", $context, $blocks);
        
        $__internal_32009791a32ed426ca78dc0daf67fe7e520f26b7237c8aa2fb7e86cc1cd65d4a->leave($__internal_32009791a32ed426ca78dc0daf67fe7e520f26b7237c8aa2fb7e86cc1cd65d4a_prof);

        
        $__internal_1801464221c9f9d48f2802e7920234818367916984245a9c1fd9cc35992e40ef->leave($__internal_1801464221c9f9d48f2802e7920234818367916984245a9c1fd9cc35992e40ef_prof);

    }

    // line 21
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_76ce5e763d6f1fd3365884ff7e25b67f0adf105ef18e27f2abfca58ba664bbbd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76ce5e763d6f1fd3365884ff7e25b67f0adf105ef18e27f2abfca58ba664bbbd->enter($__internal_76ce5e763d6f1fd3365884ff7e25b67f0adf105ef18e27f2abfca58ba664bbbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_526f9915864286c1491e6cb322205c4ede42787dfb49b4cabc9aa9b65113a0d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_526f9915864286c1491e6cb322205c4ede42787dfb49b4cabc9aa9b65113a0d3->enter($__internal_526f9915864286c1491e6cb322205c4ede42787dfb49b4cabc9aa9b65113a0d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 22
        if ((((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) != "single_text") &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            // line 23
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 24
            $context["valid"] = true;
        }
        // line 26
        $this->displayParentBlock("date_widget", $context, $blocks);
        
        $__internal_526f9915864286c1491e6cb322205c4ede42787dfb49b4cabc9aa9b65113a0d3->leave($__internal_526f9915864286c1491e6cb322205c4ede42787dfb49b4cabc9aa9b65113a0d3_prof);

        
        $__internal_76ce5e763d6f1fd3365884ff7e25b67f0adf105ef18e27f2abfca58ba664bbbd->leave($__internal_76ce5e763d6f1fd3365884ff7e25b67f0adf105ef18e27f2abfca58ba664bbbd_prof);

    }

    // line 29
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_28054293853b46dc689387a355b6d84036ed6917fa6923cdd3945a834f398549 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28054293853b46dc689387a355b6d84036ed6917fa6923cdd3945a834f398549->enter($__internal_28054293853b46dc689387a355b6d84036ed6917fa6923cdd3945a834f398549_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_38b725f5694b853056b2d4eec36ea5a119e4e8c93c20ecf6f1f1675c3766ce5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38b725f5694b853056b2d4eec36ea5a119e4e8c93c20ecf6f1f1675c3766ce5a->enter($__internal_38b725f5694b853056b2d4eec36ea5a119e4e8c93c20ecf6f1f1675c3766ce5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 30
        if ((((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) != "single_text") &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            // line 31
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 32
            $context["valid"] = true;
        }
        // line 34
        $this->displayParentBlock("time_widget", $context, $blocks);
        
        $__internal_38b725f5694b853056b2d4eec36ea5a119e4e8c93c20ecf6f1f1675c3766ce5a->leave($__internal_38b725f5694b853056b2d4eec36ea5a119e4e8c93c20ecf6f1f1675c3766ce5a_prof);

        
        $__internal_28054293853b46dc689387a355b6d84036ed6917fa6923cdd3945a834f398549->leave($__internal_28054293853b46dc689387a355b6d84036ed6917fa6923cdd3945a834f398549_prof);

    }

    // line 37
    public function block_dateinterval_widget($context, array $blocks = array())
    {
        $__internal_f83b9ce1a6d6ee5c779a349fd3b703970f6bd525972f76daadba4f1c672a76f6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f83b9ce1a6d6ee5c779a349fd3b703970f6bd525972f76daadba4f1c672a76f6->enter($__internal_f83b9ce1a6d6ee5c779a349fd3b703970f6bd525972f76daadba4f1c672a76f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        $__internal_63bffd4cb03f64249d1a25180a63d5d47c34b9217363de5e6ce9209845d70c9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63bffd4cb03f64249d1a25180a63d5d47c34b9217363de5e6ce9209845d70c9f->enter($__internal_63bffd4cb03f64249d1a25180a63d5d47c34b9217363de5e6ce9209845d70c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        // line 38
        if ((((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) != "single_text") &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            // line 39
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 40
            $context["valid"] = true;
        }
        // line 42
        $this->displayParentBlock("dateinterval_widget", $context, $blocks);
        
        $__internal_63bffd4cb03f64249d1a25180a63d5d47c34b9217363de5e6ce9209845d70c9f->leave($__internal_63bffd4cb03f64249d1a25180a63d5d47c34b9217363de5e6ce9209845d70c9f_prof);

        
        $__internal_f83b9ce1a6d6ee5c779a349fd3b703970f6bd525972f76daadba4f1c672a76f6->leave($__internal_f83b9ce1a6d6ee5c779a349fd3b703970f6bd525972f76daadba4f1c672a76f6_prof);

    }

    // line 45
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_1aae30c01c830d70b637781d6e67868b4e35b15d521a7bfec6cfdd0d7ed9ed13 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1aae30c01c830d70b637781d6e67868b4e35b15d521a7bfec6cfdd0d7ed9ed13->enter($__internal_1aae30c01c830d70b637781d6e67868b4e35b15d521a7bfec6cfdd0d7ed9ed13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_4834841083c3e03032cc848b382efcf6933b0072f5ade85da482dd7798d0e862 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4834841083c3e03032cc848b382efcf6933b0072f5ade85da482dd7798d0e862->enter($__internal_4834841083c3e03032cc848b382efcf6933b0072f5ade85da482dd7798d0e862_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 46
        echo "<div class=\"input-group";
        echo (( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) ? (" form-control is-invalid") : (""));
        echo "\">
        ";
        // line 47
        $context["valid"] = true;
        // line 48
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 49
        echo "<span class=\"input-group-addon\">%</span>
    </div>";
        
        $__internal_4834841083c3e03032cc848b382efcf6933b0072f5ade85da482dd7798d0e862->leave($__internal_4834841083c3e03032cc848b382efcf6933b0072f5ade85da482dd7798d0e862_prof);

        
        $__internal_1aae30c01c830d70b637781d6e67868b4e35b15d521a7bfec6cfdd0d7ed9ed13->leave($__internal_1aae30c01c830d70b637781d6e67868b4e35b15d521a7bfec6cfdd0d7ed9ed13_prof);

    }

    // line 53
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_4ae150a3e63ab75805f5e737ab8663f5a3eabd5788ad5e4a50665bbc6b6a39eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ae150a3e63ab75805f5e737ab8663f5a3eabd5788ad5e4a50665bbc6b6a39eb->enter($__internal_4ae150a3e63ab75805f5e737ab8663f5a3eabd5788ad5e4a50665bbc6b6a39eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_e27623cd401c0bfc0e7fbd4ff80a7574b1df7f4d2bfff264a855ece1f5254a0e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e27623cd401c0bfc0e7fbd4ff80a7574b1df7f4d2bfff264a855ece1f5254a0e->enter($__internal_e27623cd401c0bfc0e7fbd4ff80a7574b1df7f4d2bfff264a855ece1f5254a0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 54
        if (( !array_key_exists("type", $context) || ((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")) != "hidden"))) {
            // line 55
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control") . (((((array_key_exists("type", $context)) ? (_twig_default_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "")) : ("")) == "file")) ? ("-file") : (""))))));
        }
        // line 57
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_e27623cd401c0bfc0e7fbd4ff80a7574b1df7f4d2bfff264a855ece1f5254a0e->leave($__internal_e27623cd401c0bfc0e7fbd4ff80a7574b1df7f4d2bfff264a855ece1f5254a0e_prof);

        
        $__internal_4ae150a3e63ab75805f5e737ab8663f5a3eabd5788ad5e4a50665bbc6b6a39eb->leave($__internal_4ae150a3e63ab75805f5e737ab8663f5a3eabd5788ad5e4a50665bbc6b6a39eb_prof);

    }

    // line 60
    public function block_widget_attributes($context, array $blocks = array())
    {
        $__internal_706ee52a2c9493b0953b456be78be4fcda6aa8efb5f273560e81f184284e09c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_706ee52a2c9493b0953b456be78be4fcda6aa8efb5f273560e81f184284e09c9->enter($__internal_706ee52a2c9493b0953b456be78be4fcda6aa8efb5f273560e81f184284e09c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_attributes"));

        $__internal_65738fb5e0650373fb397c5628a7c1c472e77b06757beacf74aede423a30de1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65738fb5e0650373fb397c5628a7c1c472e77b06757beacf74aede423a30de1c->enter($__internal_65738fb5e0650373fb397c5628a7c1c472e77b06757beacf74aede423a30de1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_attributes"));

        // line 61
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            // line 62
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 63
            echo "    ";
        }
        // line 64
        $this->displayParentBlock("widget_attributes", $context, $blocks);
        
        $__internal_65738fb5e0650373fb397c5628a7c1c472e77b06757beacf74aede423a30de1c->leave($__internal_65738fb5e0650373fb397c5628a7c1c472e77b06757beacf74aede423a30de1c_prof);

        
        $__internal_706ee52a2c9493b0953b456be78be4fcda6aa8efb5f273560e81f184284e09c9->leave($__internal_706ee52a2c9493b0953b456be78be4fcda6aa8efb5f273560e81f184284e09c9_prof);

    }

    // line 67
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_aff990fd359e52038bf5a85eccce6f46b483eee22a99a5d42cd2ec42f81d03c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aff990fd359e52038bf5a85eccce6f46b483eee22a99a5d42cd2ec42f81d03c1->enter($__internal_aff990fd359e52038bf5a85eccce6f46b483eee22a99a5d42cd2ec42f81d03c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_1d7e846ee18f5d0f4c200fcac7ed7b060df2cc9c4fce464361f4d51cb4e48c7f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d7e846ee18f5d0f4c200fcac7ed7b060df2cc9c4fce464361f4d51cb4e48c7f->enter($__internal_1d7e846ee18f5d0f4c200fcac7ed7b060df2cc9c4fce464361f4d51cb4e48c7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 68
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "btn-secondary")) : ("btn-secondary")) . " btn"))));
        // line 69
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_1d7e846ee18f5d0f4c200fcac7ed7b060df2cc9c4fce464361f4d51cb4e48c7f->leave($__internal_1d7e846ee18f5d0f4c200fcac7ed7b060df2cc9c4fce464361f4d51cb4e48c7f_prof);

        
        $__internal_aff990fd359e52038bf5a85eccce6f46b483eee22a99a5d42cd2ec42f81d03c1->leave($__internal_aff990fd359e52038bf5a85eccce6f46b483eee22a99a5d42cd2ec42f81d03c1_prof);

    }

    // line 72
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_549cb39a72fd3371c764c56d975fe73bf6546b3e765028191e8c2e43194e4db0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_549cb39a72fd3371c764c56d975fe73bf6546b3e765028191e8c2e43194e4db0->enter($__internal_549cb39a72fd3371c764c56d975fe73bf6546b3e765028191e8c2e43194e4db0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_f75bdfe049741c58c8f42774b057bacd7ee68a8582738e9f7bfa447793cb078a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f75bdfe049741c58c8f42774b057bacd7ee68a8582738e9f7bfa447793cb078a->enter($__internal_f75bdfe049741c58c8f42774b057bacd7ee68a8582738e9f7bfa447793cb078a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 73
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) : ((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
        // line 74
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-check-input"))));
        // line 75
        if (twig_in_filter("checkbox-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 76
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } elseif (twig_in_filter("form-check-inline",         // line 77
(isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 78
            echo "        <div class=\"form-check";
            echo (( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) ? (" form-control is-invalid") : (""));
            echo " form-check-inline\">";
            // line 79
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 80
            echo "</div>
    ";
        } else {
            // line 82
            echo "<div class=\"form-check";
            echo (( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) ? (" form-control is-invalid") : (""));
            echo "\">";
            // line 83
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 84
            echo "</div>";
        }
        
        $__internal_f75bdfe049741c58c8f42774b057bacd7ee68a8582738e9f7bfa447793cb078a->leave($__internal_f75bdfe049741c58c8f42774b057bacd7ee68a8582738e9f7bfa447793cb078a_prof);

        
        $__internal_549cb39a72fd3371c764c56d975fe73bf6546b3e765028191e8c2e43194e4db0->leave($__internal_549cb39a72fd3371c764c56d975fe73bf6546b3e765028191e8c2e43194e4db0_prof);

    }

    // line 88
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_e6a4e0d6d6cef9753bc838fd537fbb36ada73be5f315596e80d9dfc1a6700546 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6a4e0d6d6cef9753bc838fd537fbb36ada73be5f315596e80d9dfc1a6700546->enter($__internal_e6a4e0d6d6cef9753bc838fd537fbb36ada73be5f315596e80d9dfc1a6700546_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_033ca31585d9a14218684705301543de426d872591ffefa24db47944e8c36689 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_033ca31585d9a14218684705301543de426d872591ffefa24db47944e8c36689->enter($__internal_033ca31585d9a14218684705301543de426d872591ffefa24db47944e8c36689_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 89
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) : ((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
        // line 90
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-check-input"))));
        // line 91
        if (twig_in_filter("radio-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 92
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 94
            echo "<div class=\"form-check";
            echo (( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) ? (" form-control is-invalid") : (""));
            echo "\">";
            // line 95
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 96
            echo "</div>";
        }
        
        $__internal_033ca31585d9a14218684705301543de426d872591ffefa24db47944e8c36689->leave($__internal_033ca31585d9a14218684705301543de426d872591ffefa24db47944e8c36689_prof);

        
        $__internal_e6a4e0d6d6cef9753bc838fd537fbb36ada73be5f315596e80d9dfc1a6700546->leave($__internal_e6a4e0d6d6cef9753bc838fd537fbb36ada73be5f315596e80d9dfc1a6700546_prof);

    }

    // line 100
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_3bcb5816060f9911cc5a9292bf993eec5fc9930e9f2bbf1ebe8665b2a531055f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3bcb5816060f9911cc5a9292bf993eec5fc9930e9f2bbf1ebe8665b2a531055f->enter($__internal_3bcb5816060f9911cc5a9292bf993eec5fc9930e9f2bbf1ebe8665b2a531055f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_9eb83183b3987902118646d1e0a511de94646bfdc2797bfc05291da53c190acf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9eb83183b3987902118646d1e0a511de94646bfdc2797bfc05291da53c190acf->enter($__internal_9eb83183b3987902118646d1e0a511de94646bfdc2797bfc05291da53c190acf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 101
        if (twig_in_filter("-inline", (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) {
            // line 102
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 103
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 104
(isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 105
(isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain")), "valid" =>                 // line 106
(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 110
            if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
                // line 111
                $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            }
            // line 113
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 114
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 115
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 116
(isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 117
(isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain")), "valid" => true));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 121
            echo "</div>";
        }
        
        $__internal_9eb83183b3987902118646d1e0a511de94646bfdc2797bfc05291da53c190acf->leave($__internal_9eb83183b3987902118646d1e0a511de94646bfdc2797bfc05291da53c190acf_prof);

        
        $__internal_3bcb5816060f9911cc5a9292bf993eec5fc9930e9f2bbf1ebe8665b2a531055f->leave($__internal_3bcb5816060f9911cc5a9292bf993eec5fc9930e9f2bbf1ebe8665b2a531055f_prof);

    }

    // line 127
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_55620d105ef5a4ad48e44f69d20f9ad70b7de09c6832e0b68575dfc63fbde35a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55620d105ef5a4ad48e44f69d20f9ad70b7de09c6832e0b68575dfc63fbde35a->enter($__internal_55620d105ef5a4ad48e44f69d20f9ad70b7de09c6832e0b68575dfc63fbde35a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_78bb855ccc8a6791d05eab24ea6573b5d2cb855f2c7543848ba500efbb1eb798 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78bb855ccc8a6791d05eab24ea6573b5d2cb855f2c7543848ba500efbb1eb798->enter($__internal_78bb855ccc8a6791d05eab24ea6573b5d2cb855f2c7543848ba500efbb1eb798_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 128
        if ((array_key_exists("compound", $context) && (isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")))) {
            // line 129
            $context["element"] = "legend";
            // line 130
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " col-form-legend"))));
        } else {
            // line 132
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " form-control-label"))));
        }
        // line 134
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_78bb855ccc8a6791d05eab24ea6573b5d2cb855f2c7543848ba500efbb1eb798->leave($__internal_78bb855ccc8a6791d05eab24ea6573b5d2cb855f2c7543848ba500efbb1eb798_prof);

        
        $__internal_55620d105ef5a4ad48e44f69d20f9ad70b7de09c6832e0b68575dfc63fbde35a->leave($__internal_55620d105ef5a4ad48e44f69d20f9ad70b7de09c6832e0b68575dfc63fbde35a_prof);

    }

    // line 137
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_f519cdd0419ac06a5ec6db05edf42dc37383857100af2ebb8a6d896a5d3e9428 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f519cdd0419ac06a5ec6db05edf42dc37383857100af2ebb8a6d896a5d3e9428->enter($__internal_f519cdd0419ac06a5ec6db05edf42dc37383857100af2ebb8a6d896a5d3e9428_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_0edd5eab7bd52e437ce55385bf1d11a094ee8605c85ea0b4695da3a163ee98d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0edd5eab7bd52e437ce55385bf1d11a094ee8605c85ea0b4695da3a163ee98d6->enter($__internal_0edd5eab7bd52e437ce55385bf1d11a094ee8605c85ea0b4695da3a163ee98d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 139
        if (array_key_exists("widget", $context)) {
            // line 140
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " form-check-label"))));
            // line 141
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                // line 142
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
            }
            // line 144
            if (array_key_exists("parent_label_class", $context)) {
                // line 145
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " ") . (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class"))))));
            }
            // line 147
            if (( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false) && twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))))) {
                // line 148
                if ( !twig_test_empty((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")))) {
                    // line 149
                    $context["label"] = twig_replace_filter((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")), array("%name%" =>                     // line 150
(isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "%id%" =>                     // line 151
(isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                } else {
                    // line 154
                    $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                }
            }
            // line 157
            echo "<label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 158
            echo (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) ? (((((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) ? ((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 159
            echo "</label>";
        }
        
        $__internal_0edd5eab7bd52e437ce55385bf1d11a094ee8605c85ea0b4695da3a163ee98d6->leave($__internal_0edd5eab7bd52e437ce55385bf1d11a094ee8605c85ea0b4695da3a163ee98d6_prof);

        
        $__internal_f519cdd0419ac06a5ec6db05edf42dc37383857100af2ebb8a6d896a5d3e9428->leave($__internal_f519cdd0419ac06a5ec6db05edf42dc37383857100af2ebb8a6d896a5d3e9428_prof);

    }

    // line 165
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_fb42471e7225c0dbd8a4b09a1202b28165625d5aab4ba8849174cffc1ab07b58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb42471e7225c0dbd8a4b09a1202b28165625d5aab4ba8849174cffc1ab07b58->enter($__internal_fb42471e7225c0dbd8a4b09a1202b28165625d5aab4ba8849174cffc1ab07b58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_4707b4c8a6099d4ee03aa6c0a43e2a567f4ce1cb1a47f34d055e101dd598530a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4707b4c8a6099d4ee03aa6c0a43e2a567f4ce1cb1a47f34d055e101dd598530a->enter($__internal_4707b4c8a6099d4ee03aa6c0a43e2a567f4ce1cb1a47f34d055e101dd598530a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 166
        if ((array_key_exists("compound", $context) && (isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")))) {
            // line 167
            $context["element"] = "fieldset";
        }
        // line 169
        echo "<";
        echo twig_escape_filter($this->env, ((array_key_exists("element", $context)) ? (_twig_default_filter((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), "div")) : ("div")), "html", null, true);
        echo " class=\"form-group\">";
        // line 170
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        // line 171
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 172
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 173
        echo "</";
        echo twig_escape_filter($this->env, ((array_key_exists("element", $context)) ? (_twig_default_filter((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), "div")) : ("div")), "html", null, true);
        echo ">";
        
        $__internal_4707b4c8a6099d4ee03aa6c0a43e2a567f4ce1cb1a47f34d055e101dd598530a->leave($__internal_4707b4c8a6099d4ee03aa6c0a43e2a567f4ce1cb1a47f34d055e101dd598530a_prof);

        
        $__internal_fb42471e7225c0dbd8a4b09a1202b28165625d5aab4ba8849174cffc1ab07b58->leave($__internal_fb42471e7225c0dbd8a4b09a1202b28165625d5aab4ba8849174cffc1ab07b58_prof);

    }

    // line 178
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_34ec737aeefc40429d1e819bf4b8d2bf4675955abdc1a628b18dbcea8511004a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34ec737aeefc40429d1e819bf4b8d2bf4675955abdc1a628b18dbcea8511004a->enter($__internal_34ec737aeefc40429d1e819bf4b8d2bf4675955abdc1a628b18dbcea8511004a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_acc2b470cd54459e03cea852b2b7e23446b385849eabfa2ead12b28505735d71 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_acc2b470cd54459e03cea852b2b7e23446b385849eabfa2ead12b28505735d71->enter($__internal_acc2b470cd54459e03cea852b2b7e23446b385849eabfa2ead12b28505735d71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 179
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 180
            echo "<div class=\"";
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")))) {
                echo "invalid-feedback";
            } else {
                echo "alert alert-danger";
            }
            echo "\">
        <ul class=\"list-unstyled mb-0\">";
            // line 182
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 183
                echo "<li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 185
            echo "</ul>
    </div>";
        }
        
        $__internal_acc2b470cd54459e03cea852b2b7e23446b385849eabfa2ead12b28505735d71->leave($__internal_acc2b470cd54459e03cea852b2b7e23446b385849eabfa2ead12b28505735d71_prof);

        
        $__internal_34ec737aeefc40429d1e819bf4b8d2bf4675955abdc1a628b18dbcea8511004a->leave($__internal_34ec737aeefc40429d1e819bf4b8d2bf4675955abdc1a628b18dbcea8511004a_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_4_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  683 => 185,  675 => 183,  671 => 182,  662 => 180,  660 => 179,  651 => 178,  639 => 173,  637 => 172,  635 => 171,  633 => 170,  629 => 169,  626 => 167,  624 => 166,  615 => 165,  604 => 159,  600 => 158,  585 => 157,  581 => 154,  578 => 151,  577 => 150,  576 => 149,  574 => 148,  572 => 147,  569 => 145,  567 => 144,  564 => 142,  562 => 141,  560 => 140,  558 => 139,  549 => 137,  539 => 134,  536 => 132,  533 => 130,  531 => 129,  529 => 128,  520 => 127,  509 => 121,  503 => 117,  502 => 116,  501 => 115,  497 => 114,  493 => 113,  490 => 111,  488 => 110,  481 => 106,  480 => 105,  479 => 104,  478 => 103,  474 => 102,  472 => 101,  463 => 100,  452 => 96,  450 => 95,  446 => 94,  443 => 92,  441 => 91,  439 => 90,  437 => 89,  428 => 88,  417 => 84,  415 => 83,  411 => 82,  407 => 80,  405 => 79,  401 => 78,  399 => 77,  397 => 76,  395 => 75,  393 => 74,  391 => 73,  382 => 72,  372 => 69,  370 => 68,  361 => 67,  351 => 64,  348 => 63,  345 => 62,  343 => 61,  334 => 60,  324 => 57,  321 => 55,  319 => 54,  310 => 53,  299 => 49,  297 => 48,  295 => 47,  290 => 46,  281 => 45,  271 => 42,  268 => 40,  266 => 39,  264 => 38,  255 => 37,  245 => 34,  242 => 32,  240 => 31,  238 => 30,  229 => 29,  219 => 26,  216 => 24,  214 => 23,  212 => 22,  203 => 21,  193 => 18,  190 => 16,  188 => 15,  186 => 14,  177 => 13,  167 => 10,  164 => 9,  161 => 8,  158 => 7,  156 => 6,  147 => 5,  137 => 178,  134 => 177,  131 => 175,  129 => 165,  126 => 164,  123 => 162,  121 => 137,  118 => 136,  116 => 127,  113 => 126,  110 => 124,  108 => 100,  105 => 99,  103 => 88,  100 => 87,  98 => 72,  95 => 71,  93 => 67,  91 => 60,  89 => 53,  86 => 52,  84 => 45,  81 => 44,  79 => 37,  76 => 36,  74 => 29,  71 => 28,  69 => 21,  66 => 20,  64 => 13,  61 => 12,  59 => 5,  56 => 4,  53 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_base_layout.html.twig\" %}

{# Widgets #}

{% block money_widget -%}
    {% if not valid %}
        {% set group_class = ' form-control is-invalid' %}
        {% set valid = true %}
    {% endif %}
    {{- parent() -}}
{%- endblock money_widget %}

{% block datetime_widget -%}
    {%- if widget != 'single_text' and not valid -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) -%}
        {% set valid = true %}
    {%- endif -%}
    {{- parent() -}}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {%- if widget != 'single_text' and not valid -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) -%}
        {% set valid = true %}
    {%- endif -%}
    {{- parent() -}}
{%- endblock date_widget %}

{% block time_widget -%}
    {%- if widget != 'single_text' and not valid -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) -%}
        {% set valid = true %}
    {%- endif -%}
    {{- parent() -}}
{%- endblock time_widget %}

{% block dateinterval_widget -%}
    {%- if widget != 'single_text' and not valid -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) -%}
        {% set valid = true %}
    {%- endif -%}
    {{- parent() -}}
{%- endblock dateinterval_widget %}

{% block percent_widget -%}
    <div class=\"input-group{{ not valid ? ' form-control is-invalid' }}\">
        {% set valid = true %}
        {{- block('form_widget_simple') -}}
        <span class=\"input-group-addon\">%</span>
    </div>
{%- endblock percent_widget %}

{% block form_widget_simple -%}
    {% if type is not defined or type != 'hidden' %}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control' ~ (type|default('') == 'file' ? '-file' : ''))|trim}) -%}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{%- block widget_attributes -%}
    {%- if not valid %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) %}
    {% endif -%}
    {{ parent() }}
{%- endblock widget_attributes -%}

{% block button_widget -%}
    {%- set attr = attr|merge({class: (attr.class|default('btn-secondary') ~ ' btn')|trim}) -%}
    {{- parent() -}}
{%- endblock button_widget %}

{% block checkbox_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-check-input')|trim}) -%}
    {% if 'checkbox-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% elseif 'form-check-inline' in parent_label_class %}
        <div class=\"form-check{{ not valid ? ' form-control is-invalid' }} form-check-inline\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {% else -%}
        <div class=\"form-check{{ not valid ? ' form-control is-invalid' }}\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif -%}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-check-input')|trim}) -%}
    {%- if 'radio-inline' in parent_label_class -%}
        {{- form_label(form, null, { widget: parent() }) -}}
    {%- else -%}
        <div class=\"form-check{{ not valid ? ' form-control is-invalid' }}\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif -%}
{%- endblock radio_widget %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') -%}
        {%- for child in form %}
            {{- form_widget(child, {
                parent_label_class: label_attr.class|default(''),
                translation_domain: choice_translation_domain,
                valid: valid,
            }) -}}
        {% endfor -%}
    {%- else -%}
        {%- if not valid -%}
            {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) %}
        {%- endif -%}
        <div {{ block('widget_container_attributes') }}>
            {%- for child in form %}
                {{- form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                    translation_domain: choice_translation_domain,
                    valid: true,
                }) -}}
            {% endfor -%}
        </div>
    {%- endif %}
{%- endblock choice_widget_expanded %}

{# Labels #}

{% block form_label -%}
    {%- if compound is defined and compound -%}
        {%- set element = 'legend' -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' col-form-legend')|trim}) -%}
    {%- else -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' form-control-label')|trim}) -%}
    {%- endif -%}
    {{- parent() -}}
{%- endblock form_label %}

{% block checkbox_radio_label -%}
    {#- Do not display the label if widget is not defined in order to prevent double label rendering -#}
    {%- if widget is defined -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' form-check-label')|trim}) -%}
        {%- if required -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) -%}
        {%- endif -%}
        {%- if parent_label_class is defined -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) -%}
        {%- endif -%}
        {%- if label is not same as(false) and label is empty -%}
            {%- if label_format is not empty -%}
                {%- set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) -%}
            {%- else -%}
                {%- set label = name|humanize -%}
            {%- endif -%}
        {%- endif -%}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}
        </label>
    {%- endif -%}
{%- endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    {%- if compound is defined and compound -%}
        {%- set element = 'fieldset' -%}
    {%- endif -%}
    <{{ element|default('div') }} class=\"form-group\">
        {{- form_label(form) -}}
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </{{ element|default('div') }}>
{%- endblock form_row %}

{# Errors #}

{% block form_errors -%}
    {%- if errors|length > 0 -%}
    <div class=\"{% if form is not rootform %}invalid-feedback{% else %}alert alert-danger{% endif %}\">
        <ul class=\"list-unstyled mb-0\">
            {%- for error in errors -%}
                <li>{{ error.message }}</li>
            {%- endfor -%}
        </ul>
    </div>
    {%- endif %}
{%- endblock form_errors %}
", "bootstrap_4_layout.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_4_layout.html.twig");
    }
}
