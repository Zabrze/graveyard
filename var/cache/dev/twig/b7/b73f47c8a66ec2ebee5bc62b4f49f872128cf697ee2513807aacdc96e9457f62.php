<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_c90c635dc75b7be14e501a18a88de31e1da2920e2104a84ee79128cd18df06ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81c7b159b9e1a74ac1b56b3c84f06ed80b48f5b266bffcf9f8ec755471976ee5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81c7b159b9e1a74ac1b56b3c84f06ed80b48f5b266bffcf9f8ec755471976ee5->enter($__internal_81c7b159b9e1a74ac1b56b3c84f06ed80b48f5b266bffcf9f8ec755471976ee5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_3edd53b2c5fab9c291d9f28252f1f344812854ab0eec9c580a624e894d5d1a4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3edd53b2c5fab9c291d9f28252f1f344812854ab0eec9c580a624e894d5d1a4d->enter($__internal_3edd53b2c5fab9c291d9f28252f1f344812854ab0eec9c580a624e894d5d1a4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_81c7b159b9e1a74ac1b56b3c84f06ed80b48f5b266bffcf9f8ec755471976ee5->leave($__internal_81c7b159b9e1a74ac1b56b3c84f06ed80b48f5b266bffcf9f8ec755471976ee5_prof);

        
        $__internal_3edd53b2c5fab9c291d9f28252f1f344812854ab0eec9c580a624e894d5d1a4d->leave($__internal_3edd53b2c5fab9c291d9f28252f1f344812854ab0eec9c580a624e894d5d1a4d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_fc1ce79cf3e3a1796180af5f0ebc1f6362b6254dca873ab46eeaa44b1fea8863 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc1ce79cf3e3a1796180af5f0ebc1f6362b6254dca873ab46eeaa44b1fea8863->enter($__internal_fc1ce79cf3e3a1796180af5f0ebc1f6362b6254dca873ab46eeaa44b1fea8863_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_8a7ed46b68823db853c572dc360035605bddce805ac5a25cb2a26a9c505828dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a7ed46b68823db853c572dc360035605bddce805ac5a25cb2a26a9c505828dd->enter($__internal_8a7ed46b68823db853c572dc360035605bddce805ac5a25cb2a26a9c505828dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_8a7ed46b68823db853c572dc360035605bddce805ac5a25cb2a26a9c505828dd->leave($__internal_8a7ed46b68823db853c572dc360035605bddce805ac5a25cb2a26a9c505828dd_prof);

        
        $__internal_fc1ce79cf3e3a1796180af5f0ebc1f6362b6254dca873ab46eeaa44b1fea8863->leave($__internal_fc1ce79cf3e3a1796180af5f0ebc1f6362b6254dca873ab46eeaa44b1fea8863_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f54ac800fffa8e3404fbdfc3605e8d890482a07d0f218035736aaa207f76486f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f54ac800fffa8e3404fbdfc3605e8d890482a07d0f218035736aaa207f76486f->enter($__internal_f54ac800fffa8e3404fbdfc3605e8d890482a07d0f218035736aaa207f76486f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_6697e5e2967199012e0222f30ebd129de5a3c9994ce9a61eb70f896d888004f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6697e5e2967199012e0222f30ebd129de5a3c9994ce9a61eb70f896d888004f0->enter($__internal_6697e5e2967199012e0222f30ebd129de5a3c9994ce9a61eb70f896d888004f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_6697e5e2967199012e0222f30ebd129de5a3c9994ce9a61eb70f896d888004f0->leave($__internal_6697e5e2967199012e0222f30ebd129de5a3c9994ce9a61eb70f896d888004f0_prof);

        
        $__internal_f54ac800fffa8e3404fbdfc3605e8d890482a07d0f218035736aaa207f76486f->leave($__internal_f54ac800fffa8e3404fbdfc3605e8d890482a07d0f218035736aaa207f76486f_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5a2091987acfe8f86cae631abe8f9fcc9b207148c28734a1507ed467b9cab6af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a2091987acfe8f86cae631abe8f9fcc9b207148c28734a1507ed467b9cab6af->enter($__internal_5a2091987acfe8f86cae631abe8f9fcc9b207148c28734a1507ed467b9cab6af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_6b80d31ddaa932d399fe8ea9d58e50b95b7f81fcd03fbf4c223179cbeeb62d5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b80d31ddaa932d399fe8ea9d58e50b95b7f81fcd03fbf4c223179cbeeb62d5c->enter($__internal_6b80d31ddaa932d399fe8ea9d58e50b95b7f81fcd03fbf4c223179cbeeb62d5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_6b80d31ddaa932d399fe8ea9d58e50b95b7f81fcd03fbf4c223179cbeeb62d5c->leave($__internal_6b80d31ddaa932d399fe8ea9d58e50b95b7f81fcd03fbf4c223179cbeeb62d5c_prof);

        
        $__internal_5a2091987acfe8f86cae631abe8f9fcc9b207148c28734a1507ed467b9cab6af->leave($__internal_5a2091987acfe8f86cae631abe8f9fcc9b207148c28734a1507ed467b9cab6af_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
