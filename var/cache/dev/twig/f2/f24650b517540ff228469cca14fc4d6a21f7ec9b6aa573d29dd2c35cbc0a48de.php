<?php

/* default/addDead.html.twig */
class __TwigTemplate_7ba9d9c931d207afdddbd70e65ead0eb5405e017331dccbbe481ca2f0f8cf507 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6b8f2857e0c3b3cc3ac9cfd961c067b5ef6bed0b9f4acba4ada32d8d34d02b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6b8f2857e0c3b3cc3ac9cfd961c067b5ef6bed0b9f4acba4ada32d8d34d02b8->enter($__internal_e6b8f2857e0c3b3cc3ac9cfd961c067b5ef6bed0b9f4acba4ada32d8d34d02b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/addDead.html.twig"));

        $__internal_27c4abb8aa58c6b309c3676d51b3f424460043426aa3cf7931086b61af2cc9ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_27c4abb8aa58c6b309c3676d51b3f424460043426aa3cf7931086b61af2cc9ee->enter($__internal_27c4abb8aa58c6b309c3676d51b3f424460043426aa3cf7931086b61af2cc9ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/addDead.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "default/addDead.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_e6b8f2857e0c3b3cc3ac9cfd961c067b5ef6bed0b9f4acba4ada32d8d34d02b8->leave($__internal_e6b8f2857e0c3b3cc3ac9cfd961c067b5ef6bed0b9f4acba4ada32d8d34d02b8_prof);

        
        $__internal_27c4abb8aa58c6b309c3676d51b3f424460043426aa3cf7931086b61af2cc9ee->leave($__internal_27c4abb8aa58c6b309c3676d51b3f424460043426aa3cf7931086b61af2cc9ee_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_73d31f3f61ac930a9eb7f50690f914caff88c94d810ac0ce83ca487f9fae43dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73d31f3f61ac930a9eb7f50690f914caff88c94d810ac0ce83ca487f9fae43dc->enter($__internal_73d31f3f61ac930a9eb7f50690f914caff88c94d810ac0ce83ca487f9fae43dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c8a4a02a9044536dc9dcc4abbef7e46de62cf2b8d0439fead403644711a2fe8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8a4a02a9044536dc9dcc4abbef7e46de62cf2b8d0439fead403644711a2fe8f->enter($__internal_c8a4a02a9044536dc9dcc4abbef7e46de62cf2b8d0439fead403644711a2fe8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo " <div class=\"form-horizontal\" >

";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "flashes", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 8
            echo "    
        ";
            // line 9
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
   
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
";
        // line 15
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

 </div>
";
        
        $__internal_c8a4a02a9044536dc9dcc4abbef7e46de62cf2b8d0439fead403644711a2fe8f->leave($__internal_c8a4a02a9044536dc9dcc4abbef7e46de62cf2b8d0439fead403644711a2fe8f_prof);

        
        $__internal_73d31f3f61ac930a9eb7f50690f914caff88c94d810ac0ce83ca487f9fae43dc->leave($__internal_73d31f3f61ac930a9eb7f50690f914caff88c94d810ac0ce83ca487f9fae43dc_prof);

    }

    public function getTemplateName()
    {
        return "default/addDead.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 15,  73 => 14,  69 => 13,  60 => 9,  57 => 8,  53 => 7,  49 => 4,  31 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'base.html.twig' %}

{% block body %}
 <div class=\"form-horizontal\" >

{#{% if app.flashes('notice') %}#}
{% for message in app.flashes('notice') %}
    
        {{ message }}
   
{% endfor %}
{#{% endif %}#}
{{ form_start(form) }}
{{ form_widget(form) }}
{{ form_end(form) }}

 </div>
{% endblock %}", "default/addDead.html.twig", "C:\\projects\\graveyard\\app\\Resources\\views\\default\\addDead.html.twig");
    }
}
