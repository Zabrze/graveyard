<?php

/* @Twig/images/icon-support.svg */
class __TwigTemplate_45b7a3aab32dbbdb294a8827f1deeddcf5b47c3c8b0b0326eba8d991cbb4c4ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0fbeba7dc4d63ff3e2e22e6ea0892aac269ffde87a50fd28ad0bb40fa655e404 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0fbeba7dc4d63ff3e2e22e6ea0892aac269ffde87a50fd28ad0bb40fa655e404->enter($__internal_0fbeba7dc4d63ff3e2e22e6ea0892aac269ffde87a50fd28ad0bb40fa655e404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-support.svg"));

        $__internal_36c7927b042ccbff5a4f2256fe100212a7ef496ff620057f1aae4a9049830d44 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36c7927b042ccbff5a4f2256fe100212a7ef496ff620057f1aae4a9049830d44->enter($__internal_36c7927b042ccbff5a4f2256fe100212a7ef496ff620057f1aae4a9049830d44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-support.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M896 0q182 0 348 71t286 191 191 286 71 348-71 348-191 286-286 191-348 71-348-71-286-191-191-286T0 896t71-348 191-286T548 71 896 0zm0 128q-190 0-361 90l194 194q82-28 167-28t167 28l194-194q-171-90-361-90zM218 1257l194-194q-28-82-28-167t28-167L218 535q-90 171-90 361t90 361zm678 407q190 0 361-90l-194-194q-82 28-167 28t-167-28l-194 194q171 90 361 90zm0-384q159 0 271.5-112.5T1280 896t-112.5-271.5T896 512 624.5 624.5 512 896t112.5 271.5T896 1280zm484-217l194 194q90-171 90-361t-90-361l-194 194q28 82 28 167t-28 167z\"/></svg>
";
        
        $__internal_0fbeba7dc4d63ff3e2e22e6ea0892aac269ffde87a50fd28ad0bb40fa655e404->leave($__internal_0fbeba7dc4d63ff3e2e22e6ea0892aac269ffde87a50fd28ad0bb40fa655e404_prof);

        
        $__internal_36c7927b042ccbff5a4f2256fe100212a7ef496ff620057f1aae4a9049830d44->leave($__internal_36c7927b042ccbff5a4f2256fe100212a7ef496ff620057f1aae4a9049830d44_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-support.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M896 0q182 0 348 71t286 191 191 286 71 348-71 348-191 286-286 191-348 71-348-71-286-191-191-286T0 896t71-348 191-286T548 71 896 0zm0 128q-190 0-361 90l194 194q82-28 167-28t167 28l194-194q-171-90-361-90zM218 1257l194-194q-28-82-28-167t28-167L218 535q-90 171-90 361t90 361zm678 407q190 0 361-90l-194-194q-82 28-167 28t-167-28l-194 194q171 90 361 90zm0-384q159 0 271.5-112.5T1280 896t-112.5-271.5T896 512 624.5 624.5 512 896t112.5 271.5T896 1280zm484-217l194 194q90-171 90-361t-90-361l-194 194q28 82 28 167t-28 167z\"/></svg>
", "@Twig/images/icon-support.svg", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\icon-support.svg");
    }
}
