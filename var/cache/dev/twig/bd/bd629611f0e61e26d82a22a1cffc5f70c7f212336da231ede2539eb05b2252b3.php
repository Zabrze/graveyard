<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_8dec9c0b97fb699b82dce6b9533d4a69599b781c87f13690cfdb39277b72e8ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26e283813e908ea0ee1b0d1940a1f9149f2c5cdc66f5ff0ceedeb1bff0e9f12a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26e283813e908ea0ee1b0d1940a1f9149f2c5cdc66f5ff0ceedeb1bff0e9f12a->enter($__internal_26e283813e908ea0ee1b0d1940a1f9149f2c5cdc66f5ff0ceedeb1bff0e9f12a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_f0760b2d81c65735fee8534b0d9a0f37a60b4b78f9a4d1523240f245004070e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0760b2d81c65735fee8534b0d9a0f37a60b4b78f9a4d1523240f245004070e0->enter($__internal_f0760b2d81c65735fee8534b0d9a0f37a60b4b78f9a4d1523240f245004070e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_26e283813e908ea0ee1b0d1940a1f9149f2c5cdc66f5ff0ceedeb1bff0e9f12a->leave($__internal_26e283813e908ea0ee1b0d1940a1f9149f2c5cdc66f5ff0ceedeb1bff0e9f12a_prof);

        
        $__internal_f0760b2d81c65735fee8534b0d9a0f37a60b4b78f9a4d1523240f245004070e0->leave($__internal_f0760b2d81c65735fee8534b0d9a0f37a60b4b78f9a4d1523240f245004070e0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\password_widget.html.php");
    }
}
