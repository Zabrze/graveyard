<?php

/* default/index.html.twig */
class __TwigTemplate_d7dee15485acb05b9f925a934870563bf9ba0d9b30d2d8d20e261fd8177704a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e48d86e6f38738994c607026349680428fe1737574ad37015b611bc920973b88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e48d86e6f38738994c607026349680428fe1737574ad37015b611bc920973b88->enter($__internal_e48d86e6f38738994c607026349680428fe1737574ad37015b611bc920973b88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_befad474ec001e7e6733959c681aadaffe192f4b5bb81ce6e47049584eba92ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_befad474ec001e7e6733959c681aadaffe192f4b5bb81ce6e47049584eba92ea->enter($__internal_befad474ec001e7e6733959c681aadaffe192f4b5bb81ce6e47049584eba92ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "default/index.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_e48d86e6f38738994c607026349680428fe1737574ad37015b611bc920973b88->leave($__internal_e48d86e6f38738994c607026349680428fe1737574ad37015b611bc920973b88_prof);

        
        $__internal_befad474ec001e7e6733959c681aadaffe192f4b5bb81ce6e47049584eba92ea->leave($__internal_befad474ec001e7e6733959c681aadaffe192f4b5bb81ce6e47049584eba92ea_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_ac0e9d34894223e92e89485905e785fc5e282f4c04e4792177747aba3503b1fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac0e9d34894223e92e89485905e785fc5e282f4c04e4792177747aba3503b1fe->enter($__internal_ac0e9d34894223e92e89485905e785fc5e282f4c04e4792177747aba3503b1fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_dd85f540439c92b4b7a98a15f9410e1713d0c0b66e32d38c9515ca2d52755bff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd85f540439c92b4b7a98a15f9410e1713d0c0b66e32d38c9515ca2d52755bff->enter($__internal_dd85f540439c92b4b7a98a15f9410e1713d0c0b66e32d38c9515ca2d52755bff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1 style=\"text-align: center; color: blueviolet\">Witaj na stronie cmentarza!</h1>
";
        
        $__internal_dd85f540439c92b4b7a98a15f9410e1713d0c0b66e32d38c9515ca2d52755bff->leave($__internal_dd85f540439c92b4b7a98a15f9410e1713d0c0b66e32d38c9515ca2d52755bff_prof);

        
        $__internal_ac0e9d34894223e92e89485905e785fc5e282f4c04e4792177747aba3503b1fe->leave($__internal_ac0e9d34894223e92e89485905e785fc5e282f4c04e4792177747aba3503b1fe_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  31 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'base.html.twig' %}

{% block body %}
    <h1 style=\"text-align: center; color: blueviolet\">Witaj na stronie cmentarza!</h1>
{% endblock %}
", "default/index.html.twig", "C:\\projects\\graveyard\\app\\Resources\\views\\default\\index.html.twig");
    }
}
