<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_8b42721c6d63495c3ed30c44c0ebfeb47d1976a128a198b05586dfed7d3f3f4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_855f8435761aae6a5fb038648f516ceb185810e9c6a596e3d5741f507a46e0ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_855f8435761aae6a5fb038648f516ceb185810e9c6a596e3d5741f507a46e0ee->enter($__internal_855f8435761aae6a5fb038648f516ceb185810e9c6a596e3d5741f507a46e0ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        $__internal_2301ec7cb4082fc6a608e84b02df641067e49b69432be0b5c8ee01ef59c05b97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2301ec7cb4082fc6a608e84b02df641067e49b69432be0b5c8ee01ef59c05b97->enter($__internal_2301ec7cb4082fc6a608e84b02df641067e49b69432be0b5c8ee01ef59c05b97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_855f8435761aae6a5fb038648f516ceb185810e9c6a596e3d5741f507a46e0ee->leave($__internal_855f8435761aae6a5fb038648f516ceb185810e9c6a596e3d5741f507a46e0ee_prof);

        
        $__internal_2301ec7cb4082fc6a608e84b02df641067e49b69432be0b5c8ee01ef59c05b97->leave($__internal_2301ec7cb4082fc6a608e84b02df641067e49b69432be0b5c8ee01ef59c05b97_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "@Twig/Exception/exception.atom.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.atom.twig");
    }
}
