<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_ad3fa144b1bf2eb634e7bba0fe3de7bc5b7f81b32c15336c2bd01fda66383bff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_05da5d2da9f1a5859b10c5445335f467aeb7f18e59d5d95187454c1621071c3e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05da5d2da9f1a5859b10c5445335f467aeb7f18e59d5d95187454c1621071c3e->enter($__internal_05da5d2da9f1a5859b10c5445335f467aeb7f18e59d5d95187454c1621071c3e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_3e129122d84e5460c947406598d59e81930c7eb75cb854f967273b0ed1144211 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e129122d84e5460c947406598d59e81930c7eb75cb854f967273b0ed1144211->enter($__internal_3e129122d84e5460c947406598d59e81930c7eb75cb854f967273b0ed1144211_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_05da5d2da9f1a5859b10c5445335f467aeb7f18e59d5d95187454c1621071c3e->leave($__internal_05da5d2da9f1a5859b10c5445335f467aeb7f18e59d5d95187454c1621071c3e_prof);

        
        $__internal_3e129122d84e5460c947406598d59e81930c7eb75cb854f967273b0ed1144211->leave($__internal_3e129122d84e5460c947406598d59e81930c7eb75cb854f967273b0ed1144211_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\container_attributes.html.php");
    }
}
