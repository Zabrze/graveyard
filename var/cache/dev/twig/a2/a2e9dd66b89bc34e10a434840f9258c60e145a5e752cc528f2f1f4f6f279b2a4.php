<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_ded63a06244dac38f4caf2e3298ac4a45c5293af9901d6cd75d5abceec7ce429 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e514efb90b46e96bf7e8392dc5bf380e660fa9f9725cd4fa9bd6a0b9ba250271 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e514efb90b46e96bf7e8392dc5bf380e660fa9f9725cd4fa9bd6a0b9ba250271->enter($__internal_e514efb90b46e96bf7e8392dc5bf380e660fa9f9725cd4fa9bd6a0b9ba250271_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_9a415a90cf86f47945b89429cdb75a210652d2bba01f84f820bfa34113c5d703 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a415a90cf86f47945b89429cdb75a210652d2bba01f84f820bfa34113c5d703->enter($__internal_9a415a90cf86f47945b89429cdb75a210652d2bba01f84f820bfa34113c5d703_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_e514efb90b46e96bf7e8392dc5bf380e660fa9f9725cd4fa9bd6a0b9ba250271->leave($__internal_e514efb90b46e96bf7e8392dc5bf380e660fa9f9725cd4fa9bd6a0b9ba250271_prof);

        
        $__internal_9a415a90cf86f47945b89429cdb75a210652d2bba01f84f820bfa34113c5d703->leave($__internal_9a415a90cf86f47945b89429cdb75a210652d2bba01f84f820bfa34113c5d703_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_options.html.php");
    }
}
