<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_a0654b23622c230108bc66893b5ef160399ded2c68b732315054e9936a0f07fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_613082a31f94500229b1e4e9945ba89ff729f9ad6c1b08421a60e400dd39172d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_613082a31f94500229b1e4e9945ba89ff729f9ad6c1b08421a60e400dd39172d->enter($__internal_613082a31f94500229b1e4e9945ba89ff729f9ad6c1b08421a60e400dd39172d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_8c1ff9985ff4cc342d48c1ed09f4afca63c1bf203b775e603cb20f13c9ad5f80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c1ff9985ff4cc342d48c1ed09f4afca63c1bf203b775e603cb20f13c9ad5f80->enter($__internal_8c1ff9985ff4cc342d48c1ed09f4afca63c1bf203b775e603cb20f13c9ad5f80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_613082a31f94500229b1e4e9945ba89ff729f9ad6c1b08421a60e400dd39172d->leave($__internal_613082a31f94500229b1e4e9945ba89ff729f9ad6c1b08421a60e400dd39172d_prof);

        
        $__internal_8c1ff9985ff4cc342d48c1ed09f4afca63c1bf203b775e603cb20f13c9ad5f80->leave($__internal_8c1ff9985ff4cc342d48c1ed09f4afca63c1bf203b775e603cb20f13c9ad5f80_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_errors.html.php");
    }
}
