<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e2d81854e0f2b4224b6fa5b73e4718848477fea0e157b9426de63eb5e51e4dae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1de8f7987290bb414ff3ffe6de704c697f75f742467ca26d73e518cf9c362fc2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1de8f7987290bb414ff3ffe6de704c697f75f742467ca26d73e518cf9c362fc2->enter($__internal_1de8f7987290bb414ff3ffe6de704c697f75f742467ca26d73e518cf9c362fc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_240b6920d9e7afa0b14d4c48cf9c4e44497f5cfb9767c03f1cde65465e0bbbc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_240b6920d9e7afa0b14d4c48cf9c4e44497f5cfb9767c03f1cde65465e0bbbc8->enter($__internal_240b6920d9e7afa0b14d4c48cf9c4e44497f5cfb9767c03f1cde65465e0bbbc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1de8f7987290bb414ff3ffe6de704c697f75f742467ca26d73e518cf9c362fc2->leave($__internal_1de8f7987290bb414ff3ffe6de704c697f75f742467ca26d73e518cf9c362fc2_prof);

        
        $__internal_240b6920d9e7afa0b14d4c48cf9c4e44497f5cfb9767c03f1cde65465e0bbbc8->leave($__internal_240b6920d9e7afa0b14d4c48cf9c4e44497f5cfb9767c03f1cde65465e0bbbc8_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2f0336ce322920dda5a17ec725ea3f5ffb93c9b2639e7e7c720e97da38c8e0c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f0336ce322920dda5a17ec725ea3f5ffb93c9b2639e7e7c720e97da38c8e0c8->enter($__internal_2f0336ce322920dda5a17ec725ea3f5ffb93c9b2639e7e7c720e97da38c8e0c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ee321df88f2376aae25c70f76ee3ff5da5517b17012707fb4082c566ff641039 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee321df88f2376aae25c70f76ee3ff5da5517b17012707fb4082c566ff641039->enter($__internal_ee321df88f2376aae25c70f76ee3ff5da5517b17012707fb4082c566ff641039_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_ee321df88f2376aae25c70f76ee3ff5da5517b17012707fb4082c566ff641039->leave($__internal_ee321df88f2376aae25c70f76ee3ff5da5517b17012707fb4082c566ff641039_prof);

        
        $__internal_2f0336ce322920dda5a17ec725ea3f5ffb93c9b2639e7e7c720e97da38c8e0c8->leave($__internal_2f0336ce322920dda5a17ec725ea3f5ffb93c9b2639e7e7c720e97da38c8e0c8_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_2193287f865e871a0e7b3c932f2b047f6de4817fcd38a06aeb70680eb6dbeb2d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2193287f865e871a0e7b3c932f2b047f6de4817fcd38a06aeb70680eb6dbeb2d->enter($__internal_2193287f865e871a0e7b3c932f2b047f6de4817fcd38a06aeb70680eb6dbeb2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6b69599be93d9fd3896e070e7389869ce14665e8608d36cd8254a78fc1de47f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b69599be93d9fd3896e070e7389869ce14665e8608d36cd8254a78fc1de47f4->enter($__internal_6b69599be93d9fd3896e070e7389869ce14665e8608d36cd8254a78fc1de47f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_6b69599be93d9fd3896e070e7389869ce14665e8608d36cd8254a78fc1de47f4->leave($__internal_6b69599be93d9fd3896e070e7389869ce14665e8608d36cd8254a78fc1de47f4_prof);

        
        $__internal_2193287f865e871a0e7b3c932f2b047f6de4817fcd38a06aeb70680eb6dbeb2d->leave($__internal_2193287f865e871a0e7b3c932f2b047f6de4817fcd38a06aeb70680eb6dbeb2d_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_130e9bcb10b2a9377b043dbb49d878472759b9d52176eca5187f0dd513c01140 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_130e9bcb10b2a9377b043dbb49d878472759b9d52176eca5187f0dd513c01140->enter($__internal_130e9bcb10b2a9377b043dbb49d878472759b9d52176eca5187f0dd513c01140_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_162e628f4ab9a346be87e78e53ca85c15cf680ff1713ec0c01f5a7c7217a1288 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_162e628f4ab9a346be87e78e53ca85c15cf680ff1713ec0c01f5a7c7217a1288->enter($__internal_162e628f4ab9a346be87e78e53ca85c15cf680ff1713ec0c01f5a7c7217a1288_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_162e628f4ab9a346be87e78e53ca85c15cf680ff1713ec0c01f5a7c7217a1288->leave($__internal_162e628f4ab9a346be87e78e53ca85c15cf680ff1713ec0c01f5a7c7217a1288_prof);

        
        $__internal_130e9bcb10b2a9377b043dbb49d878472759b9d52176eca5187f0dd513c01140->leave($__internal_130e9bcb10b2a9377b043dbb49d878472759b9d52176eca5187f0dd513c01140_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
