<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_b791489f7246a91c5f3a98e978aafcea03a375a68e0334a67d88d6f0ee5e8936 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c77520d7c8c23e6c1fc745b106c61c1e5232c35daf95b2381396b8d072bdde6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c77520d7c8c23e6c1fc745b106c61c1e5232c35daf95b2381396b8d072bdde6->enter($__internal_2c77520d7c8c23e6c1fc745b106c61c1e5232c35daf95b2381396b8d072bdde6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        $__internal_bddc065fe5ca8ff3249f21476c91c809f2a0071e1eed211f1dcd50ec5b3d81e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bddc065fe5ca8ff3249f21476c91c809f2a0071e1eed211f1dcd50ec5b3d81e2->enter($__internal_bddc065fe5ca8ff3249f21476c91c809f2a0071e1eed211f1dcd50ec5b3d81e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_2c77520d7c8c23e6c1fc745b106c61c1e5232c35daf95b2381396b8d072bdde6->leave($__internal_2c77520d7c8c23e6c1fc745b106c61c1e5232c35daf95b2381396b8d072bdde6_prof);

        
        $__internal_bddc065fe5ca8ff3249f21476c91c809f2a0071e1eed211f1dcd50ec5b3d81e2->leave($__internal_bddc065fe5ca8ff3249f21476c91c809f2a0071e1eed211f1dcd50ec5b3d81e2_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "@Twig/Exception/error.js.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.js.twig");
    }
}
