<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_d8bbf82a64c75f31028b970d0e833c49f0bb3447e90cbbdf8e7b2f9ce14e40e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a082c13e1be26f3b14c77f9efb31c6e7a81182073d080e86bb4c57ca14d2408c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a082c13e1be26f3b14c77f9efb31c6e7a81182073d080e86bb4c57ca14d2408c->enter($__internal_a082c13e1be26f3b14c77f9efb31c6e7a81182073d080e86bb4c57ca14d2408c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_4a51b0ce3e552b63e3e1363d5bb4bff8b1f13dffb08f2a3e0c905f262fb907f3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a51b0ce3e552b63e3e1363d5bb4bff8b1f13dffb08f2a3e0c905f262fb907f3->enter($__internal_4a51b0ce3e552b63e3e1363d5bb4bff8b1f13dffb08f2a3e0c905f262fb907f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_a082c13e1be26f3b14c77f9efb31c6e7a81182073d080e86bb4c57ca14d2408c->leave($__internal_a082c13e1be26f3b14c77f9efb31c6e7a81182073d080e86bb4c57ca14d2408c_prof);

        
        $__internal_4a51b0ce3e552b63e3e1363d5bb4bff8b1f13dffb08f2a3e0c905f262fb907f3->leave($__internal_4a51b0ce3e552b63e3e1363d5bb4bff8b1f13dffb08f2a3e0c905f262fb907f3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_row.html.php");
    }
}
