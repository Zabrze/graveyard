<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_a45ee52b07c68062bb7c1d25b901ce3826b0aedfe8d86f29e84a4567016cd32e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80bd78b3261024cd94efb49bec0f68c5cd38530852f3b2f1adf3889ed95c1578 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80bd78b3261024cd94efb49bec0f68c5cd38530852f3b2f1adf3889ed95c1578->enter($__internal_80bd78b3261024cd94efb49bec0f68c5cd38530852f3b2f1adf3889ed95c1578_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_f092872025e197d33f49844b31881b10044b1f87ff79dd1cb5e00790c603141a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f092872025e197d33f49844b31881b10044b1f87ff79dd1cb5e00790c603141a->enter($__internal_f092872025e197d33f49844b31881b10044b1f87ff79dd1cb5e00790c603141a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_80bd78b3261024cd94efb49bec0f68c5cd38530852f3b2f1adf3889ed95c1578->leave($__internal_80bd78b3261024cd94efb49bec0f68c5cd38530852f3b2f1adf3889ed95c1578_prof);

        
        $__internal_f092872025e197d33f49844b31881b10044b1f87ff79dd1cb5e00790c603141a->leave($__internal_f092872025e197d33f49844b31881b10044b1f87ff79dd1cb5e00790c603141a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rest.html.php");
    }
}
