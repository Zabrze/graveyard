<?php

/* @FOSUser/Registration/check_email.html.twig */
class __TwigTemplate_2a09d7848e102269770ff1b57c867b212badcb80d631ef949c2f9f044a09ef63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09811bc4bf5ec1aa1af8199698f155d0c510e758873e6278bd23757e69adfca2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09811bc4bf5ec1aa1af8199698f155d0c510e758873e6278bd23757e69adfca2->enter($__internal_09811bc4bf5ec1aa1af8199698f155d0c510e758873e6278bd23757e69adfca2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        $__internal_a56e6fe8e26fe206939ad9ded998d1e2f81baa62c97d9ba6cfd54c72dcb5159d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a56e6fe8e26fe206939ad9ded998d1e2f81baa62c97d9ba6cfd54c72dcb5159d->enter($__internal_a56e6fe8e26fe206939ad9ded998d1e2f81baa62c97d9ba6cfd54c72dcb5159d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_09811bc4bf5ec1aa1af8199698f155d0c510e758873e6278bd23757e69adfca2->leave($__internal_09811bc4bf5ec1aa1af8199698f155d0c510e758873e6278bd23757e69adfca2_prof);

        
        $__internal_a56e6fe8e26fe206939ad9ded998d1e2f81baa62c97d9ba6cfd54c72dcb5159d->leave($__internal_a56e6fe8e26fe206939ad9ded998d1e2f81baa62c97d9ba6cfd54c72dcb5159d_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4829a1cb86b3835ef991e9e27644dd26829a150d5ef63dfbc4bbb61a1f0e017a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4829a1cb86b3835ef991e9e27644dd26829a150d5ef63dfbc4bbb61a1f0e017a->enter($__internal_4829a1cb86b3835ef991e9e27644dd26829a150d5ef63dfbc4bbb61a1f0e017a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_5d43035dfe6874748c1c09636bbaceb06ae833ee8774c9a9931aceec141ecd1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d43035dfe6874748c1c09636bbaceb06ae833ee8774c9a9931aceec141ecd1f->enter($__internal_5d43035dfe6874748c1c09636bbaceb06ae833ee8774c9a9931aceec141ecd1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_5d43035dfe6874748c1c09636bbaceb06ae833ee8774c9a9931aceec141ecd1f->leave($__internal_5d43035dfe6874748c1c09636bbaceb06ae833ee8774c9a9931aceec141ecd1f_prof);

        
        $__internal_4829a1cb86b3835ef991e9e27644dd26829a150d5ef63dfbc4bbb61a1f0e017a->leave($__internal_4829a1cb86b3835ef991e9e27644dd26829a150d5ef63dfbc4bbb61a1f0e017a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "@FOSUser/Registration/check_email.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\check_email.html.twig");
    }
}
