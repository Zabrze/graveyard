<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_ce5e298ea7c566ba912d0cb4d5695f690b63e95c6189996a45de6c3ac1516709 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8ed2b2d7ee77ec3e0c176e067691971be29ed4718afabe0caefb9687a702e91e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ed2b2d7ee77ec3e0c176e067691971be29ed4718afabe0caefb9687a702e91e->enter($__internal_8ed2b2d7ee77ec3e0c176e067691971be29ed4718afabe0caefb9687a702e91e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_ae6b9ff422c3f224d0dfd1c4a0105d8882d605ce92307570a43533146e1c863f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae6b9ff422c3f224d0dfd1c4a0105d8882d605ce92307570a43533146e1c863f->enter($__internal_ae6b9ff422c3f224d0dfd1c4a0105d8882d605ce92307570a43533146e1c863f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8ed2b2d7ee77ec3e0c176e067691971be29ed4718afabe0caefb9687a702e91e->leave($__internal_8ed2b2d7ee77ec3e0c176e067691971be29ed4718afabe0caefb9687a702e91e_prof);

        
        $__internal_ae6b9ff422c3f224d0dfd1c4a0105d8882d605ce92307570a43533146e1c863f->leave($__internal_ae6b9ff422c3f224d0dfd1c4a0105d8882d605ce92307570a43533146e1c863f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6edbfb4f99ad45d6691098b0b6a3e2cce2ce48c1c33fe1baeaeeaac89802ce7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6edbfb4f99ad45d6691098b0b6a3e2cce2ce48c1c33fe1baeaeeaac89802ce7e->enter($__internal_6edbfb4f99ad45d6691098b0b6a3e2cce2ce48c1c33fe1baeaeeaac89802ce7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2707bf3b540041d8637ce5609b1956f2cd7e9a0d0d34ebeec41c9600567f5bbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2707bf3b540041d8637ce5609b1956f2cd7e9a0d0d34ebeec41c9600567f5bbf->enter($__internal_2707bf3b540041d8637ce5609b1956f2cd7e9a0d0d34ebeec41c9600567f5bbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_2707bf3b540041d8637ce5609b1956f2cd7e9a0d0d34ebeec41c9600567f5bbf->leave($__internal_2707bf3b540041d8637ce5609b1956f2cd7e9a0d0d34ebeec41c9600567f5bbf_prof);

        
        $__internal_6edbfb4f99ad45d6691098b0b6a3e2cce2ce48c1c33fe1baeaeeaac89802ce7e->leave($__internal_6edbfb4f99ad45d6691098b0b6a3e2cce2ce48c1c33fe1baeaeeaac89802ce7e_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_b4245c8b0a6c2bfb8b4b17baf753bb27078d230d7f6fe67ab6b7379a56d9efd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4245c8b0a6c2bfb8b4b17baf753bb27078d230d7f6fe67ab6b7379a56d9efd7->enter($__internal_b4245c8b0a6c2bfb8b4b17baf753bb27078d230d7f6fe67ab6b7379a56d9efd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_cf84849d96ba054667da01f5401f59df5d35f7f75c1ef7dc1144d589ad58df6e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf84849d96ba054667da01f5401f59df5d35f7f75c1ef7dc1144d589ad58df6e->enter($__internal_cf84849d96ba054667da01f5401f59df5d35f7f75c1ef7dc1144d589ad58df6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_cf84849d96ba054667da01f5401f59df5d35f7f75c1ef7dc1144d589ad58df6e->leave($__internal_cf84849d96ba054667da01f5401f59df5d35f7f75c1ef7dc1144d589ad58df6e_prof);

        
        $__internal_b4245c8b0a6c2bfb8b4b17baf753bb27078d230d7f6fe67ab6b7379a56d9efd7->leave($__internal_b4245c8b0a6c2bfb8b4b17baf753bb27078d230d7f6fe67ab6b7379a56d9efd7_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_8f25abb410db61c6a0012dddb14dd8e8b622b9a3e31cb42f2f85e8cad64391bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f25abb410db61c6a0012dddb14dd8e8b622b9a3e31cb42f2f85e8cad64391bd->enter($__internal_8f25abb410db61c6a0012dddb14dd8e8b622b9a3e31cb42f2f85e8cad64391bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_a9f4f70e08d0bff1826bc6e1c3449fff91bddd24fd2a68a2ed9344eec7286975 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9f4f70e08d0bff1826bc6e1c3449fff91bddd24fd2a68a2ed9344eec7286975->enter($__internal_a9f4f70e08d0bff1826bc6e1c3449fff91bddd24fd2a68a2ed9344eec7286975_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_a9f4f70e08d0bff1826bc6e1c3449fff91bddd24fd2a68a2ed9344eec7286975->leave($__internal_a9f4f70e08d0bff1826bc6e1c3449fff91bddd24fd2a68a2ed9344eec7286975_prof);

        
        $__internal_8f25abb410db61c6a0012dddb14dd8e8b622b9a3e31cb42f2f85e8cad64391bd->leave($__internal_8f25abb410db61c6a0012dddb14dd8e8b622b9a3e31cb42f2f85e8cad64391bd_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
