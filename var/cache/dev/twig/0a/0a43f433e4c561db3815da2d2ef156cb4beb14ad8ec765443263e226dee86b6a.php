<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_1150906dbea88cad3c73edad53f454f977691b3cf296008d3800b990fb93a975 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e47246a3a6fbb4850d7a670f6f701db8f6e8daadc1d0f2ac44ebac0a01550dbd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e47246a3a6fbb4850d7a670f6f701db8f6e8daadc1d0f2ac44ebac0a01550dbd->enter($__internal_e47246a3a6fbb4850d7a670f6f701db8f6e8daadc1d0f2ac44ebac0a01550dbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_b1a281c63758f5f109ce9149875c2a3b03e74eeb3ec06eadfa47b226ddbffa62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1a281c63758f5f109ce9149875c2a3b03e74eeb3ec06eadfa47b226ddbffa62->enter($__internal_b1a281c63758f5f109ce9149875c2a3b03e74eeb3ec06eadfa47b226ddbffa62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_e47246a3a6fbb4850d7a670f6f701db8f6e8daadc1d0f2ac44ebac0a01550dbd->leave($__internal_e47246a3a6fbb4850d7a670f6f701db8f6e8daadc1d0f2ac44ebac0a01550dbd_prof);

        
        $__internal_b1a281c63758f5f109ce9149875c2a3b03e74eeb3ec06eadfa47b226ddbffa62->leave($__internal_b1a281c63758f5f109ce9149875c2a3b03e74eeb3ec06eadfa47b226ddbffa62_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_row.html.php");
    }
}
