<?php

/* default/addCurator.html.twig */
class __TwigTemplate_014c5946f60345768acacb60211f93d06959d7cd53f587e86863360bdaf60899 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b37fdd69dfd6c49f9235ca14bd13be54dc5a23a85b803c2a8c57fba3fc8f8f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b37fdd69dfd6c49f9235ca14bd13be54dc5a23a85b803c2a8c57fba3fc8f8f0->enter($__internal_5b37fdd69dfd6c49f9235ca14bd13be54dc5a23a85b803c2a8c57fba3fc8f8f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/addCurator.html.twig"));

        $__internal_d3068946421d65029616c3a75df1d7be4c89817d8c9a708d1e6dc689ed151004 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3068946421d65029616c3a75df1d7be4c89817d8c9a708d1e6dc689ed151004->enter($__internal_d3068946421d65029616c3a75df1d7be4c89817d8c9a708d1e6dc689ed151004_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/addCurator.html.twig"));

        // line 1
        $this->loadTemplate("base.html.twig", "default/addCurator.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 3
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_5b37fdd69dfd6c49f9235ca14bd13be54dc5a23a85b803c2a8c57fba3fc8f8f0->leave($__internal_5b37fdd69dfd6c49f9235ca14bd13be54dc5a23a85b803c2a8c57fba3fc8f8f0_prof);

        
        $__internal_d3068946421d65029616c3a75df1d7be4c89817d8c9a708d1e6dc689ed151004->leave($__internal_d3068946421d65029616c3a75df1d7be4c89817d8c9a708d1e6dc689ed151004_prof);

    }

    public function block_body($context, array $blocks = array())
    {
        $__internal_cde7d39ae52ad8600766927eaa2e93bb8e89a18825301450aa9fabd8fffb793a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cde7d39ae52ad8600766927eaa2e93bb8e89a18825301450aa9fabd8fffb793a->enter($__internal_cde7d39ae52ad8600766927eaa2e93bb8e89a18825301450aa9fabd8fffb793a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d1e9be334512c0630f5d0542b36c8b59c6b50c26d230d9647baaf8d5d87631ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1e9be334512c0630f5d0542b36c8b59c6b50c26d230d9647baaf8d5d87631ed->enter($__internal_d1e9be334512c0630f5d0542b36c8b59c6b50c26d230d9647baaf8d5d87631ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <div class=\"form-horizontal\" >
        ";
        // line 7
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "flashes", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 8
            echo "            ";
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "        ";
        // line 11
        echo "
        ";
        // line 12
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    </div>

";
        
        $__internal_d1e9be334512c0630f5d0542b36c8b59c6b50c26d230d9647baaf8d5d87631ed->leave($__internal_d1e9be334512c0630f5d0542b36c8b59c6b50c26d230d9647baaf8d5d87631ed_prof);

        
        $__internal_cde7d39ae52ad8600766927eaa2e93bb8e89a18825301450aa9fabd8fffb793a->leave($__internal_cde7d39ae52ad8600766927eaa2e93bb8e89a18825301450aa9fabd8fffb793a_prof);

    }

    public function getTemplateName()
    {
        return "default/addCurator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 14,  76 => 13,  72 => 12,  69 => 11,  67 => 10,  58 => 8,  53 => 7,  49 => 4,  31 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include 'base.html.twig' %}

{% block body %}

    <div class=\"form-horizontal\" >
        {#{% if app.flashes('notice') %}#}
        {% for message in app.flashes('notice') %}
            {{ message }}
        {% endfor %}
        {#{% endif %}#}

        {{ form_start(form) }}
        {{ form_widget(form) }}
        {{ form_end(form) }}

    </div>

{% endblock %}", "default/addCurator.html.twig", "C:\\projects\\graveyard\\app\\Resources\\views\\default\\addCurator.html.twig");
    }
}
