<?php

/* @FOSUser/Resetting/check_email.html.twig */
class __TwigTemplate_ceb5d99ece68a82a83a739112504d5aeb4b85194add5cab95a05f5333556678d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7782c6a8fec3cf5bb9bd67bbef771e250f5a7180a37e08a78bf11365d8b9797c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7782c6a8fec3cf5bb9bd67bbef771e250f5a7180a37e08a78bf11365d8b9797c->enter($__internal_7782c6a8fec3cf5bb9bd67bbef771e250f5a7180a37e08a78bf11365d8b9797c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $__internal_b74b36ae6ed966d51711e57a76cb359a7d263909d1476d072c3e4a4e5a7ed3e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b74b36ae6ed966d51711e57a76cb359a7d263909d1476d072c3e4a4e5a7ed3e0->enter($__internal_b74b36ae6ed966d51711e57a76cb359a7d263909d1476d072c3e4a4e5a7ed3e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7782c6a8fec3cf5bb9bd67bbef771e250f5a7180a37e08a78bf11365d8b9797c->leave($__internal_7782c6a8fec3cf5bb9bd67bbef771e250f5a7180a37e08a78bf11365d8b9797c_prof);

        
        $__internal_b74b36ae6ed966d51711e57a76cb359a7d263909d1476d072c3e4a4e5a7ed3e0->leave($__internal_b74b36ae6ed966d51711e57a76cb359a7d263909d1476d072c3e4a4e5a7ed3e0_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_94b32d89c5a98199c3d0c42f9d8298ca8308f1990e97ba348bcae43eb2410d7d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_94b32d89c5a98199c3d0c42f9d8298ca8308f1990e97ba348bcae43eb2410d7d->enter($__internal_94b32d89c5a98199c3d0c42f9d8298ca8308f1990e97ba348bcae43eb2410d7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_dffab9cfb8f3f2331d833766a0ca55717c4fa1a818756f633ac6337a306aae24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dffab9cfb8f3f2331d833766a0ca55717c4fa1a818756f633ac6337a306aae24->enter($__internal_dffab9cfb8f3f2331d833766a0ca55717c4fa1a818756f633ac6337a306aae24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) ? $context["tokenLifetime"] : $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_dffab9cfb8f3f2331d833766a0ca55717c4fa1a818756f633ac6337a306aae24->leave($__internal_dffab9cfb8f3f2331d833766a0ca55717c4fa1a818756f633ac6337a306aae24_prof);

        
        $__internal_94b32d89c5a98199c3d0c42f9d8298ca8308f1990e97ba348bcae43eb2410d7d->leave($__internal_94b32d89c5a98199c3d0c42f9d8298ca8308f1990e97ba348bcae43eb2410d7d_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "@FOSUser/Resetting/check_email.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\check_email.html.twig");
    }
}
