<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_9c89f10cb6bf51da95570ca3bd60a41f7ad1521ff9b1c997966bb939d5b6447e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a5e84a54c7e448796b508869d1132f4e447348ac65c66bc39a3b67273bc42ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a5e84a54c7e448796b508869d1132f4e447348ac65c66bc39a3b67273bc42ac->enter($__internal_4a5e84a54c7e448796b508869d1132f4e447348ac65c66bc39a3b67273bc42ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        $__internal_990eeaa9fff3adbd1693acf78af8158f12543b4db339064fb1be5767871340f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_990eeaa9fff3adbd1693acf78af8158f12543b4db339064fb1be5767871340f1->enter($__internal_990eeaa9fff3adbd1693acf78af8158f12543b4db339064fb1be5767871340f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_4a5e84a54c7e448796b508869d1132f4e447348ac65c66bc39a3b67273bc42ac->leave($__internal_4a5e84a54c7e448796b508869d1132f4e447348ac65c66bc39a3b67273bc42ac_prof);

        
        $__internal_990eeaa9fff3adbd1693acf78af8158f12543b4db339064fb1be5767871340f1->leave($__internal_990eeaa9fff3adbd1693acf78af8158f12543b4db339064fb1be5767871340f1_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_ec1960165ba9cd1fe1c6dc056bb3cfd04b5e399b5c8898cd77a70d9862700e63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec1960165ba9cd1fe1c6dc056bb3cfd04b5e399b5c8898cd77a70d9862700e63->enter($__internal_ec1960165ba9cd1fe1c6dc056bb3cfd04b5e399b5c8898cd77a70d9862700e63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c1f7b11cd1d5b3052773ff24b0511fba563bd3463c1ac1bb831e2173d3c47106 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1f7b11cd1d5b3052773ff24b0511fba563bd3463c1ac1bb831e2173d3c47106->enter($__internal_c1f7b11cd1d5b3052773ff24b0511fba563bd3463c1ac1bb831e2173d3c47106_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_c1f7b11cd1d5b3052773ff24b0511fba563bd3463c1ac1bb831e2173d3c47106->leave($__internal_c1f7b11cd1d5b3052773ff24b0511fba563bd3463c1ac1bb831e2173d3c47106_prof);

        
        $__internal_ec1960165ba9cd1fe1c6dc056bb3cfd04b5e399b5c8898cd77a70d9862700e63->leave($__internal_ec1960165ba9cd1fe1c6dc056bb3cfd04b5e399b5c8898cd77a70d9862700e63_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "@WebProfiler/Profiler/ajax_layout.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\ajax_layout.html.twig");
    }
}
