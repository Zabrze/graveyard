<?php

/* @FOSUser/Resetting/request.html.twig */
class __TwigTemplate_39378a91bc76bf2218218abfde964cb335d601ac4effa8db10d5fa86c85d3f18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c64d8375c964b74bb726ef27b09cc681c745ce1f7789e1e7828955281c6ba18e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c64d8375c964b74bb726ef27b09cc681c745ce1f7789e1e7828955281c6ba18e->enter($__internal_c64d8375c964b74bb726ef27b09cc681c745ce1f7789e1e7828955281c6ba18e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $__internal_b4887ccb3a4440f9666937f422e6f37c4660bb41e1dcd492855b150ea96fe676 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4887ccb3a4440f9666937f422e6f37c4660bb41e1dcd492855b150ea96fe676->enter($__internal_b4887ccb3a4440f9666937f422e6f37c4660bb41e1dcd492855b150ea96fe676_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c64d8375c964b74bb726ef27b09cc681c745ce1f7789e1e7828955281c6ba18e->leave($__internal_c64d8375c964b74bb726ef27b09cc681c745ce1f7789e1e7828955281c6ba18e_prof);

        
        $__internal_b4887ccb3a4440f9666937f422e6f37c4660bb41e1dcd492855b150ea96fe676->leave($__internal_b4887ccb3a4440f9666937f422e6f37c4660bb41e1dcd492855b150ea96fe676_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5f74b0d9e780f966cebb4ace8625e41a2b44d50ab6347d786dd939d07970f625 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f74b0d9e780f966cebb4ace8625e41a2b44d50ab6347d786dd939d07970f625->enter($__internal_5f74b0d9e780f966cebb4ace8625e41a2b44d50ab6347d786dd939d07970f625_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_2514135bd43a30cacb914f0d93ed4e929dff75dbfa937d0fe414e0b1382bad25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2514135bd43a30cacb914f0d93ed4e929dff75dbfa937d0fe414e0b1382bad25->enter($__internal_2514135bd43a30cacb914f0d93ed4e929dff75dbfa937d0fe414e0b1382bad25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "@FOSUser/Resetting/request.html.twig", 4)->display($context);
        
        $__internal_2514135bd43a30cacb914f0d93ed4e929dff75dbfa937d0fe414e0b1382bad25->leave($__internal_2514135bd43a30cacb914f0d93ed4e929dff75dbfa937d0fe414e0b1382bad25_prof);

        
        $__internal_5f74b0d9e780f966cebb4ace8625e41a2b44d50ab6347d786dd939d07970f625->leave($__internal_5f74b0d9e780f966cebb4ace8625e41a2b44d50ab6347d786dd939d07970f625_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Resetting/request_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Resetting/request.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\request.html.twig");
    }
}
