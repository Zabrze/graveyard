<?php

/* foundation_5_layout.html.twig */
class __TwigTemplate_0b43bc2f70c3d7d549a460a79aa9aeb6b9215ff127425cf85d9174c9d075d778 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("form_div_layout.html.twig", "foundation_5_layout.html.twig", 1);
        $this->blocks = array(
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'button_widget' => array($this, 'block_button_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'radio_widget' => array($this, 'block_radio_widget'),
            'form_label' => array($this, 'block_form_label'),
            'choice_label' => array($this, 'block_choice_label'),
            'checkbox_label' => array($this, 'block_checkbox_label'),
            'radio_label' => array($this, 'block_radio_label'),
            'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
            'form_row' => array($this, 'block_form_row'),
            'choice_row' => array($this, 'block_choice_row'),
            'date_row' => array($this, 'block_date_row'),
            'time_row' => array($this, 'block_time_row'),
            'datetime_row' => array($this, 'block_datetime_row'),
            'checkbox_row' => array($this, 'block_checkbox_row'),
            'radio_row' => array($this, 'block_radio_row'),
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b24938502d7c54acc5b2670eff71b09d831e26f45eb6959363574820d11708aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b24938502d7c54acc5b2670eff71b09d831e26f45eb6959363574820d11708aa->enter($__internal_b24938502d7c54acc5b2670eff71b09d831e26f45eb6959363574820d11708aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "foundation_5_layout.html.twig"));

        $__internal_f1b0a54cecbfc3ca617465b2c22e00ac38104c01b2525cdcd26429c35667d6b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1b0a54cecbfc3ca617465b2c22e00ac38104c01b2525cdcd26429c35667d6b8->enter($__internal_f1b0a54cecbfc3ca617465b2c22e00ac38104c01b2525cdcd26429c35667d6b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "foundation_5_layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b24938502d7c54acc5b2670eff71b09d831e26f45eb6959363574820d11708aa->leave($__internal_b24938502d7c54acc5b2670eff71b09d831e26f45eb6959363574820d11708aa_prof);

        
        $__internal_f1b0a54cecbfc3ca617465b2c22e00ac38104c01b2525cdcd26429c35667d6b8->leave($__internal_f1b0a54cecbfc3ca617465b2c22e00ac38104c01b2525cdcd26429c35667d6b8_prof);

    }

    // line 6
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_44d9b19b0e33b5998bdc98611d3e3e6766096b7793961846bb3042368fca317e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44d9b19b0e33b5998bdc98611d3e3e6766096b7793961846bb3042368fca317e->enter($__internal_44d9b19b0e33b5998bdc98611d3e3e6766096b7793961846bb3042368fca317e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_600c9fe981f0fed8a76b52e26524ce5e7cdb4145085bb0c4aabc6c67e6109d42 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_600c9fe981f0fed8a76b52e26524ce5e7cdb4145085bb0c4aabc6c67e6109d42->enter($__internal_600c9fe981f0fed8a76b52e26524ce5e7cdb4145085bb0c4aabc6c67e6109d42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 7
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 8
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " error"))));
            // line 9
            echo "    ";
        }
        // line 10
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_600c9fe981f0fed8a76b52e26524ce5e7cdb4145085bb0c4aabc6c67e6109d42->leave($__internal_600c9fe981f0fed8a76b52e26524ce5e7cdb4145085bb0c4aabc6c67e6109d42_prof);

        
        $__internal_44d9b19b0e33b5998bdc98611d3e3e6766096b7793961846bb3042368fca317e->leave($__internal_44d9b19b0e33b5998bdc98611d3e3e6766096b7793961846bb3042368fca317e_prof);

    }

    // line 13
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_ac38d5282ed713767d4fb4a9452cfb6844ec6cd4eca9950ebbb154623f8d6a2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac38d5282ed713767d4fb4a9452cfb6844ec6cd4eca9950ebbb154623f8d6a2e->enter($__internal_ac38d5282ed713767d4fb4a9452cfb6844ec6cd4eca9950ebbb154623f8d6a2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_86d6d822ff80689796dda4843cfb7a6f4d05284b2b570cf97fa4b35ef602c9bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86d6d822ff80689796dda4843cfb7a6f4d05284b2b570cf97fa4b35ef602c9bd->enter($__internal_86d6d822ff80689796dda4843cfb7a6f4d05284b2b570cf97fa4b35ef602c9bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 14
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 15
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " error"))));
            // line 16
            echo "    ";
        }
        // line 17
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_86d6d822ff80689796dda4843cfb7a6f4d05284b2b570cf97fa4b35ef602c9bd->leave($__internal_86d6d822ff80689796dda4843cfb7a6f4d05284b2b570cf97fa4b35ef602c9bd_prof);

        
        $__internal_ac38d5282ed713767d4fb4a9452cfb6844ec6cd4eca9950ebbb154623f8d6a2e->leave($__internal_ac38d5282ed713767d4fb4a9452cfb6844ec6cd4eca9950ebbb154623f8d6a2e_prof);

    }

    // line 20
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_3d14dfaa66a5fb3d2dfbd75939242cb3d1fa26afa97c61156881bde19117638d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3d14dfaa66a5fb3d2dfbd75939242cb3d1fa26afa97c61156881bde19117638d->enter($__internal_3d14dfaa66a5fb3d2dfbd75939242cb3d1fa26afa97c61156881bde19117638d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_a8d174be9683fd8926dd1841341b78a8862d09647fe438243de8143099c013a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8d174be9683fd8926dd1841341b78a8862d09647fe438243de8143099c013a5->enter($__internal_a8d174be9683fd8926dd1841341b78a8862d09647fe438243de8143099c013a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 21
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " button"))));
        // line 22
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_a8d174be9683fd8926dd1841341b78a8862d09647fe438243de8143099c013a5->leave($__internal_a8d174be9683fd8926dd1841341b78a8862d09647fe438243de8143099c013a5_prof);

        
        $__internal_3d14dfaa66a5fb3d2dfbd75939242cb3d1fa26afa97c61156881bde19117638d->leave($__internal_3d14dfaa66a5fb3d2dfbd75939242cb3d1fa26afa97c61156881bde19117638d_prof);

    }

    // line 25
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_8fd23e673f6f3efeb51b5d84fe1429e90553a45e427a0ab1070425b85e25d573 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8fd23e673f6f3efeb51b5d84fe1429e90553a45e427a0ab1070425b85e25d573->enter($__internal_8fd23e673f6f3efeb51b5d84fe1429e90553a45e427a0ab1070425b85e25d573_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_f6f47d72fd3ed8a62c4a4e8a3666315d4dfeb3656e5489d0ddfccdaf24b60c15 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6f47d72fd3ed8a62c4a4e8a3666315d4dfeb3656e5489d0ddfccdaf24b60c15->enter($__internal_f6f47d72fd3ed8a62c4a4e8a3666315d4dfeb3656e5489d0ddfccdaf24b60c15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 26
        echo "<div class=\"row collapse\">
        ";
        // line 27
        $context["prepend"] = ("{{" == twig_slice($this->env, (isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), 0, 2));
        // line 28
        echo "        ";
        if ( !(isset($context["prepend"]) ? $context["prepend"] : $this->getContext($context, "prepend"))) {
            // line 29
            echo "            <div class=\"small-3 large-2 columns\">
                <span class=\"prefix\">";
            // line 30
            echo twig_escape_filter($this->env, twig_replace_filter((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
            </div>
        ";
        }
        // line 33
        echo "        <div class=\"small-9 large-10 columns\">";
        // line 34
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 35
        echo "</div>
        ";
        // line 36
        if ((isset($context["prepend"]) ? $context["prepend"] : $this->getContext($context, "prepend"))) {
            // line 37
            echo "            <div class=\"small-3 large-2 columns\">
                <span class=\"postfix\">";
            // line 38
            echo twig_escape_filter($this->env, twig_replace_filter((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
            </div>
        ";
        }
        // line 41
        echo "    </div>";
        
        $__internal_f6f47d72fd3ed8a62c4a4e8a3666315d4dfeb3656e5489d0ddfccdaf24b60c15->leave($__internal_f6f47d72fd3ed8a62c4a4e8a3666315d4dfeb3656e5489d0ddfccdaf24b60c15_prof);

        
        $__internal_8fd23e673f6f3efeb51b5d84fe1429e90553a45e427a0ab1070425b85e25d573->leave($__internal_8fd23e673f6f3efeb51b5d84fe1429e90553a45e427a0ab1070425b85e25d573_prof);

    }

    // line 44
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_4265d2d0c8459069912cd1add9e398673cec156e6a85dabf37845a757e46d708 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4265d2d0c8459069912cd1add9e398673cec156e6a85dabf37845a757e46d708->enter($__internal_4265d2d0c8459069912cd1add9e398673cec156e6a85dabf37845a757e46d708_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_b3a10f30169e4b3fe1c05bf853668658cbdcab09fce2276b34ae25ef70c69a93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3a10f30169e4b3fe1c05bf853668658cbdcab09fce2276b34ae25ef70c69a93->enter($__internal_b3a10f30169e4b3fe1c05bf853668658cbdcab09fce2276b34ae25ef70c69a93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 45
        echo "<div class=\"row collapse\">
        <div class=\"small-9 large-10 columns\">";
        // line 47
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 48
        echo "</div>
        <div class=\"small-3 large-2 columns\">
            <span class=\"postfix\">%</span>
        </div>
    </div>";
        
        $__internal_b3a10f30169e4b3fe1c05bf853668658cbdcab09fce2276b34ae25ef70c69a93->leave($__internal_b3a10f30169e4b3fe1c05bf853668658cbdcab09fce2276b34ae25ef70c69a93_prof);

        
        $__internal_4265d2d0c8459069912cd1add9e398673cec156e6a85dabf37845a757e46d708->leave($__internal_4265d2d0c8459069912cd1add9e398673cec156e6a85dabf37845a757e46d708_prof);

    }

    // line 55
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_8ce9dd25d5d13c9fd1e26f503fa65de731cd8b0fab46fb96b77fdacc0afbd80d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ce9dd25d5d13c9fd1e26f503fa65de731cd8b0fab46fb96b77fdacc0afbd80d->enter($__internal_8ce9dd25d5d13c9fd1e26f503fa65de731cd8b0fab46fb96b77fdacc0afbd80d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_bda50b64f91a4b16b2d2880383c2f1675ce619af661facd5ef51ccfeffa26889 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bda50b64f91a4b16b2d2880383c2f1675ce619af661facd5ef51ccfeffa26889->enter($__internal_bda50b64f91a4b16b2d2880383c2f1675ce619af661facd5ef51ccfeffa26889_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 56
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 57
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 59
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " row"))));
            // line 60
            echo "        <div class=\"row\">
            <div class=\"large-7 columns\">";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'errors');
            echo "</div>
            <div class=\"large-5 columns\">";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'errors');
            echo "</div>
        </div>
        <div ";
            // line 64
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            <div class=\"large-7 columns\">";
            // line 65
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            echo "</div>
            <div class=\"large-5 columns\">";
            // line 66
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            echo "</div>
        </div>
    ";
        }
        
        $__internal_bda50b64f91a4b16b2d2880383c2f1675ce619af661facd5ef51ccfeffa26889->leave($__internal_bda50b64f91a4b16b2d2880383c2f1675ce619af661facd5ef51ccfeffa26889_prof);

        
        $__internal_8ce9dd25d5d13c9fd1e26f503fa65de731cd8b0fab46fb96b77fdacc0afbd80d->leave($__internal_8ce9dd25d5d13c9fd1e26f503fa65de731cd8b0fab46fb96b77fdacc0afbd80d_prof);

    }

    // line 71
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_65283395ab601116ee1d21f12fc88817b468e506b7f0a60aa71510f90b20f963 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_65283395ab601116ee1d21f12fc88817b468e506b7f0a60aa71510f90b20f963->enter($__internal_65283395ab601116ee1d21f12fc88817b468e506b7f0a60aa71510f90b20f963_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_03c65e1f4bff909f5269e6edf4b8f0fcaa2fd8f92672c6098e6a98a2dd9ee19c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03c65e1f4bff909f5269e6edf4b8f0fcaa2fd8f92672c6098e6a98a2dd9ee19c->enter($__internal_03c65e1f4bff909f5269e6edf4b8f0fcaa2fd8f92672c6098e6a98a2dd9ee19c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 72
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 73
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 75
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " row"))));
            // line 76
            echo "        ";
            if (( !array_key_exists("datetime", $context) ||  !(isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime")))) {
                // line 77
                echo "            <div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">
        ";
            }
            // line 79
            echo twig_replace_filter((isset($context["date_pattern"]) ? $context["date_pattern"] : $this->getContext($context, "date_pattern")), array("{{ year }}" => (("<div class=\"large-4 columns\">" .             // line 80
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "year", array()), 'widget')) . "</div>"), "{{ month }}" => (("<div class=\"large-4 columns\">" .             // line 81
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "month", array()), 'widget')) . "</div>"), "{{ day }}" => (("<div class=\"large-4 columns\">" .             // line 82
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "day", array()), 'widget')) . "</div>")));
            // line 84
            if (( !array_key_exists("datetime", $context) ||  !(isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime")))) {
                // line 85
                echo "            </div>
        ";
            }
            // line 87
            echo "    ";
        }
        
        $__internal_03c65e1f4bff909f5269e6edf4b8f0fcaa2fd8f92672c6098e6a98a2dd9ee19c->leave($__internal_03c65e1f4bff909f5269e6edf4b8f0fcaa2fd8f92672c6098e6a98a2dd9ee19c_prof);

        
        $__internal_65283395ab601116ee1d21f12fc88817b468e506b7f0a60aa71510f90b20f963->leave($__internal_65283395ab601116ee1d21f12fc88817b468e506b7f0a60aa71510f90b20f963_prof);

    }

    // line 90
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_bacae0d7c1dd894e751468f24463a6b6964726af893442a7a30ec194ad385ccd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bacae0d7c1dd894e751468f24463a6b6964726af893442a7a30ec194ad385ccd->enter($__internal_bacae0d7c1dd894e751468f24463a6b6964726af893442a7a30ec194ad385ccd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_e96ce1931cd2a87217a29e8cc47752701c2d6e1da0e57f985e0cddbb37079d87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e96ce1931cd2a87217a29e8cc47752701c2d6e1da0e57f985e0cddbb37079d87->enter($__internal_e96ce1931cd2a87217a29e8cc47752701c2d6e1da0e57f985e0cddbb37079d87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 91
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 92
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 94
            echo "        ";
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " row"))));
            // line 95
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == (isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime"))))) {
                // line 96
                echo "            <div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">
        ";
            }
            // line 98
            echo "        ";
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : $this->getContext($context, "with_seconds"))) {
                // line 99
                echo "            <div class=\"large-4 columns\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hour", array()), 'widget');
                echo "</div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 106
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minute", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 116
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
        ";
            } else {
                // line 121
                echo "            <div class=\"large-6 columns\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hour", array()), 'widget');
                echo "</div>
            <div class=\"large-6 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 128
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minute", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
        ";
            }
            // line 133
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == (isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime"))))) {
                // line 134
                echo "            </div>
        ";
            }
            // line 136
            echo "    ";
        }
        
        $__internal_e96ce1931cd2a87217a29e8cc47752701c2d6e1da0e57f985e0cddbb37079d87->leave($__internal_e96ce1931cd2a87217a29e8cc47752701c2d6e1da0e57f985e0cddbb37079d87_prof);

        
        $__internal_bacae0d7c1dd894e751468f24463a6b6964726af893442a7a30ec194ad385ccd->leave($__internal_bacae0d7c1dd894e751468f24463a6b6964726af893442a7a30ec194ad385ccd_prof);

    }

    // line 139
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_796bba90925d2d7dbd35811b46f618332e0b38e24d97661d99d220829398b946 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_796bba90925d2d7dbd35811b46f618332e0b38e24d97661d99d220829398b946->enter($__internal_796bba90925d2d7dbd35811b46f618332e0b38e24d97661d99d220829398b946_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_4b7bc90c4e6360733b2ffcafd331928912cec982e988c5bcccc64ef9cf969067 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b7bc90c4e6360733b2ffcafd331928912cec982e988c5bcccc64ef9cf969067->enter($__internal_4b7bc90c4e6360733b2ffcafd331928912cec982e988c5bcccc64ef9cf969067_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 140
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 141
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " error"))));
            // line 142
            echo "    ";
        }
        // line 143
        echo "
    ";
        // line 144
        if ((isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple"))) {
            // line 145
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("style" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "style", array()), "")) : ("")) . " height: auto; background-image: none;"))));
            // line 146
            echo "    ";
        }
        // line 147
        echo "
    ";
        // line 148
        if (((((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required")) && (null === (isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder")))) &&  !(isset($context["placeholder_in_choices"]) ? $context["placeholder_in_choices"] : $this->getContext($context, "placeholder_in_choices"))) &&  !(isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple")))) {
            // line 149
            $context["required"] = false;
        }
        // line 151
        echo "<select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ((isset($context["multiple"]) ? $context["multiple"] : $this->getContext($context, "multiple"))) {
            echo " multiple=\"multiple\" data-customforms=\"disabled\"";
        }
        echo ">
        ";
        // line 152
        if ( !(null === (isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder")))) {
            // line 153
            echo "<option value=\"\"";
            if (((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required")) && twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, ((((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) ? ((isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["placeholder"]) ? $context["placeholder"] : $this->getContext($context, "placeholder")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))))), "html", null, true);
            echo "</option>";
        }
        // line 155
        if ((twig_length_filter($this->env, (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"))) > 0)) {
            // line 156
            $context["options"] = (isset($context["preferred_choices"]) ? $context["preferred_choices"] : $this->getContext($context, "preferred_choices"));
            // line 157
            $this->displayBlock("choice_widget_options", $context, $blocks);
            // line 158
            if (((twig_length_filter($this->env, (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"))) > 0) &&  !(null === (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator"))))) {
                // line 159
                echo "<option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator")), "html", null, true);
                echo "</option>";
            }
        }
        // line 162
        $context["options"] = (isset($context["choices"]) ? $context["choices"] : $this->getContext($context, "choices"));
        // line 163
        $this->displayBlock("choice_widget_options", $context, $blocks);
        // line 164
        echo "</select>";
        
        $__internal_4b7bc90c4e6360733b2ffcafd331928912cec982e988c5bcccc64ef9cf969067->leave($__internal_4b7bc90c4e6360733b2ffcafd331928912cec982e988c5bcccc64ef9cf969067_prof);

        
        $__internal_796bba90925d2d7dbd35811b46f618332e0b38e24d97661d99d220829398b946->leave($__internal_796bba90925d2d7dbd35811b46f618332e0b38e24d97661d99d220829398b946_prof);

    }

    // line 167
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_c3ab0039eaa3d2ae8752edf2437450b04ce599290c4f56fbe8f734bfb0aa8fa6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3ab0039eaa3d2ae8752edf2437450b04ce599290c4f56fbe8f734bfb0aa8fa6->enter($__internal_c3ab0039eaa3d2ae8752edf2437450b04ce599290c4f56fbe8f734bfb0aa8fa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_d6c73f62bfd1ad39655645cb592846abfbf7e44750ad651dcecfd120cd22201b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6c73f62bfd1ad39655645cb592846abfbf7e44750ad651dcecfd120cd22201b->enter($__internal_d6c73f62bfd1ad39655645cb592846abfbf7e44750ad651dcecfd120cd22201b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 168
        if (twig_in_filter("-inline", (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) {
            // line 169
            echo "        <ul class=\"inline-list\">
            ";
            // line 170
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 171
                echo "                <li>";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 172
(isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
                // line 173
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 175
            echo "        </ul>
    ";
        } else {
            // line 177
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 178
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 179
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 180
(isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
                // line 181
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 183
            echo "        </div>
    ";
        }
        
        $__internal_d6c73f62bfd1ad39655645cb592846abfbf7e44750ad651dcecfd120cd22201b->leave($__internal_d6c73f62bfd1ad39655645cb592846abfbf7e44750ad651dcecfd120cd22201b_prof);

        
        $__internal_c3ab0039eaa3d2ae8752edf2437450b04ce599290c4f56fbe8f734bfb0aa8fa6->leave($__internal_c3ab0039eaa3d2ae8752edf2437450b04ce599290c4f56fbe8f734bfb0aa8fa6_prof);

    }

    // line 187
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_19a404bc0c77dd96080ac58581a0809cfff1038ac18f73d81de1bc75812462f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19a404bc0c77dd96080ac58581a0809cfff1038ac18f73d81de1bc75812462f0->enter($__internal_19a404bc0c77dd96080ac58581a0809cfff1038ac18f73d81de1bc75812462f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_bcbac50269e5e7c91542716bd24a9540d352ebb86759bd144aff7d976052b6ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcbac50269e5e7c91542716bd24a9540d352ebb86759bd144aff7d976052b6ea->enter($__internal_bcbac50269e5e7c91542716bd24a9540d352ebb86759bd144aff7d976052b6ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 188
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), "")) : (""));
        // line 189
        echo "    ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 190
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " error"))));
            // line 191
            echo "    ";
        }
        // line 192
        echo "    ";
        if (twig_in_filter("checkbox-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 193
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            echo "
    ";
        } else {
            // line 195
            echo "        <div class=\"checkbox\">
            ";
            // line 196
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            echo "
        </div>
    ";
        }
        
        $__internal_bcbac50269e5e7c91542716bd24a9540d352ebb86759bd144aff7d976052b6ea->leave($__internal_bcbac50269e5e7c91542716bd24a9540d352ebb86759bd144aff7d976052b6ea_prof);

        
        $__internal_19a404bc0c77dd96080ac58581a0809cfff1038ac18f73d81de1bc75812462f0->leave($__internal_19a404bc0c77dd96080ac58581a0809cfff1038ac18f73d81de1bc75812462f0_prof);

    }

    // line 201
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_b6b8c9e4dc34c6c7f2695d0352d512ee0a461b25cd595edce21b32f846696e5b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6b8c9e4dc34c6c7f2695d0352d512ee0a461b25cd595edce21b32f846696e5b->enter($__internal_b6b8c9e4dc34c6c7f2695d0352d512ee0a461b25cd595edce21b32f846696e5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_316f1ba5c2e4baa00f016f57dc8c67a848538f4fcc47b0720fc4ea883052f3ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_316f1ba5c2e4baa00f016f57dc8c67a848538f4fcc47b0720fc4ea883052f3ab->enter($__internal_316f1ba5c2e4baa00f016f57dc8c67a848538f4fcc47b0720fc4ea883052f3ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 202
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), "")) : (""));
        // line 203
        echo "    ";
        if (twig_in_filter("radio-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 204
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            echo "
    ";
        } else {
            // line 206
            echo "        ";
            if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
                // line 207
                $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " error"))));
                // line 208
                echo "        ";
            }
            // line 209
            echo "        <div class=\"radio\">
            ";
            // line 210
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            echo "
        </div>
    ";
        }
        
        $__internal_316f1ba5c2e4baa00f016f57dc8c67a848538f4fcc47b0720fc4ea883052f3ab->leave($__internal_316f1ba5c2e4baa00f016f57dc8c67a848538f4fcc47b0720fc4ea883052f3ab_prof);

        
        $__internal_b6b8c9e4dc34c6c7f2695d0352d512ee0a461b25cd595edce21b32f846696e5b->leave($__internal_b6b8c9e4dc34c6c7f2695d0352d512ee0a461b25cd595edce21b32f846696e5b_prof);

    }

    // line 217
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_ea159e706803e1c06be22fa145c424d683b2ae7776e95dbc69d4a34118fb5175 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ea159e706803e1c06be22fa145c424d683b2ae7776e95dbc69d4a34118fb5175->enter($__internal_ea159e706803e1c06be22fa145c424d683b2ae7776e95dbc69d4a34118fb5175_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_8ce7de36c69cf45fab673288df03bbe78f37bf932c313747db4810bed9fbf176 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ce7de36c69cf45fab673288df03bbe78f37bf932c313747db4810bed9fbf176->enter($__internal_8ce7de36c69cf45fab673288df03bbe78f37bf932c313747db4810bed9fbf176_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 218
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 219
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " error"))));
            // line 220
            echo "    ";
        }
        // line 221
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_8ce7de36c69cf45fab673288df03bbe78f37bf932c313747db4810bed9fbf176->leave($__internal_8ce7de36c69cf45fab673288df03bbe78f37bf932c313747db4810bed9fbf176_prof);

        
        $__internal_ea159e706803e1c06be22fa145c424d683b2ae7776e95dbc69d4a34118fb5175->leave($__internal_ea159e706803e1c06be22fa145c424d683b2ae7776e95dbc69d4a34118fb5175_prof);

    }

    // line 224
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_ce6f9e2aed37228af21e4edca056d4bcea8df2943b64b4dfaab3e2d6ad7af14a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce6f9e2aed37228af21e4edca056d4bcea8df2943b64b4dfaab3e2d6ad7af14a->enter($__internal_ce6f9e2aed37228af21e4edca056d4bcea8df2943b64b4dfaab3e2d6ad7af14a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_15bf5b06a27ba24f4bf5132dac6ca897a490f7af2dceba84c54ffaa543fda34f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15bf5b06a27ba24f4bf5132dac6ca897a490f7af2dceba84c54ffaa543fda34f->enter($__internal_15bf5b06a27ba24f4bf5132dac6ca897a490f7af2dceba84c54ffaa543fda34f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 225
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 226
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " error"))));
            // line 227
            echo "    ";
        }
        // line 228
        echo "    ";
        // line 229
        echo "    ";
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(twig_replace_filter((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 230
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_15bf5b06a27ba24f4bf5132dac6ca897a490f7af2dceba84c54ffaa543fda34f->leave($__internal_15bf5b06a27ba24f4bf5132dac6ca897a490f7af2dceba84c54ffaa543fda34f_prof);

        
        $__internal_ce6f9e2aed37228af21e4edca056d4bcea8df2943b64b4dfaab3e2d6ad7af14a->leave($__internal_ce6f9e2aed37228af21e4edca056d4bcea8df2943b64b4dfaab3e2d6ad7af14a_prof);

    }

    // line 233
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_d7dbe9512c0bb064ecae9b2cc05e250390643f11c4789563aa8f06f9b2200ada = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7dbe9512c0bb064ecae9b2cc05e250390643f11c4789563aa8f06f9b2200ada->enter($__internal_d7dbe9512c0bb064ecae9b2cc05e250390643f11c4789563aa8f06f9b2200ada_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_af13a718218f2ee3ed808e6b9127f555b57876e9f972b7646b6ba0102f95a4f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af13a718218f2ee3ed808e6b9127f555b57876e9f972b7646b6ba0102f95a4f7->enter($__internal_af13a718218f2ee3ed808e6b9127f555b57876e9f972b7646b6ba0102f95a4f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 234
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_af13a718218f2ee3ed808e6b9127f555b57876e9f972b7646b6ba0102f95a4f7->leave($__internal_af13a718218f2ee3ed808e6b9127f555b57876e9f972b7646b6ba0102f95a4f7_prof);

        
        $__internal_d7dbe9512c0bb064ecae9b2cc05e250390643f11c4789563aa8f06f9b2200ada->leave($__internal_d7dbe9512c0bb064ecae9b2cc05e250390643f11c4789563aa8f06f9b2200ada_prof);

    }

    // line 237
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_05bd04340faf42d82e3181a95d153b4af5f0e2475e26ce86abacef8873592d70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05bd04340faf42d82e3181a95d153b4af5f0e2475e26ce86abacef8873592d70->enter($__internal_05bd04340faf42d82e3181a95d153b4af5f0e2475e26ce86abacef8873592d70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_1bc69417df5b54a520e4d736195be5b79fe3c5bbeee8cfcb0ab8ac665a60f3f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1bc69417df5b54a520e4d736195be5b79fe3c5bbeee8cfcb0ab8ac665a60f3f9->enter($__internal_1bc69417df5b54a520e4d736195be5b79fe3c5bbeee8cfcb0ab8ac665a60f3f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 238
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_1bc69417df5b54a520e4d736195be5b79fe3c5bbeee8cfcb0ab8ac665a60f3f9->leave($__internal_1bc69417df5b54a520e4d736195be5b79fe3c5bbeee8cfcb0ab8ac665a60f3f9_prof);

        
        $__internal_05bd04340faf42d82e3181a95d153b4af5f0e2475e26ce86abacef8873592d70->leave($__internal_05bd04340faf42d82e3181a95d153b4af5f0e2475e26ce86abacef8873592d70_prof);

    }

    // line 241
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_cfb3188c865a139786000776c33ac051c7e5e0708e488bb1523022bcf424a61d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cfb3188c865a139786000776c33ac051c7e5e0708e488bb1523022bcf424a61d->enter($__internal_cfb3188c865a139786000776c33ac051c7e5e0708e488bb1523022bcf424a61d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_88e5a44f8a576f7967d481de7e219e94924b05766dc1d825c38683adaae76b30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88e5a44f8a576f7967d481de7e219e94924b05766dc1d825c38683adaae76b30->enter($__internal_88e5a44f8a576f7967d481de7e219e94924b05766dc1d825c38683adaae76b30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 242
        if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
            // line 243
            echo "        ";
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
            // line 244
            echo "    ";
        }
        // line 245
        echo "    ";
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 246
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " error"))));
            // line 247
            echo "    ";
        }
        // line 248
        echo "    ";
        if (array_key_exists("parent_label_class", $context)) {
            // line 249
            echo "        ";
            $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class"))))));
            // line 250
            echo "    ";
        }
        // line 251
        echo "    ";
        if (twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")))) {
            // line 252
            if ( !twig_test_empty((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")))) {
                // line 253
                $context["label"] = twig_replace_filter((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")), array("%name%" =>                 // line 254
(isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "%id%" =>                 // line 255
(isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
            } else {
                // line 258
                $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
            }
        }
        // line 261
        echo "    <label";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
        ";
        // line 262
        echo (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget"));
        echo "
        ";
        // line 263
        echo twig_escape_filter($this->env, ((((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) ? ((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain"))))), "html", null, true);
        echo "
    </label>";
        
        $__internal_88e5a44f8a576f7967d481de7e219e94924b05766dc1d825c38683adaae76b30->leave($__internal_88e5a44f8a576f7967d481de7e219e94924b05766dc1d825c38683adaae76b30_prof);

        
        $__internal_cfb3188c865a139786000776c33ac051c7e5e0708e488bb1523022bcf424a61d->leave($__internal_cfb3188c865a139786000776c33ac051c7e5e0708e488bb1523022bcf424a61d_prof);

    }

    // line 269
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_8586f7d431368401ac1f3e30fdc3706cdc4c248cabbaa684ec3f887b16b62473 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8586f7d431368401ac1f3e30fdc3706cdc4c248cabbaa684ec3f887b16b62473->enter($__internal_8586f7d431368401ac1f3e30fdc3706cdc4c248cabbaa684ec3f887b16b62473_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_418308fdc38c524d8c4a74bae30d36f51d6c334a9be356e14149eab004f3de20 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_418308fdc38c524d8c4a74bae30d36f51d6c334a9be356e14149eab004f3de20->enter($__internal_418308fdc38c524d8c4a74bae30d36f51d6c334a9be356e14149eab004f3de20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 270
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 271
        if ((( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter((isset($context["force_error"]) ? $context["force_error"] : $this->getContext($context, "force_error")), false)) : (false))) &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            echo " error";
        }
        echo "\">
            ";
        // line 272
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        echo "
            ";
        // line 273
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 274
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_418308fdc38c524d8c4a74bae30d36f51d6c334a9be356e14149eab004f3de20->leave($__internal_418308fdc38c524d8c4a74bae30d36f51d6c334a9be356e14149eab004f3de20_prof);

        
        $__internal_8586f7d431368401ac1f3e30fdc3706cdc4c248cabbaa684ec3f887b16b62473->leave($__internal_8586f7d431368401ac1f3e30fdc3706cdc4c248cabbaa684ec3f887b16b62473_prof);

    }

    // line 279
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_425c07e4ae8504d0414ce108361ce2f22157c1c09b5490a88f18641add30852a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_425c07e4ae8504d0414ce108361ce2f22157c1c09b5490a88f18641add30852a->enter($__internal_425c07e4ae8504d0414ce108361ce2f22157c1c09b5490a88f18641add30852a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_62c77022b36cffa80176774fce5798d197dd4df5912411178b4c7c77aa998738 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62c77022b36cffa80176774fce5798d197dd4df5912411178b4c7c77aa998738->enter($__internal_62c77022b36cffa80176774fce5798d197dd4df5912411178b4c7c77aa998738_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 280
        $context["force_error"] = true;
        // line 281
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_62c77022b36cffa80176774fce5798d197dd4df5912411178b4c7c77aa998738->leave($__internal_62c77022b36cffa80176774fce5798d197dd4df5912411178b4c7c77aa998738_prof);

        
        $__internal_425c07e4ae8504d0414ce108361ce2f22157c1c09b5490a88f18641add30852a->leave($__internal_425c07e4ae8504d0414ce108361ce2f22157c1c09b5490a88f18641add30852a_prof);

    }

    // line 284
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_d10c46a5e0f83e296d3490e72524510e8aa2d4344a3b4d841d20e64a8fda6ea7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d10c46a5e0f83e296d3490e72524510e8aa2d4344a3b4d841d20e64a8fda6ea7->enter($__internal_d10c46a5e0f83e296d3490e72524510e8aa2d4344a3b4d841d20e64a8fda6ea7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_fbc270b49ca365e945339a2cdc1e427d7bf970c823d24883e24064d5837f67af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbc270b49ca365e945339a2cdc1e427d7bf970c823d24883e24064d5837f67af->enter($__internal_fbc270b49ca365e945339a2cdc1e427d7bf970c823d24883e24064d5837f67af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 285
        $context["force_error"] = true;
        // line 286
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_fbc270b49ca365e945339a2cdc1e427d7bf970c823d24883e24064d5837f67af->leave($__internal_fbc270b49ca365e945339a2cdc1e427d7bf970c823d24883e24064d5837f67af_prof);

        
        $__internal_d10c46a5e0f83e296d3490e72524510e8aa2d4344a3b4d841d20e64a8fda6ea7->leave($__internal_d10c46a5e0f83e296d3490e72524510e8aa2d4344a3b4d841d20e64a8fda6ea7_prof);

    }

    // line 289
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_29d8734e2934bad77879acda4656b84070b5a007847a5b54ecf2cdb2f545bdd1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29d8734e2934bad77879acda4656b84070b5a007847a5b54ecf2cdb2f545bdd1->enter($__internal_29d8734e2934bad77879acda4656b84070b5a007847a5b54ecf2cdb2f545bdd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_94f28a952a741f62c57880e17c9004ba460afdd5497d1284a28154a3f23b4586 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94f28a952a741f62c57880e17c9004ba460afdd5497d1284a28154a3f23b4586->enter($__internal_94f28a952a741f62c57880e17c9004ba460afdd5497d1284a28154a3f23b4586_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 290
        $context["force_error"] = true;
        // line 291
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_94f28a952a741f62c57880e17c9004ba460afdd5497d1284a28154a3f23b4586->leave($__internal_94f28a952a741f62c57880e17c9004ba460afdd5497d1284a28154a3f23b4586_prof);

        
        $__internal_29d8734e2934bad77879acda4656b84070b5a007847a5b54ecf2cdb2f545bdd1->leave($__internal_29d8734e2934bad77879acda4656b84070b5a007847a5b54ecf2cdb2f545bdd1_prof);

    }

    // line 294
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_1e008ea4107b83fdf279bba4bb534bd88d29738209f3e6421b9927cf1e9af3c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e008ea4107b83fdf279bba4bb534bd88d29738209f3e6421b9927cf1e9af3c4->enter($__internal_1e008ea4107b83fdf279bba4bb534bd88d29738209f3e6421b9927cf1e9af3c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_7e10cb63a06748d6c26b83485483866587dba3007bbe5397fac60e9e2e1e4285 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e10cb63a06748d6c26b83485483866587dba3007bbe5397fac60e9e2e1e4285->enter($__internal_7e10cb63a06748d6c26b83485483866587dba3007bbe5397fac60e9e2e1e4285_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 295
        $context["force_error"] = true;
        // line 296
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_7e10cb63a06748d6c26b83485483866587dba3007bbe5397fac60e9e2e1e4285->leave($__internal_7e10cb63a06748d6c26b83485483866587dba3007bbe5397fac60e9e2e1e4285_prof);

        
        $__internal_1e008ea4107b83fdf279bba4bb534bd88d29738209f3e6421b9927cf1e9af3c4->leave($__internal_1e008ea4107b83fdf279bba4bb534bd88d29738209f3e6421b9927cf1e9af3c4_prof);

    }

    // line 299
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_19341a1f73bb2093b3980501e49784e4f964d142e62872b007b700a84351866a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19341a1f73bb2093b3980501e49784e4f964d142e62872b007b700a84351866a->enter($__internal_19341a1f73bb2093b3980501e49784e4f964d142e62872b007b700a84351866a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_3279f17a752ad40d2540ba36c6749f7709209a80a94963167ff93d4c8b12ec11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3279f17a752ad40d2540ba36c6749f7709209a80a94963167ff93d4c8b12ec11->enter($__internal_3279f17a752ad40d2540ba36c6749f7709209a80a94963167ff93d4c8b12ec11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 300
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 301
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " error";
        }
        echo "\">
            ";
        // line 302
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 303
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_3279f17a752ad40d2540ba36c6749f7709209a80a94963167ff93d4c8b12ec11->leave($__internal_3279f17a752ad40d2540ba36c6749f7709209a80a94963167ff93d4c8b12ec11_prof);

        
        $__internal_19341a1f73bb2093b3980501e49784e4f964d142e62872b007b700a84351866a->leave($__internal_19341a1f73bb2093b3980501e49784e4f964d142e62872b007b700a84351866a_prof);

    }

    // line 308
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_cbe220200fa6da4b39bd69285e57f442e2e58c7d52dda9d2933ab6ca38356a10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cbe220200fa6da4b39bd69285e57f442e2e58c7d52dda9d2933ab6ca38356a10->enter($__internal_cbe220200fa6da4b39bd69285e57f442e2e58c7d52dda9d2933ab6ca38356a10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_3c1aaae7442883e6c7bf75d0a55c94bb511ba1a88cd5c6dd8917f631cab7ff50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c1aaae7442883e6c7bf75d0a55c94bb511ba1a88cd5c6dd8917f631cab7ff50->enter($__internal_3c1aaae7442883e6c7bf75d0a55c94bb511ba1a88cd5c6dd8917f631cab7ff50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 309
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 310
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " error";
        }
        echo "\">
            ";
        // line 311
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 312
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_3c1aaae7442883e6c7bf75d0a55c94bb511ba1a88cd5c6dd8917f631cab7ff50->leave($__internal_3c1aaae7442883e6c7bf75d0a55c94bb511ba1a88cd5c6dd8917f631cab7ff50_prof);

        
        $__internal_cbe220200fa6da4b39bd69285e57f442e2e58c7d52dda9d2933ab6ca38356a10->leave($__internal_cbe220200fa6da4b39bd69285e57f442e2e58c7d52dda9d2933ab6ca38356a10_prof);

    }

    // line 319
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_60bf846e96ff31860566060ffe34991022fc20773091ca5889c5664554f1ca6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60bf846e96ff31860566060ffe34991022fc20773091ca5889c5664554f1ca6e->enter($__internal_60bf846e96ff31860566060ffe34991022fc20773091ca5889c5664554f1ca6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_7ed0ca4daae5131e3241be9da05b7bd26705e35100e98a3886290bef95cb287b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ed0ca4daae5131e3241be9da05b7bd26705e35100e98a3886290bef95cb287b->enter($__internal_7ed0ca4daae5131e3241be9da05b7bd26705e35100e98a3886290bef95cb287b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 320
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 321
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")))) {
                echo "<small class=\"error\">";
            } else {
                echo "<div data-alert class=\"alert-box alert\">";
            }
            // line 322
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 323
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "
            ";
                // line 324
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 326
            if ( !Symfony\Bridge\Twig\Extension\twig_is_root_form((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")))) {
                echo "</small>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_7ed0ca4daae5131e3241be9da05b7bd26705e35100e98a3886290bef95cb287b->leave($__internal_7ed0ca4daae5131e3241be9da05b7bd26705e35100e98a3886290bef95cb287b_prof);

        
        $__internal_60bf846e96ff31860566060ffe34991022fc20773091ca5889c5664554f1ca6e->leave($__internal_60bf846e96ff31860566060ffe34991022fc20773091ca5889c5664554f1ca6e_prof);

    }

    public function getTemplateName()
    {
        return "foundation_5_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1078 => 326,  1062 => 324,  1058 => 323,  1041 => 322,  1035 => 321,  1033 => 320,  1024 => 319,  1011 => 312,  1007 => 311,  1001 => 310,  998 => 309,  989 => 308,  976 => 303,  972 => 302,  966 => 301,  963 => 300,  954 => 299,  943 => 296,  941 => 295,  932 => 294,  921 => 291,  919 => 290,  910 => 289,  899 => 286,  897 => 285,  888 => 284,  877 => 281,  875 => 280,  866 => 279,  853 => 274,  849 => 273,  845 => 272,  839 => 271,  836 => 270,  827 => 269,  815 => 263,  811 => 262,  795 => 261,  791 => 258,  788 => 255,  787 => 254,  786 => 253,  784 => 252,  781 => 251,  778 => 250,  775 => 249,  772 => 248,  769 => 247,  767 => 246,  764 => 245,  761 => 244,  758 => 243,  756 => 242,  747 => 241,  737 => 238,  728 => 237,  718 => 234,  709 => 233,  699 => 230,  696 => 229,  694 => 228,  691 => 227,  689 => 226,  687 => 225,  678 => 224,  668 => 221,  665 => 220,  663 => 219,  661 => 218,  652 => 217,  638 => 210,  635 => 209,  632 => 208,  630 => 207,  627 => 206,  621 => 204,  618 => 203,  616 => 202,  607 => 201,  593 => 196,  590 => 195,  584 => 193,  581 => 192,  578 => 191,  576 => 190,  573 => 189,  571 => 188,  562 => 187,  550 => 183,  543 => 181,  541 => 180,  539 => 179,  535 => 178,  530 => 177,  526 => 175,  519 => 173,  517 => 172,  515 => 171,  511 => 170,  508 => 169,  506 => 168,  497 => 167,  487 => 164,  485 => 163,  483 => 162,  477 => 159,  475 => 158,  473 => 157,  471 => 156,  469 => 155,  460 => 153,  458 => 152,  450 => 151,  447 => 149,  445 => 148,  442 => 147,  439 => 146,  437 => 145,  435 => 144,  432 => 143,  429 => 142,  427 => 141,  425 => 140,  416 => 139,  405 => 136,  401 => 134,  398 => 133,  390 => 128,  379 => 121,  371 => 116,  358 => 106,  347 => 99,  344 => 98,  338 => 96,  335 => 95,  332 => 94,  329 => 92,  327 => 91,  318 => 90,  307 => 87,  303 => 85,  301 => 84,  299 => 82,  298 => 81,  297 => 80,  296 => 79,  290 => 77,  287 => 76,  284 => 75,  281 => 73,  279 => 72,  270 => 71,  256 => 66,  252 => 65,  248 => 64,  243 => 62,  239 => 61,  236 => 60,  233 => 59,  230 => 57,  228 => 56,  219 => 55,  205 => 48,  203 => 47,  200 => 45,  191 => 44,  181 => 41,  175 => 38,  172 => 37,  170 => 36,  167 => 35,  165 => 34,  163 => 33,  157 => 30,  154 => 29,  151 => 28,  149 => 27,  146 => 26,  137 => 25,  127 => 22,  125 => 21,  116 => 20,  106 => 17,  103 => 16,  101 => 15,  99 => 14,  90 => 13,  80 => 10,  77 => 9,  75 => 8,  73 => 7,  64 => 6,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"form_div_layout.html.twig\" %}

{# Based on Foundation 5 Doc #}
{# Widgets #}

{% block form_widget_simple -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block textarea_widget -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock textarea_widget %}

{% block button_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' button')|trim}) %}
    {{- parent() -}}
{%- endblock button_widget %}

{% block money_widget -%}
    <div class=\"row collapse\">
        {% set prepend = '{{' == money_pattern[0:2] %}
        {% if not prepend %}
            <div class=\"small-3 large-2 columns\">
                <span class=\"prefix\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
            </div>
        {% endif %}
        <div class=\"small-9 large-10 columns\">
            {{- block('form_widget_simple') -}}
        </div>
        {% if prepend %}
            <div class=\"small-3 large-2 columns\">
                <span class=\"postfix\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
            </div>
        {% endif %}
    </div>
{%- endblock money_widget %}

{% block percent_widget -%}
    <div class=\"row collapse\">
        <div class=\"small-9 large-10 columns\">
            {{- block('form_widget_simple') -}}
        </div>
        <div class=\"small-3 large-2 columns\">
            <span class=\"postfix\">%</span>
        </div>
    </div>
{%- endblock percent_widget %}

{% block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        <div class=\"row\">
            <div class=\"large-7 columns\">{{ form_errors(form.date) }}</div>
            <div class=\"large-5 columns\">{{ form_errors(form.time) }}</div>
        </div>
        <div {{ block('widget_container_attributes') }}>
            <div class=\"large-7 columns\">{{ form_widget(form.date, { datetime: true } ) }}</div>
            <div class=\"large-5 columns\">{{ form_widget(form.time, { datetime: true } ) }}</div>
        </div>
    {% endif %}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        {% if datetime is not defined or not datetime %}
            <div {{ block('widget_container_attributes') }}>
        {% endif %}
        {{- date_pattern|replace({
            '{{ year }}': '<div class=\"large-4 columns\">' ~ form_widget(form.year) ~ '</div>',
            '{{ month }}': '<div class=\"large-4 columns\">' ~ form_widget(form.month) ~ '</div>',
            '{{ day }}': '<div class=\"large-4 columns\">' ~ form_widget(form.day) ~ '</div>',
        })|raw -}}
        {% if datetime is not defined or not datetime %}
            </div>
        {% endif %}
    {% endif %}
{%- endblock date_widget %}

{% block time_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        {% if datetime is not defined or false == datetime %}
            <div {{ block('widget_container_attributes') -}}>
        {% endif %}
        {% if with_seconds %}
            <div class=\"large-4 columns\">{{ form_widget(form.hour) }}</div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.minute) }}
                    </div>
                </div>
            </div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.second) }}
                    </div>
                </div>
            </div>
        {% else %}
            <div class=\"large-6 columns\">{{ form_widget(form.hour) }}</div>
            <div class=\"large-6 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.minute) }}
                    </div>
                </div>
            </div>
        {% endif %}
        {% if datetime is not defined or false == datetime %}
            </div>
        {% endif %}
    {% endif %}
{%- endblock time_widget %}

{% block choice_widget_collapsed -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}

    {% if multiple -%}
        {% set attr = attr|merge({style: (attr.style|default('') ~ ' height: auto; background-image: none;')|trim}) %}
    {% endif %}

    {% if required and placeholder is none and not placeholder_in_choices and not multiple -%}
        {% set required = false %}
    {%- endif -%}
    <select {{ block('widget_attributes') }}{% if multiple %} multiple=\"multiple\" data-customforms=\"disabled\"{% endif %}>
        {% if placeholder is not none -%}
            <option value=\"\"{% if required and value is empty %} selected=\"selected\"{% endif %}>{{ translation_domain is same as(false) ? placeholder : placeholder|trans({}, translation_domain) }}</option>
        {%- endif %}
        {%- if preferred_choices|length > 0 -%}
            {% set options = preferred_choices %}
            {{- block('choice_widget_options') -}}
            {% if choices|length > 0 and separator is not none -%}
                <option disabled=\"disabled\">{{ separator }}</option>
            {%- endif %}
        {%- endif -%}
        {% set options = choices -%}
        {{- block('choice_widget_options') -}}
    </select>
{%- endblock choice_widget_collapsed %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') %}
        <ul class=\"inline-list\">
            {% for child in form %}
                <li>{{ form_widget(child, {
                        parent_label_class: label_attr.class|default(''),
                    }) }}</li>
            {% endfor %}
        </ul>
    {% else %}
        <div {{ block('widget_container_attributes') }}>
            {% for child in form %}
                {{ form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                }) }}
            {% endfor %}
        </div>
    {% endif %}
{%- endblock choice_widget_expanded %}

{% block checkbox_widget -%}
    {% set parent_label_class = parent_label_class|default('') %}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {% if 'checkbox-inline' in parent_label_class %}
        {{ form_label(form, null, { widget: parent() }) }}
    {% else %}
        <div class=\"checkbox\">
            {{ form_label(form, null, { widget: parent() }) }}
        </div>
    {% endif %}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {% set parent_label_class = parent_label_class|default('') %}
    {% if 'radio-inline' in parent_label_class %}
        {{ form_label(form, null, { widget: parent() }) }}
    {% else %}
        {% if errors|length > 0 -%}
            {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
        {% endif %}
        <div class=\"radio\">
            {{ form_label(form, null, { widget: parent() }) }}
        </div>
    {% endif %}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {% set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) %}
    {{- block('form_label') -}}
{%- endblock choice_label %}

{% block checkbox_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label -%}
    {% if required %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}
    {% endif %}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {% if parent_label_class is defined %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ parent_label_class)|trim}) %}
    {% endif %}
    {% if label is empty %}
        {%- if label_format is not empty -%}
            {% set label = label_format|replace({
                '%name%': name,
                '%id%': id,
            }) %}
        {%- else -%}
            {% set label = name|humanize %}
        {%- endif -%}
    {% endif %}
    <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
        {{ widget|raw }}
        {{ translation_domain is same as(false) ? label : label|trans({}, translation_domain) }}
    </label>
{%- endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if (not compound or force_error|default(false)) and not valid %} error{% endif %}\">
            {{ form_label(form) }}
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock form_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if not valid %} error{% endif %}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if not valid %} error{% endif %}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form is not rootform %}<small class=\"error\">{% else %}<div data-alert class=\"alert-box alert\">{% endif %}
        {%- for error in errors -%}
            {{ error.message }}
            {% if not loop.last %}, {% endif %}
        {%- endfor -%}
        {% if form is not rootform %}</small>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "foundation_5_layout.html.twig", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\foundation_5_layout.html.twig");
    }
}
