<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_2867206b76ebf5b7bfb8b1a11c64c11726dde62a2420441a86e55cdda0040bc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b57c781ad7edaaf41fda81f42f1ce5435bc465063a860321857fcfbdaaa49c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b57c781ad7edaaf41fda81f42f1ce5435bc465063a860321857fcfbdaaa49c7->enter($__internal_7b57c781ad7edaaf41fda81f42f1ce5435bc465063a860321857fcfbdaaa49c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_06cfa60365ab49462d15dc69c4ee170845af003e5fe957bb9d4f98c9c6615216 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06cfa60365ab49462d15dc69c4ee170845af003e5fe957bb9d4f98c9c6615216->enter($__internal_06cfa60365ab49462d15dc69c4ee170845af003e5fe957bb9d4f98c9c6615216_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_7b57c781ad7edaaf41fda81f42f1ce5435bc465063a860321857fcfbdaaa49c7->leave($__internal_7b57c781ad7edaaf41fda81f42f1ce5435bc465063a860321857fcfbdaaa49c7_prof);

        
        $__internal_06cfa60365ab49462d15dc69c4ee170845af003e5fe957bb9d4f98c9c6615216->leave($__internal_06cfa60365ab49462d15dc69c4ee170845af003e5fe957bb9d4f98c9c6615216_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "C:\\projects\\graveyard\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_widget.html.php");
    }
}
