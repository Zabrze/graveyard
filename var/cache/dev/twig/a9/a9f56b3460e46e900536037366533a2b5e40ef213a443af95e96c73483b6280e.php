<?php

/* @FOSUser/ChangePassword/change_password.html.twig */
class __TwigTemplate_764399fb868d4954748218e3742284a3995ebcc7c7f654ac5fc3615b5898281c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_06f0846662c88e744680caf78e76b4863a61a1af600d65258d550f3e1abfa418 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_06f0846662c88e744680caf78e76b4863a61a1af600d65258d550f3e1abfa418->enter($__internal_06f0846662c88e744680caf78e76b4863a61a1af600d65258d550f3e1abfa418_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $__internal_236534990ae4c80832e3ca9565586175ef5b7e577979381a55e1ba98219afc2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_236534990ae4c80832e3ca9565586175ef5b7e577979381a55e1ba98219afc2d->enter($__internal_236534990ae4c80832e3ca9565586175ef5b7e577979381a55e1ba98219afc2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_06f0846662c88e744680caf78e76b4863a61a1af600d65258d550f3e1abfa418->leave($__internal_06f0846662c88e744680caf78e76b4863a61a1af600d65258d550f3e1abfa418_prof);

        
        $__internal_236534990ae4c80832e3ca9565586175ef5b7e577979381a55e1ba98219afc2d->leave($__internal_236534990ae4c80832e3ca9565586175ef5b7e577979381a55e1ba98219afc2d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ee5603786690f5c4904122641ed06d9b61d08e200ce4a98a3cf9b4917037ab52 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee5603786690f5c4904122641ed06d9b61d08e200ce4a98a3cf9b4917037ab52->enter($__internal_ee5603786690f5c4904122641ed06d9b61d08e200ce4a98a3cf9b4917037ab52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_edb65b1bf4a6b092bc32787e372c59f0e72b368eeee9722cf899377507f01552 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_edb65b1bf4a6b092bc32787e372c59f0e72b368eeee9722cf899377507f01552->enter($__internal_edb65b1bf4a6b092bc32787e372c59f0e72b368eeee9722cf899377507f01552_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 4)->display($context);
        
        $__internal_edb65b1bf4a6b092bc32787e372c59f0e72b368eeee9722cf899377507f01552->leave($__internal_edb65b1bf4a6b092bc32787e372c59f0e72b368eeee9722cf899377507f01552_prof);

        
        $__internal_ee5603786690f5c4904122641ed06d9b61d08e200ce4a98a3cf9b4917037ab52->leave($__internal_ee5603786690f5c4904122641ed06d9b61d08e200ce4a98a3cf9b4917037ab52_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/ChangePassword/change_password.html.twig", "C:\\projects\\graveyard\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\ChangePassword\\change_password.html.twig");
    }
}
