<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use \AppBundle\Entity\Dead;
use \AppBundle\Entity\Grave;
use \AppBundle\Entity\Curator;
use \AppBundle\Entity\Address;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

//$types = [
//    '1' => 'typ pierwszy'
//];
//
//$types['1'];

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {

        return $this->render('default/index.html.twig', [
                    'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/szukaj", name="search")
     */
    public function searchDeadAction(Request $request) {

        if ($request->get('name')) {
            $searchedName = $request->get('name');
            $searchedSurname = $request->get('surname');
        }

        if (isset($searchedName) && ($searchedName) !== '' && isset($searchedSurname) && ($searchedSurname) !== '') {

            $entityManager = $this->getDoctrine()->getManager();
            $deads = $entityManager->getRepository(Dead::class)->findBy(['name' => $searchedName, 'surname' => $searchedSurname]);

            if ($deads) {
                return $this->render('default/deadView.html.twig', ['users' => $deads]);
            } else {
                return $this->render('default/deadView.html.twig', ['users' => FALSE]);
            }
        }

        return $this->render('default/searchForm.html.twig');
    }

    /**
     * @Route("/display/{id}", name="wyswietl_zmarlego")
     */
    public function displayDeadAction($id) {

        $entityManager = $this->getDoctrine()->getManager();

        $dead = $entityManager->getRepository(Dead::class)->findOneBy(['id' => $id]);

        return $this->render('default/deadCard.html.twig', ['dead' => $dead]);
    }
    
    /**
     * @Route("/addDead", name="dodaj_zmarlego")
     */
    public function addDeadAction(Request $request) {

        //$grave = $this->getGrave();
        $task = new Dead();

        $session = new Session();
        $form = $this->createFormBuilder($task)
                ->add('deadPlace', TextType::class, array('label' => 'Numer miejsca: '))
                ->add('name', TextType::class, array('label' => 'Imię: '))
                ->add('secondName', TextType::class, array('required' => false, 'label' => 'Drugie imię(niewymagane): '))
                ->add('surname', TextType::class, array('label' => 'Nazwisko: '))
                ->add('sex', TextType::class, array('label' => 'Płeć (1-kobieta, 2-mężczyzna): '))
                ->add('birthDate', BirthdayType::class, array('label' => 'Data urodzenia: '))
                ->add('deathDate', DateType::class, array('label' => 'Data Zgonu: '))
                ->add('funeralDate', DateType::class, array('label' => 'Data Pochówku: '))
                ->add('boxType', TextType::class, array('label' => 'Rodzaj skrzynki (1-urna, 2-trumna)'))
                ->add('save', SubmitType::class, array('label' => 'Dodaj zmarłego'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            //$task->setGrave($grave);
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            $session->getFlashBag()->add('notice', 'Dodano ');
            unset($_POST['name']);
            unset($_POST['surname']);


            return $this->redirectToRoute('dodaj_zmarlego');
        }
        return $this->render('default/addDead.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/addGrave", name="dodaj_grob")
     */
    public function addGraveAction(Request $request) {

        $task = new Grave();

        $session = new Session();
        $form = $this->createFormBuilder($task)
                ->add('place', TextType::class, array('label' => 'Miejsce'))
                ->add('graveType', TextType::class, array('label' => 'Typ grobu'))
                ->add('tombType', TextType::class, array('required' => false, 'label' => 'Typ grobu'))
                ->add('reserved', TextType::class, array('label' => 'Zarezerwowany'))
                ->add('expireDate', DateType::class, array('label' => 'Opłacony do:'))
                ->add('save', SubmitType::class, array('label' => 'Dodaj grób'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!


            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            $session->getFlashBag()->add('notice', 'Dodano ');



            return $this->redirectToRoute('dodaj_grob');
        }
        return $this->render('default/addGrave.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/addCurator", name="dodaj_opiekuna")
     */
    public function addCuratorAction(Request $request) {

        $task = new Curator();

        $session = new Session();

        $form = $this->createFormBuilder($task)
                ->add('name', TextType::class, array('label' => 'Imię'))
                ->add('secondName', TextType::class, array(
                    'required' => false,
                    'label' => 'Drugie imię'))
                ->add('surname', TextType::class, array('label' => 'Nazwisko'))
                ->add('sex', TextType::class, array('label' => 'Płeć'))
                ->add('phoneNumber', TextType::class, array('label' => 'Numer telefonu'))
                ->add('email', TextType::class, array('label' => 'e-mail'))
                // ->add('address', TextType::class, array('label' => 'Adres(?)'))
//                ->add('boxType', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Dodaj opiekuna'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $task = $form->getData();


            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!


            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            $session->getFlashBag()->add('notice', 'Dodano ');



            return $this->redirectToRoute('dodaj_opiekuna');
        }
        return $this->render('default/addCurator.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
