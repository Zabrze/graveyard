<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Payment
 *
 * @ORM\Table(name="payment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentRepository")
 */
class Payment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="payer", type="integer")
     */
    private $payer;

    /**
     * @var int
     *
     * @ORM\Column(name="paid_grave", type="integer")
     */
    private $paidGrave;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", precision=10, scale=2)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_date", type="datetime")
     */
    private $paymentDate;

    /**
     * @var int
     *
     * @ORM\Column(name="payment_period", type="integer")
     */
    private $paymentPeriod;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_time", type="datetime")
     */
    private $expireTime;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payer
     *
     * @param integer $payer
     *
     * @return Payment
     */
    public function setPayer($payer)
    {
        $this->payer = $payer;

        return $this;
    }

    /**
     * Get payer
     *
     * @return int
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * Set paidGrave
     *
     * @param integer $paidGrave
     *
     * @return Payment
     */
    public function setPaidGrave($paidGrave)
    {
        $this->paidGrave = $paidGrave;

        return $this;
    }

    /**
     * Get paidGrave
     *
     * @return int
     */
    public function getPaidGrave()
    {
        return $this->paidGrave;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Payment
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set paymentDate
     *
     * @param \DateTime $paymentDate
     *
     * @return Payment
     */
    public function setPaymentDate($paymentDate)
    {
        $this->paymentDate = $paymentDate;

        return $this;
    }

    /**
     * Get paymentDate
     *
     * @return \DateTime
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * Set paymentPeriod
     *
     * @param integer $paymentPeriod
     *
     * @return Payment
     */
    public function setPaymentPeriod($paymentPeriod)
    {
        $this->paymentPeriod = $paymentPeriod;

        return $this;
    }

    /**
     * Get paymentPeriod
     *
     * @return int
     */
    public function getPaymentPeriod()
    {
        return $this->paymentPeriod;
    }

    /**
     * Set expireTime
     *
     * @param \DateTime $expireTime
     *
     * @return Payment
     */
    public function setExpireTime($expireTime)
    {
        $this->expireTime = $expireTime;

        return $this;
    }

    /**
     * Get expireTime
     *
     * @return \DateTime
     */
    public function getExpireTime()
    {
        return $this->expireTime;
    }
}

