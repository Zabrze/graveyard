<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Dead
 *
 * @ORM\Table(name="dead")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DeadRepository")
 */
class Dead
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="Grave", inversedBy="deads")
     */
    
    private $grave;

    /**
     * @var string
     *
     * @ORM\Column(name="dead_place", type="string")
     */
    private $deadPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="second_name", type="string", length=50, nullable=true)
     */
    private $secondName;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=50)
     */
    private $surname;

    /**
     * @var int
     *
     * @ORM\Column(name="sex", type="integer")
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(name="maiden_surname", type="string", length=50, nullable=true)
     */
    private $maidenSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="mother_name", type="string", length=50, nullable=true)
     */
    private $motherName;

    /**
     * @var string
     *
     * @ORM\Column(name="father_name", type="string", length=50, nullable=true)
     */
    private $fatherName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime")
     */
    private $birthDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="death_date", type="datetime")
     */
    private $deathDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="funeral_date", type="datetime")
     */
    private $funeralDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable = true)
     * @Gedmo\Timestampable(on="create")
     */
    private $createDate;

    /**
     * @var int
     *
     * @ORM\Column(name="box_type", type="integer")
     */
    private $boxType;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deadPlace
     *
     * @param integer $deadPlace
     *
     * @return Dead
     */
    public function setDeadPlace($deadPlace)
    {
        $this->deadPlace = $deadPlace;

        return $this;
    }

    /**
     * Get deadPlace
     *
     * @return int
     */
    public function getDeadPlace()
    {
        return $this->deadPlace;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dead
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set secondName
     *
     * @param string $secondName
     *
     * @return Dead
     */
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * Get secondName
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return Dead
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     *
     * @return Dead
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return int
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set maidenSurname
     *
     * @param string $maidenSurname
     *
     * @return Dead
     */
    public function setMaidenSurname($maidenSurname)
    {
        $this->maidenSurname = $maidenSurname;

        return $this;
    }

    /**
     * Get maidenSurname
     *
     * @return string
     */
    public function getMaidenSurname()
    {
        return $this->maidenSurname;
    }

    /**
     * Set motherName
     *
     * @param string $motherName
     *
     * @return Dead
     */
    public function setMotherName($motherName)
    {
        $this->motherName = $motherName;

        return $this;
    }

    /**
     * Get motherName
     *
     * @return string
     */
    public function getMotherName()
    {
        return $this->motherName;
    }

    /**
     * Set fatherName
     *
     * @param string $fatherName
     *
     * @return Dead
     */
    public function setFatherName($fatherName)
    {
        $this->fatherName = $fatherName;

        return $this;
    }

    /**
     * Get fatherName
     *
     * @return string
     */
    public function getFatherName()
    {
        return $this->fatherName;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return Dead
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set deathDate
     *
     * @param \DateTime $deathDate
     *
     * @return Dead
     */
    public function setDeathDate($deathDate)
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    /**
     * Get deathDate
     *
     * @return \DateTime
     */
    public function getDeathDate()
    {
        return $this->deathDate;
    }

    /**
     * Set funeralDate
     *
     * @param \DateTime $funeralDate
     *
     * @return Dead
     */
    public function setFuneralDate($funeralDate)
    {
        $this->funeralDate = $funeralDate;

        return $this;
    }

    /**
     * Get funeralDate
     *
     * @return \DateTime
     */
    public function getFuneralDate()
    {
        return $this->funeralDate;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Dead
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set boxType
     *
     * @param integer $boxType
     *
     * @return Dead
     */
    public function setBoxType($boxType)
    {
        $this->boxType = $boxType;

        return $this;
    }

    /**
     * Get boxType
     *
     * @return int
     */
    public function getBoxType()
    {
        return $this->boxType;
    }
    function getGrave() {
        return $this->grave;
    }

    function setGrave(Grave $grave) {
        $this->grave = $grave;
        return $this;
    }


    
}

