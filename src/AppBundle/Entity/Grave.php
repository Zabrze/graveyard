<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grave
 *
 * @ORM\Table(name="grave")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GraveRepository")
 */
class Grave
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="Dead", mappedBy="grave")
     */
    private $deads;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=10, unique=true)
     */
    private $place;

    /**
     * @var int
     *
     * @ORM\Column(name="grave_type", type="integer")
     */
    private $graveType;

    /**
     * @var int
     *
     * @ORM\Column(name="tomb_type", type="integer")
     */
    private $tombType;

    /**
     * @var bool
     *
     * @ORM\Column(name="reserved", type="boolean")
     */
    private $reserved;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_date", type="datetime", nullable=true)
     */
    private $expireDate;

    
    function __construct() {
        $this->deads = new \Doctrine\Common\Collections\ArrayCollection();
    }

        /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set place
     *
     * @param string $place
     *
     * @return Grave
     */
    public function setPlace($place)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return string
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set graveType
     *
     * @param integer $graveType
     *
     * @return Grave
     */
    public function setGraveType($graveType)
    {
        $this->graveType = $graveType;

        return $this;
    }

    /**
     * Get graveType
     *
     * @return int
     */
    public function getGraveType()
    {
        return $this->graveType;
    }

    /**
     * Set tombType
     *
     * @param integer $tombType
     *
     * @return Grave
     */
    public function setTombType($tombType)
    {
        $this->tombType = $tombType;

        return $this;
    }

    /**
     * Get tombType
     *
     * @return int
     */
    public function getTombType()
    {
        return $this->tombType;
    }

    /**
     * Set reserved
     *
     * @param boolean $reserved
     *
     * @return Grave
     */
    public function setReserved($reserved)
    {
        $this->reserved = $reserved;

        return $this;
    }

    /**
     * Get reserved
     *
     * @return bool
     */
    public function getReserved()
    {
        return $this->reserved;
    }

    /**
     * Set expireDate
     *
     * @param \DateTime $expireDate
     *
     * @return Grave
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;

        return $this;
    }

    /**
     * Get expireDate
     *
     * @return \DateTime
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }
}

